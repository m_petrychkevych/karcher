from wtforms.widgets import FileInput, TextInput
from wtforms.widgets import html_params, HTMLString


class UploadInput(FileInput):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        class_ = kwargs.get('class_', '')
        class_ = class_ + ' file-loading' if class_ else 'file-loading'
        kwargs['class_'] = class_
        return HTMLString('<input %s>' % html_params(name=field.name, type='file', **kwargs))


class SelectMultipleCheckbox():
    def __call__(self, field, **kwargs):
        kwargs.setdefault('type', 'checkbox')
        field_id = kwargs.pop('id', field.id)
        html = []

        html.append(u'<ul class="list-group">')

        for value, label, checked in field.iter_choices():
            choice_id = u'%s-%s' % (field_id, value)
            options = dict(kwargs, name=field.name, value=value, id=choice_id)

            if checked:
                options['checked'] = 'checked'

            html.append(u'<li class="list-group-item"><label class="%s" for="%s"><input %s /> %s</label></li>' % (
                kwargs.get('label_class', ''), choice_id, html_params(**options), label
            ))

        html.append(u'</ul>')

        return HTMLString(u''.join(html))


class DateInput(TextInput):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        kwargs.setdefault('type', self.input_type)

        if 'value' not in kwargs:
            kwargs['value'] = field._value()

        html = []
        html.append(u'<div class="input-group">')
        html.append(u'<input %s data-provide="datepicker">' % self.html_params(name=field.name, **kwargs))
        html.append(u'<div class="input-group-addon">')
        html.append(u'<span class="glyphicon glyphicon-th"></span>')
        html.append(u'</div>')
        html.append(u'</div>')

        return HTMLString('\n'.join(html))
