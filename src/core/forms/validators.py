from flask.ext.babel import lazy_gettext as __
from wtforms.validators import StopValidation, ValidationError


class IntRequired(object):
    def __init__(self, message=None, positive=False):
        self.message = message
        self.positive = positive

    def __call__(self, form, field):
        if field.data:
            try:
                result = int(field.data)
            except ValueError as e:
                result = None

            if result is None:
                if self.message is None:
                    message = field.gettext('Field must be an integer.')
                    message = __(message)  # FIX: Need lazy_gettext instead of gettext
                else:
                    message = self.message

                field.errors[:] = []
                raise StopValidation(message)

            result_positive = result >= 0

            if not result_positive and self.positive:
                raise StopValidation(field.gettext('Field must be a positive integer.'))
