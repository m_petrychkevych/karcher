
def is_empty(filters_form):
    result = True

    for d in filters_form.data.values():
        if d and d != 'None':
            result = False
            break

    return result
