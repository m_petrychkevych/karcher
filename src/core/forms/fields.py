# coding: utf-8
from slugify import slugify
from wtforms import FileField, SelectMultipleField, TextField
from wtforms.widgets import Select
from . import widgets


__all__ = (
    'UploadField', 'ColorSelect', 'SlugField'
)


class SlugField(TextField):
    def process_formdata(self, valuelist):
        super(SlugField, self).process_formdata(valuelist)
        new_valuelist = []

        for value in valuelist:
            new_valuelist.append(unicode(slugify(value)))

        if new_valuelist:
            self.data = new_valuelist[0]
        else:
            self.data = ''


class UploadField(FileField):
    """
    Can render a file-upload field. Will take any passed filename value, if
    any is sent by the browser in the post params.
    """
    widget = widgets.UploadInput()


class ColorSelect(Select):
    """
    Renders a color select field.
    """
    @classmethod
    def render_option(cls, value, label, selected, **kwargs):
        data = {'data-color': label}
        options = dict(kwargs.items() + data.items())
        return super(ColorSelect, cls).render_option(value, label, selected, **options)


class TagSelectMultipleField(SelectMultipleField):
    def pre_validate(self, form):
        return True
