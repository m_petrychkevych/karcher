class YoutubeHelper:
    @classmethod
    def extract_id(cls, url):
        return url

    @classmethod
    def link(cls, id):
        if id:
            return u'https://www.youtube.com/embed/{0}?rel=0&amp;enablejsapi=1'.format(id)
        return None
