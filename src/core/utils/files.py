# coding: utf-8
import hashlib
import os
import time

from werkzeug import secure_filename


class FileHelper(object):
    @classmethod
    def generate_filename_hash(cls, filename):
        """
        Возвращает кортеж из названий файлов
        (хешированное название, оригинальное название)
        """
        hash = hashlib.sha1()
        hash.update(str(time))
        hash.update(filename)

        fname = hash.hexdigest()
        fext = cls.get_file_ext(filename)
        fdata = [fname, fext]

        return (secure_filename(''.join(fdata)), secure_filename(filename))

    @classmethod
    def get_file_ext(cls, filename):
        """ Возвращает расширение файла с точкой """
        fname, fext = os.path.splitext(filename)
        return fext

    @classmethod
    def check_file_extension(cls, filename, allowed_extensions=[]):
        return '.' in filename and filename.rsplit('.', 1)[1] in allowed_extensions

    @classmethod
    def delete_file(cls, filename):
        if os.path.isfile(filename):
            try:
                os.unlink(filename)
            except Exception as e:
                current_app.logger.exception(e)
            else:
                return True

        return False
