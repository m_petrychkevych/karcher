from jinja2 import Markup


def text_2_html(text):
    text = text.replace(u'\n\n', u'</p><p>')
    text = text.replace(u'\n', u'<br />')
    return Markup(text)
