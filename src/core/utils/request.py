from flask import session
from werkzeug.datastructures import ImmutableMultiDict


def clear_filters(workspace):
    session_filters = session.get(workspace, None)

    if session_filters:
        session.pop(workspace)


def process_filters(request, workspace, ignore_params=[]):
    session_filters = session.get(workspace, None)

    if not session_filters:
        session_filters = session.setdefault(workspace, {})

    for rkey, rvalue in request.args.items():
        if rkey not in ignore_params:
            session_filters[rkey] = rvalue

    session_filters = ImmutableMultiDict(session_filters)
    return session_filters
