# coding: utf-8
import os

from core.utils.files import FileHelper
from flask import abort, Blueprint, current_app, flash, redirect, render_template, request, url_for
from flask.ext.babel import gettext as _, lazy_gettext as __
from flask.ext.login import current_user, login_required

from karcher.site_models import Carwash, CarwashNetwork, CarwashProgram, Cleanser, FAQCategory, FAQ, MapObject, MediaCollection, NewsArticle

from .models import db
from .views import admin
from .site_forms import (CarwashForm,
                         CarwashNetworkForm,
                         CarwashProgramForm,
                         CleanserForm,
                         FAQCategoryForm,
                         FAQForm,
                         MapObjectForm,
                         MediaVideoForm,
                         MediaPhotoForm,
                         NewsArticleForm)


@admin.route('/carwash-programs')
@login_required
def carwash_programs_list():
    programs = CarwashProgram.admin_list().all()

    return render_template('admin/site/carwash_programs.html',
                           programs=programs)


@admin.route('/carwash-programs/add', methods=['GET', 'POST'])
@admin.route('/carwash-programs/<int:program_id>', methods=['GET', 'POST'])
@login_required
def manage_carwash_program(program_id=None):
    if program_id:
        program = CarwashProgram.query.filter(CarwashProgram.id.__eq__(program_id)).first()

        if not program:
            flash(_('Carwash program with id %(id)s not found message', id=program_id), 'warning')
            return redirect(url_for('admin.carwash_programs_list'))
    else:
        program = CarwashProgram()

    form = CarwashProgramForm(obj=program)

    if form.validate_on_submit():
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'carwashes')
        files_to_remove = []
        try:
            form.populate_obj(program)
            if not program_id:
                db.session.add(program)
            if program.stored_icon and os.path.isfile(os.path.join(folder, program.stored_icon)):
                files_to_remove.append(os.path.join(folder, program.stored_icon))
            if program.stored_small_icon and os.path.isfile(os.path.join(folder, program.stored_small_icon)):
                files_to_remove.append(os.path.join(folder, program.stored_small_icon))
            if form.imicon.data:
                if isinstance(form.imicon.data, basestring):
                    files_to_remove = []
                else:
                    if FileHelper.check_file_extension(form.imicon.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                        stored_filename, original_filename = FileHelper.generate_filename_hash(form.imicon.data.filename)
                        form.imicon.data.save(os.path.join(folder, stored_filename))
                        program.icon = [stored_filename, original_filename]
            else:
                program.icon = []
            if form.imsmicon.data:
                if isinstance(form.imsmicon.data, basestring):
                    files_to_remove = []
                else:
                    if FileHelper.check_file_extension(form.imsmicon.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                        stored_filename, original_filename = FileHelper.generate_filename_hash(form.imsmicon.data.filename)
                        form.imsmicon.data.save(os.path.join(folder, stored_filename))
                        program.small_icon = [stored_filename, original_filename]
            else:
                program.small_icon = []
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)
            if files_to_remove:
                for ftr in files_to_remove:
                    FileHelper.delete_file(ftr)
            flash(_("Can't save carwash program message") if program_id else _("Can't add carwash program message"), 'danger')
        else:
            flash(_('Carwash program saved successfully message') if program_id else _('Carwash program added successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_carwash_program', program_id=program_id))
        return redirect(url_for('admin.carwash_programs_list'))

    return render_template('admin/site/manage_carwash_program.html',
                           program=program,
                           program_id=program_id,
                           form=form)


@admin.route('/carwash-networks')
@login_required
def carwash_networks_list():
    networks = CarwashNetwork.admin_list().all()

    return render_template('admin/site/carwash_networks.html',
                           networks=networks)


@admin.route('/carwash-networks/add', methods=['GET', 'POST'])
@admin.route('/carwash-networks/<int:network_id>', methods=['GET', 'POST'])
@login_required
def manage_carwash_network(network_id=None):
    if network_id:
        network = CarwashNetwork.query.filter(CarwashNetwork.id.__eq__(network_id)).first()
        if not network:
            flash(_('Carwash network id %(id)s not found message', id=network_id), 'warning')
            return redirect(url_for('admin.carwash_networks_list'))
    else:
        network = CarwashNetwork()

    form = CarwashNetworkForm(obj=network)

    if form.validate_on_submit():
        try:
            form.populate_obj(network)
            if not network_id:
                db.session.add(network)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)
            flash(_("Can't save carwash network message") if network_id else _("Can't add carwash network message"), 'danger')
        else:
            flash(_('Carwash network saved successfully message') if network_id else _('Carwash network added successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_carwash_network', network_id=network_id))
        return redirect(url_for('admin.carwash_networks_list'))

    return render_template('admin/site/manage_carwash_network.html',
                           network=network,
                           network_id=network_id,
                           form=form)


@admin.route('/carwashes')
@login_required
def carwashes_list():
    carwashes = Carwash.admin_list().all()

    return render_template('admin/site/carwashes.html',
                           carwashes=carwashes)


@admin.route('/carwashes/add', methods=['GET', 'POST'])
@admin.route('/carwashes/<int:carwash_id>', methods=['GET', 'POST'])
@login_required
def manage_carwash(carwash_id=None):
    if carwash_id:
        carwash = Carwash.query.filter(Carwash.id.__eq__(carwash_id)).first()

        if not carwash:
            flash(_('Carwash with id %(id)s not found message', id=carwash_id), 'warning')
            return redirect(url_for('admin.carwashes_list'))
    else:
        carwash = Carwash()

    form = CarwashForm(obj=carwash)

    if form.validate_on_submit():
        try:
            form.populate_obj(carwash)
            if not carwash_id:
                db.session.add(carwash)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)

            flash(_("Can't save carwash message") if carwash_id else _("Can't add carwash message"), 'danger')
        else:
            flash(_('Carwash saved successfully message') if carwash_id else _('Carwash added successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_carwash', carwash_id=carwash.id))
        return redirect(url_for('admin.carwashes_list'))

    return render_template('admin/site/manage_carwash.html',
                           carwash=carwash,
                           carwash_id=carwash_id,
                           form=form)


@admin.route('/map-objects')
@login_required
def map_objects_list():
    map_objects = MapObject.admin_list().all()

    return render_template('admin/site/map_objects.html',
                           map_objects=map_objects)


@admin.route('/map-objects/add', methods=['GET', 'POST'])
@admin.route('/map-objects/<int:map_object_id>', methods=['GET', 'POST'])
@login_required
def manage_map_object(map_object_id=None):
    if map_object_id:
        map_object = MapObject.query.filter(MapObject.id.__eq__(map_object_id)).first()

        if not map_object:
            flash(_('Map object with id %(id)s not found message', id=map_object_id), 'warning')
            return redirect(url_for('admin.map_objects_list'))
    else:
        map_object = MapObject()

    form = MapObjectForm(obj=map_object)

    if form.validate_on_submit():
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'map_objects')
        file_to_remove = None
        try:
            form.populate_obj(map_object)
            if not map_object_id:
                db.session.add(map_object)
            if map_object.stored_photo and os.path.isfile(os.path.join(folder, map_object.stored_image)):
                file_to_remove = os.path.join(folder, map_object.stored_image)
            if form.mophoto.data:
                if isinstance(form.mophoto.data, basestring):
                    file_to_remove = False
                else:
                    if FileHelper.check_file_extension(form.mophoto.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                        stored_filename, original_filename = FileHelper.generate_filename_hash(form.mophoto.data.filename)
                        form.mophoto.data.save(os.path.join(folder, stored_filename))
                        map_object.photo = [stored_filename, original_filename]
            else:
                map_object.photo = []
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)
            if file_to_remove:
                FileHelper.delete_file(file_to_remove)
            flash(_("Can't save map object message") if map_object_id else _("Can't add map object message"), 'danger')
        else:
            flash(_('Map object saved successfully message') if map_object_id else _('Map object added successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_map_object', map_object_id=map_object.id))
        return redirect(url_for('admin.map_objects_list'))

    return render_template('admin/site/manage_map_object.html',
                           map_object=map_object,
                           map_object_id=map_object_id,
                           form=form)


faq_tabs = [dict(alias='general', title=__('FAQ category general tab')),
            dict(alias='questions', title=__('FAQ category questions tab'))]


@admin.route('/faq', methods=['GET', 'POST'])
@login_required
def faq_categories_list():
    faq_categories = FAQCategory.admin_list().all()

    if request.method == 'POST':
        positions = request.form.getlist('positions', type=int)
        to_remove = request.form.getlist('toremove', type=int)
        trash_selected = 'trash_selected' in request.form
        if positions and not trash_selected:
            try:
                for fc in faq_categories:
                    if fc.id in positions:
                        fc.position = positions.index(fc.id) + 1
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash(_("Can't save FAQ categories order message"), 'danger')
            else:
                flash(_('FAQ categories order saved successfully message'), 'success')
        if to_remove and trash_selected:
            try:
                cats = {fc.id: fc for fc in faq_categories}
                for tr in to_remove:
                    obj = cats.get(tr)
                    if obj and obj.common_ is False:
                        db.session.delete(obj)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash(_("Can't delete selected FAQ categories message"), 'danger')
            else:
                flash(_('Selected FAQ categories deleted successfully message'), 'success')
        return redirect(url_for('admin.faq_categories_list'))

    return render_template('admin/site/faq_categories.html',
                           faq_categories=faq_categories)


@admin.route('/faq/add', methods=['GET', 'POST'])
@admin.route('/faq/<int:faq_category_id>', methods=['GET', 'POST'])
@admin.route('/faq/<int:faq_category_id>/<any(general, questions):active_tab>', methods=['GET', 'POST'])
@login_required
def manage_faq_category(faq_category_id=None, active_tab='general'):
    if faq_category_id:
        faq_category = FAQCategory.query.filter(FAQCategory.id.__eq__(faq_category_id)).first()

        if not faq_category:
            flash(_('FAQ category with id %(id)s not found message', id=faq_category_id), 'warning')
            return redirect(url_for('admin.faq_categories_list'))
    else:
        faq_category = FAQCategory()

    form = None
    questions = []

    if active_tab == 'general':
        form = FAQCategoryForm(obj=faq_category)
    elif active_tab == 'questions':
        questions = faq_category.questions

    if request.method == 'POST':
        if active_tab == 'general':
            if form and form.validate_on_submit():
                try:
                    form.populate_obj(faq_category)
                    if not faq_category_id:
                        db.session.add(faq_category)
                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    current_app.logger.exception(e)
                    flash(_("Can't save faq category message") if faq_category_id else _("Can't add faq category message"), 'danger')
                else:
                    flash(_('FAQ category saved successfully message') if faq_category_id else _('FAQ category added successfully message'), 'success')

                if 'submit_and_continue' in request.form:
                    return redirect(url_for('admin.manage_faq_category', faq_category_id=faq_category_id))
                return redirect(url_for('admin.faq_categories_list'))

        elif active_tab == 'questions':
            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_faq_category', faq_category_id=faq_category_id))
            return redirect(url_for('admin.faq_categories_list'))

    return render_template('admin/site/manage_faq_category.html',
                           faq_category=faq_category,
                           faq_category_id=faq_category_id,
                           form=form,
                           questions=questions,
                           tabs=faq_tabs,
                           active_tab=active_tab)


@admin.route('/faq/<int:faq_category_id>/questions/add', methods=['GET', 'POST'])
@admin.route('/faq/<int:faq_category_id>/questions/<int:question_id>', methods=['GET', 'POST'])
@login_required
def manage_faq_question(faq_category_id, question_id=None):
    if question_id:
        question = FAQ.query.filter(FAQ.id.__eq__(question_id)).first()
        if not question:
            flash(_('FAQ question with id %(id)s not found message', id=question_id), 'warning')
            return redirect(url_for('FAQ question with id %(id)s not found message', id=question_id), 'warning')
        faq_category = question.category
        if not faq_category:
            flash(_('FAQ category with id %(id)s not found message', id=faq_category_id), 'warning')
            return redirect(url_for('admin.faq_categories_list'))
        if question.category_id != faq_category_id:
            flash(_('FAQ question in another category message', id=question_id), 'info')
            return redirect(url_for('admin.manage_faq_question', faq_category_id=faq_category.id, question_id=question_id))
    else:
        faq_category = FAQCategory.query.filter(FAQCategory.id.__eq__(faq_category_id)).first()
        if not faq_category:
            flash(_('FAQ category with id %(id)s not found message', id=faq_category_id), 'warning')
            return redirect(url_for('admin.faq_categories_list'))
        question = FAQ(category=faq_category)

    form = FAQForm(obj=question)

    if form.validate_on_submit():
        try:
            form.populate_obj(question)
            if not question_id:
                db.session.add(question)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)
            flash(_("Can't save faq question message") if question_id else _("Can't add faq question message"), 'danger')
        else:
            flash(_('FAQ question saved successfully message') if question_id else _('FAQ question added successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_faq_question', faq_category_id=question.category_id, question_id=question.id))
        return redirect(url_for('admin.manage_faq_category', faq_category_id=question.category_id, active_tab='questions'))

    return render_template('admin/site/manage_faq_question.html',
                           faq_category=faq_category,
                           faq_category_id=faq_category_id,
                           question=question,
                           question_id=question_id,
                           form=form,
                           active_tab='questions')


@admin.route('/news')
@login_required
def news_list():
    news = NewsArticle.admin_list().all()

    return render_template('admin/site/news.html',
                           news=news)


@admin.route('/news/add', methods=['GET', 'POST'])
@admin.route('/news/<int:news_article_id>', methods=['GET', 'POST'])
@login_required
def manage_news_article(news_article_id=None):
    if news_article_id:
        news_article = NewsArticle.query.filter(NewsArticle.id.__eq__(news_article_id)).first()

        if not news_article:
            flash(_('News article with id %(id)s not found message', id=news_article_id), 'warning')
            return redirect(url_for('admin.news_list'))
    else:
        news_article = NewsArticle()

    form = NewsArticleForm(obj=news_article)

    if form.validate_on_submit():
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'news')
        file_to_remove = None
        try:
            form.populate_obj(news_article)
            if not news_article_id:
                db.session.add(news_article)
            if news_article.stored_image and os.path.isfile(os.path.join(folder, news_article.stored_image)):
                file_to_remove = os.path.join(folder, news_article.stored_image)
            if form.an_image.data:
                if isinstance(form.an_image.data, basestring):
                    file_to_remove = False
                else:
                    if FileHelper.check_file_extension(form.an_image.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                        stored_filename, original_filename = FileHelper.generate_filename_hash(form.an_image.data.filename)
                        form.an_image.data.save(os.path.join(folder, stored_filename))
                        news_article.announcement_image = [stored_filename, original_filename]
            else:
                news_article.announcement_image = []
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)
            if file_to_remove:
                FileHelper.delete_file(file_to_remove)
            flash(_("Can't save news article message") if news_article_id else _("Can't add news article message"), 'danger')
        else:
            flash(_('News article saved successfully message') if news_article_id else _('News article added successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_news_article', news_article_id=news_article.id))
        return redirect(url_for('admin.news_list'))

    return render_template('admin/site/manage_news_article.html',
                           news_article=news_article,
                           news_article_id=news_article_id,
                           form=form)


@admin.route('/media')
@login_required
def media_list():
    media = MediaCollection.admin_list().all()

    return render_template('admin/site/media.html',
                           media=media)


@admin.route('/media/<any(video,photo):type_>/add', methods=['GET', 'POST'])
@admin.route('/media/<any(video,photo):type_>/<int:media_id>', methods=['GET', 'POST'])
@login_required
def manage_media(type_, media_id=None):
    if media_id:
        media = MediaCollection.query.filter(MediaCollection.id.__eq__(media_id)).first()
        if not media:
            flash(_('Media with id %(id)s not found message', id=cleanser_id), 'warning')
            return redirect(url_for('admin.media_list'))
    else:
        media = MediaCollection()

    if type_ == 'video':
        form = MediaVideoForm(obj=media)
    elif type_ == 'photo':
        form = MediaPhotoForm(obj=media)

    if form.validate_on_submit():
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'media')
        file_to_remove = None
        try:
            form.populate_obj(media)
            if not media_id:
                db.session.add(media)
            if type_ == 'photo' and 'mphoto' in form:
                if form.mphoto.data:
                    if isinstance(form.mphoto.data, basestring):
                        file_to_remove = False
                    else:
                        if FileHelper.check_file_extension(form.mphoto.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                            stored_filename, original_filename = FileHelper.generate_filename_hash(form.mphoto.data.filename)
                            form.mphoto.data.save(os.path.join(folder, stored_filename))
                            media.image = [stored_filename, original_filename]
                else:
                    media.image = []
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)
            if file_to_remove:
                FileHelper.delete_file(file_to_remove)
            flash(_("Can't save media message") if media_id else _("Can't add media message"), 'error')
        else:
            flash(_('Media saved successfully message') if media_id else _('Media added successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_media', type_=type_, media_id=media.id))
        return redirect(url_for('admin.media_list'))

    return render_template('admin/site/manage_media.html',
                           media=media,
                           media_id=media_id,
                           type_=type_,
                           form=form)


@admin.route('/cleansers')
@login_required
def cleansers_list():
    cleansers = Cleanser.admin_list().all()

    return render_template('admin/site/cleansers.html',
                           cleansers=cleansers)


@admin.route('/cleansers/add', methods=['GET', 'POST'])
@admin.route('/cleansers/<int:cleanser_id>', methods=['GET', 'POST'])
@login_required
def manage_cleanser(cleanser_id=None):
    if cleanser_id:
        cleanser = Cleanser.query.filter(Cleanser.id.__eq__(cleanser_id)).first()
        if not cleanser:
            flash(_('Cleanser with id %(id)s not found message', id=cleanser_id), 'warning')
            return redirect(url_for('admin.cleansers_list'))
    else:
        cleanser = Cleanser()

    form = CleanserForm(obj=cleanser)

    if form.validate_on_submit():
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'cleansers')
        file_to_remove = None
        try:
            form.populate_obj(cleanser)
            if not cleanser_id:
                db.session.add(cleanser)
            if form.cphoto.data:
                if isinstance(form.cphoto.data, basestring):
                    file_to_remove = False
                else:
                    if FileHelper.check_file_extension(form.cphoto.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                        stored_filename, original_filename = FileHelper.generate_filename_hash(form.cphoto.data.filename)
                        form.cphoto.data.save(os.path.join(folder, stored_filename))
                        cleanser.photo = [stored_filename, original_filename]
            else:
                cleanser.photo = []
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)
            if file_to_remove:
                FileHelper.delete_file(file_to_remove)
            flash(_("Can't save cleanser message") if cleanser_id else _("Can't add cleanser message"), 'error')
        else:
            flash(_('Cleanser saved successfully message') if cleanser_id else _('Cleanser added successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_cleanser', cleanser_id=cleanser.id))
        return redirect(url_for('admin.cleansers_list'))

    return render_template('admin/site/manage_cleanser.html',
                           cleanser=cleanser,
                           cleanser_id=cleanser_id,
                           form=form)
