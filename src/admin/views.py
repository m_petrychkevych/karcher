# coding: utf-8
import json
import os
import re

from core.forms.utils import is_empty
from core.utils.files import FileHelper
from core.utils.request import clear_filters, process_filters
from flask import abort, Blueprint, current_app, flash, redirect, render_template, request, send_from_directory, url_for
from flask.ext.babel import gettext as _, lazy_gettext as __
from flask.ext.login import current_user, login_required, login_user, logout_user
from werkzeug import secure_filename

from karcher.models import EmbeddedPage, Page, PageBlock
from karcher.models import NavTypeConverter, NavHelper
from karcher.models import NavigationGroup, NavigationGroupItem, NavigationGroupPopupItem
from karcher.models import NavTypeConverter
from users.forms import UserFiltersForm, UserForm
from users.models import roles, user_created, User, UserProfile

from .forms import (LoginForm,
                    PageBlockForm,
                    PageForm)
from .forms import NavigationGroupForm, NavigationSwitchtypeForm, NavigationGroupItemForm, NavigationGroupPopupItemForm
from .forms import FileUploadForm
from .models import db, rbac, Role


admin = Blueprint('admin',
                  __name__,
                  template_folder='templates',
                  static_folder='static',
                  url_prefix='/admin')

admin.add_app_url_converter('navtype', NavTypeConverter)
current_app.login_manager.login_view = 'admin.login'

pages_tabs = [dict(alias='general', title=__('Page general tab')),
              dict(alias='blocks', title=__('Page blocks tab'))]


@admin.route('/')
@login_required
def dashboard():
    return render_template('admin/dashboard.html')


@admin.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated():
        return redirect(url_for('admin.dashboard'))

    login_form = LoginForm() if request.method == 'POST' else LoginForm(request.args)

    if login_form.validate_on_submit():
        if login_user(login_form.user, remember=login_form.remember_me.data) is True:
            flash(_('Logeed in successfully message'))
        else:
            flash(_('Logging failed message'))

        return redirect(login_form.next.data or url_for('admin.dashboard'))

    return render_template('admin/login.html',
                           login_form=login_form)


@admin.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(request.args.get('next') or url_for('admin.dashboard'))


@admin.route('/users/clear_filters')
@login_required
def users_list_clear_filters():
    clear_filters('user_filters')
    return redirect(url_for('admin.users_list'))


@admin.route('/users')
@login_required
def users_list():
    session_filters = process_filters(request, 'user_filters')
    filters_form = UserFiltersForm(session_filters)
    users, total = User.admin_list(filters_form=filters_form, with_total=False)
    # print(is_empty(filters_form), filters_form.data)  # DEBUG

    return render_template('admin/users.html',
                           users=users,
                           filters_form=filters_form,
                           filtered_list=not is_empty(filters_form))


@admin.route('/users/add', methods=['GET', 'POST'])
@admin.route('/users/<int:user_id>', methods=['GET', 'POST'])
@login_required
def manage_user(user_id=None):
    data = dict()
    is_current_user = current_user.id == user_id

    if user_id:
        user = User.query.get(user_id) if not is_current_user else current_user

        if not user:
            flash(_('User with %(user_id)s not found message', user_id=user_id), 'warning')
            return redirect(url_for('admin.users_list'))

        profile = user.profile
        data = dict(user.__dict__.items() + profile.__dict__.items())
        assigned_roles = rbac.assignedRoles(user.login)

        if assigned_roles:
            data['roles'] = roles.filter(Role.id.in_([role.id for role in rbac.assignedRoles(user.login)]))
    else:
        user = User()
        profile = UserProfile(user=user)

    data['update'] = True if user_id else False
    data['user_id'] = user_id
    data['is_current_user'] = is_current_user
    user_form = UserForm(**data)

    if user_form.validate_on_submit():
        try:
            user_form.populate_obj(user)
            user_form.populate_obj(profile)
            user.system = True

            if not user_id:
                db.session.add(user)
                db.session.add(profile)
                db.session.flush()
            else:
                assigned = [role.id for role in rbac.assignedRoles(user.login)]

                for rid in assigned:
                    rbac.deassignUser(user.login, rid)

                for role in user_form.roles.data:
                    rbac.assignUser(user.login, role.id)

            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)

            if user_id:
                flash(_("Can't edit user %(login)s message", login=user.login), 'danger')
            else:
                flash(_("Can't register user %(login)s message", login=user.login), 'danger')
        else:
            if not user_id:
                user_created.send(current_app, user=user, roles=user.roles or [])
                flash(_('User %(login)s added successfully message', login=user.login), 'success')
            else:
                flash(_('User %(login)s saved successfully message', login=user.login), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_user', user_id=user.id))

        return redirect(url_for('admin.users_list'))

    return render_template('admin/manage_user.html',
                           user_id=user_id,
                           user=user,
                           user_form=user_form)


@admin.route('/pages')
@login_required
def pages_list():
    pages = Page.admin_list().all()

    return render_template('admin/pages.html',
                           pages=pages)


@admin.route('/pages/<int:page_id>/edit')
@login_required
def manage_page_content(page_id):
    page = Page.query.get(page_id)

    if not page:
        flash(_('Page with id %(id)s not found message', id=page_id), 'warning')
        return redirect(url_for('admin.pages_list'))

    return page.render(editable=True)


@admin.route('/api/upload', methods=['POST'])
@login_required
def ajaxapi_upload_image():
    form = FileUploadForm(request.files)

    try:
        if form.validate():
            folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'blocks_ct')
            if form.upload.data:
                if not isinstance(form.upload.data, basestring):
                    if FileHelper.check_file_extension(form.upload.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                        filename = secure_filename(form.upload.data.filename)
                        form.upload.data.save(os.path.join(folder, filename))
    except Exception as e:
        current_app.logger.exception(e)
    else:
        if filename:
            return send_from_directory(folder, filename)
            return url_for('public_dir', filename=os.path.join(folder, filename))

    abort(404)


@admin.route('/pages/<int:page_id>/api/save', methods=['POST'])
@login_required
def ajaxapi_page_content_save(page_id):
    """ Save pageblock data by ajax. """
    response_data = {'global_result': {'status': 0, 'status_msg': ''}}

    if request.is_xhr:
        page = Page.query.get(page_id)

        if not page:
            response_data['global_result']['status'] = 404
            response_data['global_result']['status_msg'] = _('Page %(id)s not found message', id=page_id)
            return json.dumps(response_data)

        request_data = request.get_json(force=True)

        if request_data:
            response_data.setdefault('blocks', {})

            for block, detail in request_data.items():
                response_data['blocks'].setdefault(block, {'status': 0, 'status_msg': '', 'fields': {}})
                page_block = None
                block_id = detail.get('id', None)

                if block_id:
                    page_block = PageBlock.query.get(block_id)

                if not page_block:
                    response_data['blocks'][block]['status'] = 404
                    response_data['blocks'][block]['status_msg'] = _('Block %(id)s not found message', id=block_id)
                    continue

                # Save
                data_to_save = {field_name: field_content.get('content') for field_name, field_content in detail.get('fields', {}).items()}

                for field_name in detail.get('fields', {}).keys():
                    response_data['blocks'][block]['fields'].setdefault(field_name, {'status': 0, 'status_msg': ''})

                pb_data = page_block.get_data_object()

                if pb_data:
                    for field_name, field_content in data_to_save.items():
                        if hasattr(pb_data, field_name):
                            try:
                                setattr(pb_data, field_name, field_content)
                                db.session.commit()
                            except Exception as e:
                                db.session.rollback()
                                current_app.logger.exception(e)
                                response_data['blocks'][block]['fields'][field_name]['status'] = 500
                                response_data['blocks'][block]['fields'][field_name]['status_msg'] = _("Can't save block field %(name)s message", name=field_name)
                        else:
                            response_data['blocks'][block]['fields'][field_name]['status'] = 404
                            response_data['blocks'][block]['fields'][field_name]['status_msg'] = _('Block field %(name)s not found message', name=field_name)

        return json.dumps(response_data)

    response_data['global_result']['status'] = 403
    return json.dumps(response_data)


@admin.route('/pages/add', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>/<any(general, blocks):active_tab>', methods=['GET', 'POST'])
@login_required
def manage_page(page_id=None, active_tab='general'):
    if page_id:
        query = Page.query.filter(Page.id.__eq__(page_id))
        page = query.first()

        if not page:
            flash(_('Page with id %(id)s not found message', id=page_id), 'warning')
            return redirect(url_for('admin.pages_list'))
    else:
        page = Page()

    form = None

    if active_tab == 'general':
        form = PageForm(obj=page)

    if form and form.validate_on_submit():
        try:
            form.populate_obj(page)

            if not page_id:
                db.session.add(page)

            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)

            if not page_id:
                flash(_("Can't add page message"), 'danger')
            else:
                flash(_("Can't save page message"), 'danger')
        else:
            if not page_id:
                flash(_('Page added successfully message'), 'success')
            else:
                flash(_('Page saved successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_page', page_id=page.id))

        return redirect(url_for('admin.pages_list'))

    if not form and request.method == 'POST':
        if active_tab == 'blocks':
            positions = request.form.getlist('positions', type=int)
            to_remove = request.form.getlist('toremove', type=int)
            trash_selected = 'trash_selected' in request.form

            if positions and not trash_selected:
                try:
                    for b in page.blocks:
                        if b.id in positions:
                            b.position = positions.index(b.id) + 1

                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    current_app.logger.exception(e)
                else:
                    flash(_('Blocks order saved successfully message'), 'success')

            if to_remove and trash_selected:
                try:
                    blocks = {b.id: b for b in page.blocks}

                    for tr in to_remove:
                        obj = blocks.get(tr)

                        if obj:
                            if obj.status == PageBlock.statusActive:  # Mark as Trashed if active
                                obj.status = PageBlock.statusTrash
                            elif obj.status == PageBlock.statusTrash:  # Physically remove object if trashed
                                db.session.delete(obj)

                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    current_app.logger.exception(e)
                else:
                    flash(_('Selected pageblocks moved to trash message'), 'success')

        return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    return render_template('admin/manage_page.html',
                           page=page,
                           page_id=page_id,
                           form=form,
                           tabs=pages_tabs,
                           active_tab=active_tab)


@admin.route('/pages/<int:page_id>/<any(blocks):active_tab>/<int:pageblock_id>/data/<string:active_subtab>/<int:epage_id>/<int:epageblock_id>/data', methods=['GET', 'POST'])
@login_required
def manage_epageblock_data(page_id, pageblock_id, epage_id, epageblock_id=None, active_tab='blocks', active_subtab=None):
    """
    Редактирование данных встроенных блоков.
    """
    page, pageblock = _pageblock_helper(page_id, pageblock_id, active_tab)
    epage = EmbeddedPage.query.get(epage_id)

    if not epage:
        flash(_('Embedded page with id %(id)s not found message', id=epage_id), 'warning')
        return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

    query = PageBlock.query.filter(PageBlock.id.__eq__(epageblock_id))
    epageblock = query.first()

    if not epageblock:
        flash(_('Embedded pageblock with id %(id)s not found message', id=epageblock_id), 'warning')
        return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

    if epageblock.embedded_page != epage:
        flash(_('Embedded pageblock with id %(id)s not found in current embedded page message', id=epageblock_id), 'warning')
        return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

    pb_data = epageblock.get_data_object()
    epageblock_form = epageblock.get_form()

    if epageblock_form:
        form = epageblock_form(obj=pb_data)

        if form and form.validate_on_submit():
            try:
                form.populate_obj(pb_data)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash(_("Can't save embedded pageblock data message"), 'danger')
            else:
                flash(_('Embedded pageblock data saved successfully message'), 'success')

            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_epageblock_data',
                                page_id=page_id,
                                pageblock_id=pageblock_id,
                                epage_id=epage_id,
                                epageblock_id=epageblock_id,
                                active_tab=active_tab,
                                active_subtab=active_subtab))

            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))
        elif not form and request.method == 'POST':
            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock.id))

            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    return render_template('admin/manage_pageblock_data.html',
                           page=page,
                           page_id=page_id,
                           pageblock=pageblock,
                           pageblock_id=pageblock_id,
                           epage=epage,
                           epage_id=epage_id,
                           epageblock=epageblock,
                           epageblock_id=epageblock_id,
                           form=form,
                           active_tab=active_tab,
                           active_subtab=active_subtab)


def _pageblock_helper(page_id, pageblock_id, active_tab):
    page = Page.query.get(page_id)

    if not page:
        flash(_('Page with id %(id)s not found message', id=page_id), 'warning')
        return redirect(url_for('admin.pages_list'))

    query = PageBlock.query.filter(PageBlock.id.__eq__(pageblock_id))
    pageblock = query.first()

    if not pageblock:
        flash(_('Pageblock with id %(id)s not found message', id=pageblock_id), 'warning')
        return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    if pageblock.page:
        if pageblock.page != page:
            flash(_('Pageblock with id %(id)s not found in current page message', id=pageblock_id), 'warning')
            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    return page, pageblock


@admin.route('/pages/<int:page_id>/<string:active_tab>/<int:pageblock_id>/data/<string:active_subtab>/<int:epage_id>/add', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>/<string:active_tab>/<int:pageblock_id>/data/<string:active_subtab>/<int:epage_id>/<int:epageblock_id>', methods=['GET', 'POST'])
@login_required
def manage_epageblock(page_id, pageblock_id, epage_id, epageblock_id=None, active_tab=None, active_subtab=None):
    """
    Редактирование/добавление блоков в embedded page.
    """
    page, pageblock = _pageblock_helper(page_id, pageblock_id, active_tab)
    epage = EmbeddedPage.query.get(epage_id)

    if not epage:
        flash(_('Embedded page with id %(id)s not found message', id=epage_id), 'warning')
        return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

    if epageblock_id:
        query = PageBlock.query.filter(PageBlock.id.__eq__(epageblock_id))
        epageblock = query.first()

        if not epageblock:
            flash(_('Embedded pageblock with id %(id)s not found message', id=epageblock_id), 'warning')
            return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

        if epageblock.embedded_page != epage:
            flash(_('Embedded pageblock with id %(id)s not found in current embedded page message', id=epageblock_id), 'warning')
            return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))
    else:
        epageblock = PageBlock()

    form = PageBlockForm(obj=epageblock, update=True if epageblock_id else False, embedded=True)

    if form.validate_on_submit():
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'blocks_bg')
        file_to_remove = None

        try:
            form.populate_obj(epageblock)

            if not epageblock_id:
                epageblock.embedded_page = epage
                db.session.add(epageblock)
                db.session.flush()
                attrs = epageblock.create_all_data()
                db.session.add(attrs)

            if epageblock.background_image and os.path.isfile(os.path.join(folder, epageblock.stored_background_image)):
                file_to_remove = os.path.join(folder, epageblock.stored_background_image)

            if form.bimage.data:
                if isinstance(form.bimage.data, basestring):
                    file_to_remove = False
                else:
                    if FileHelper.check_file_extension(form.bimage.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                        stored_filename, original_filename = FileHelper.generate_filename_hash(form.bimage.data.filename)
                        form.bimage.data.save(os.path.join(folder, stored_filename))
                        epageblock.background_image = [stored_filename, original_filename]
            else:
                epageblock.background_image = []

            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)

            if not epageblock_id:
                flash(_("Can't add embedded pageblock message"), 'danger')
            else:
                flash(_("Can't save embedded pageblock message"), 'danger')
        else:
            if file_to_remove:
                try:
                    os.unlink(file_to_remove)
                except Exception as e:
                    current_app.logger.exception(e)

            if not epageblock_id:
                flash(_('Embedded pageblock added successfully message'), 'success')
            else:
                flash(_('Embedded pageblock saved successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_epageblock', page_id=page_id, pageblock_id=pageblock_id, active_tab=active_tab, epage_id=epage_id, epageblock_id=epageblock_id, active_subtab=active_subtab))

        return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

    return render_template('admin/manage_pageblock.html',
                           embedded=True,
                           page_id=page_id,
                           page=page,
                           pageblock_id=pageblock_id,
                           pageblock=pageblock,
                           epage=epage,
                           epage_id=epage_id,
                           epageblock=epageblock,
                           epageblock_id=epageblock_id,
                           form=form,
                           active_tab=active_tab,
                           active_subtab=active_subtab)


@admin.route('/pages/<int:page_id>/blocks/add', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>', methods=['GET', 'POST'])
@login_required
def manage_pageblock(page_id, pageblock_id=None):
    active_tab = 'blocks'
    page = Page.query.get(page_id)

    if not page:
        flash(_('Page with id %(id)s not found message', id=page_id), 'warning')
        return redirect(url_for('admin.pages_list'))

    if pageblock_id:
        query = PageBlock.query.filter(PageBlock.id.__eq__(pageblock_id))
        pageblock = query.first()

        if not pageblock:
            flash(_('Pageblock with id %(id)s not found message', id=pageblock_id), 'warning')
            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

        if pageblock.page != page:
            flash(_('Pageblock with id %(id)s not found in current page message', id=pageblock_id), 'warning')
            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))
    else:
        pageblock = PageBlock()

    form = PageBlockForm(obj=pageblock, update=True if pageblock_id else False)

    if form.validate_on_submit():
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'blocks_bg')
        file_to_remove = None

        try:
            form.populate_obj(pageblock)

            if not pageblock_id:
                pageblock.page = page
                db.session.add(pageblock)
                db.session.flush()
                attrs = pageblock.create_all_data()
                db.session.add(attrs)

            # Mark old file to remove first
            if pageblock.stored_background_image and os.path.isfile(os.path.join(folder, pageblock.stored_background_image)):
                file_to_remove = os.path.join(folder, pageblock.stored_background_image)

            if form.bimage.data:
                if isinstance(form.bimage.data, basestring):
                    file_to_remove = False
                else:
                    if FileHelper.check_file_extension(form.bimage.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                        stored_filename, original_filename = FileHelper.generate_filename_hash(form.bimage.data.filename)
                        form.bimage.data.save(os.path.join(folder, stored_filename))
                        pageblock.background_image = [stored_filename, original_filename]
            else:
                pageblock.background_image = []

            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)

            if not page_id:
                flash(_("Can't add pageblock message"), 'danger')
            else:
                flash(_("Can't save pageblock message"), 'danger')
        else:
            if file_to_remove:
                try:
                    os.unlink(file_to_remove)
                except Exception as e:
                    current_app.logger.exception(e)

            if not pageblock_id:
                flash(_('Pageblock added successfully message'), 'success')
            else:
                flash(_('Pageblock saved successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_pageblock', page_id=page_id, pageblock_id=pageblock.id))

        return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    return render_template('admin/manage_pageblock.html',
                           page=page,
                           page_id=page_id,
                           pageblock=pageblock,
                           pageblock_id=pageblock_id,
                           form=form,
                           active_tab=active_tab)


@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>/data', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>/data/<string:active_subtab>', methods=['GET', 'POST'])
@login_required
def manage_pageblock_data(page_id, pageblock_id, active_tab='blocks', active_subtab=None):
    """
    Редактирование данных блоков.
    """
    page = Page.query.get(page_id)

    if not page:
        flash(_('Page with id %(id)s not found message', id=page_id), 'warning')
        return redirect(url_for('admin.pages_list'))

    query = PageBlock.query.filter(PageBlock.id.__eq__(pageblock_id))
    pageblock = query.first()

    if not pageblock:
        flash(_('Pageblock with id %(id)s not found message', id=pageblock_id), 'warning')
        return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    if pageblock.page:
        if pageblock.page != page:
            flash(_('Pageblock with id %(id)s not found in current page message', id=pageblock_id), 'warning')
            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    pb_data = pageblock.get_data_object()
    pageblock_form = pageblock.get_form()
    form = None

    if pageblock_form:
        form = pageblock_form(obj=pb_data)

        if form and form.validate_on_submit():
            try:
                form.populate_obj(pb_data)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash(_("Can't save pageblock data message"), 'danger')
            else:
                flash(_('Pageblock data saved successfully message'), 'success')

            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock.id))

            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))
        elif not form and request.method == 'POST':
            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock.id))

            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

        return render_template('admin/manage_pageblock_data.html',
                               page=page,
                               page_id=page_id,
                               pageblock=pageblock,
                               pageblock_id=pageblock_id,
                               form=form,
                               active_tab=active_tab)

    # Если стандартной формы у блока нет, проверим есть ли у нее свое представление
    kwargs = {}

    if active_subtab:
        kwargs.setdefault('active_subtab', active_subtab)

    custom_manage_view = pageblock.custom_manage_view(page=page,
                                                      page_id=page_id,
                                                      pageblock=pageblock,
                                                      pageblock_id=pageblock_id,
                                                      active_tab=active_tab,
                                                      **kwargs)

    if custom_manage_view:
        return custom_manage_view

    return render_template('admin/manage_pageblock_data.html',
                           page=page,
                           page_id=page_id,
                           pageblock=pageblock,
                           pageblock_id=pageblock_id)


@admin.route('/navigation/switch')
@login_required
def navigation_switch_type():
    switchtype_form = NavigationSwitchtypeForm(request.args)
    return redirect(url_for('admin.navigation_list', nav_type=switchtype_form.nav_type.data))


@admin.route('/navigation/<navtype:nav_type>', methods=['GET', 'POST'])
@login_required
def navigation_list(nav_type):
    nav_groups = NavigationGroup.admin_list(navtype=nav_type).all()
    switchtype_form = NavigationSwitchtypeForm(data={'nav_type': nav_type})

    if request.method == 'POST':
        positions = request.form.getlist('positions', type=int)
        to_remove = request.form.getlist('toremove', type=int)
        trash_selected = 'trash_selected' in request.form

        if positions and not trash_selected:
            try:
                for group in nav_groups:
                    if group.id in positions:
                        group.position = positions.index(group.id) + 1
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash(_("Can't save navigation groups order message"), 'error')
            else:
                flash(_('Navigation groups order saved successfully'), 'success')

        if to_remove and trash_selected:
            try:
                groups = {group.id: group for group in nav_groups}

                for tr in to_remove:
                    obj = groups.get(tr)
                    if obj:
                        db.session.delete(obj)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash(_("Can't delete selected navigation groups message"), 'error')
            else:
                flash(_('Selected navigation groups was deleted message'), 'success')

        return redirect(url_for('admin.navigation_list', nav_type=nav_type))
    return render_template('admin/nav_groups.html',
                           switchtype_form=switchtype_form,
                           nav_groups=nav_groups,
                           nav_type=nav_type)


@admin.route('/navigation/<navtype:nav_type>/add', methods=['GET', 'POST'])
@admin.route('/navigation/<navtype:nav_type>/<int:group_id>', methods=['GET', 'POST'])
@login_required
def manage_navigation_group(nav_type, group_id=None, active_tab='general'):
    items_tab = False
    popup_items_tab = False

    if group_id:
        query = NavigationGroup.query
        query = query.filter(NavigationGroup.navtype_.__eq__(nav_type))
        query = query.filter(NavigationGroup.id.__eq__(group_id))
        group = query.first()

        if not group:
            flash(_('Group with id %(id)s not found message', id=group_id), 'warning')
            return redirect(url_for('admin.navigation_list', nav_type=nav_type))

        items_tab = group.behavior_ == NavHelper.behaviorOnlyHeader
        popup_items_tab = group.behavior_ == NavHelper.behaviorPopup
    else:
        group = NavigationGroup(language=current_app.config['BABEL_DEFAULT_LOCALE'],
                                navtype_=nav_type)

    form = NavigationGroupForm(obj=group)

    only_switch = False

    if form.is_submitted():
        if not any(ch in request.form for ch in ['submit', 'submit_and_continue']):
            only_switch = True

    if not only_switch:
        if form.validate_on_submit():
            try:
                form.populate_obj(group)

                if not group_id:
                    db.session.add(group)

                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)

                if not group_id:
                    flash(_("Can't add navigation group message"), 'danger')
                else:
                    flash(_("Can't save navigation group message"), 'danger')
            else:
                if not group_id:
                    flash(_('Navigation group added successfully message'), 'success')
                else:
                    flash(_('Navigation group saved successfully message'), 'success')

            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_navigation_group', nav_type=nav_type, group_id=group_id))

            return redirect(url_for('admin.navigation_list', nav_type=nav_type))

    return render_template('admin/manage_nav_group.html',
                           group=group,
                           group_id=group_id,
                           form=form,
                           nav_type=nav_type,
                           items_tab=items_tab,
                           popup_items_tab=popup_items_tab)


def _get_navigation_group_basequery(nav_type, group_id):
    query = NavigationGroup.query
    query = query.filter(NavigationGroup.navtype_.__eq__(nav_type))
    query = query.filter(NavigationGroup.id.__eq__(group_id))
    query = query.filter(db.or_(NavigationGroup.has_items.is_(True),
                                NavigationGroup.has_popup_items.is_(True)))
    return query


@admin.route('/navigation/<navtype:nav_type>/<int:group_id>/items')
@login_required
def manage_navigation_group_items_list(nav_type, group_id):
    group = _get_navigation_group_basequery(nav_type, group_id).first()

    if not group:
        flash(_('Group with id %(id)s items not found message', id=group_id), 'warning')
        return redirect(url_for('admin.navigation_list', nav_type=nav_type))

    active_tab = group.behavior_ == NavHelper.behaviorOnlyHeader

    if not active_tab:
        flash(_('No items of this type of navigation group message'), 'warning')
        return redirect(url_for('admin.manage_navigation_group', nav_type=nav_type, group_id=group_id))

    items = group.items.all()

    return render_template('admin/manage_nav_group_items.html',
                           group=group,
                           group_id=group_id,
                           items=items,
                           nav_type=nav_type,
                           active_tab=active_tab)


@admin.route('/navigation/<navtype:nav_type>/<int:group_id>/popup-items', methods=['GET', 'POST'])
@login_required
def manage_navigation_group_popup_items_list(nav_type, group_id):
    group = _get_navigation_group_basequery(nav_type, group_id).first()

    if not group:
        flash(_('Group with id %(id)s popup items not found message', id=group_id), 'warning')
        return redirect(url_for('admin.navigation_list', nav_type=nav_type))

    popup_items_tab = group.behavior_ == NavHelper.behaviorPopup

    if not popup_items_tab:
        flash(_('No popup items of the type of navigation group message'), 'warning')
        return redirect(url_for('admin.manage_navigation_group', nav_type=nav_type, group_id=group_id))

    popup_items = group.popup_items.all()
    square_items = [pi for pi in popup_items if pi.itemtype_ == NavigationGroupPopupItem.itemSquare]
    circle_items = [pi for pi in popup_items if pi.itemtype_ == NavigationGroupPopupItem.itemCircle]

    if request.method == 'POST':
        square_positions = request.form.getlist('square_positions', type=int)
        circle_positions = request.form.getlist('circle_positions', type=int)
        square_to_remove = request.form.getlist('square_toremove', type=int)
        circle_to_remove = request.form.getlist('circle_toremove', type=int)
        trash_selected = 'trash_selected' in request.form
        order_save_error = False

        if not trash_selected:
            if square_positions:
                try:
                    for si in square_items:
                        if si.id in square_positions:
                            si.position = square_positions.index(si.id) + 1
                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    current_app.logger.exception(e)
                    flash(_("Can't save square items order message"), 'danger')
                    order_save_error = True
            if circle_positions:
                try:
                    for ci in circle_items:
                        if ci.id in circle_positions:
                            ci.position = circle_positions.index(ci.id) + 1
                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    current_app.logger.exception(e)
                    flash(_("Can't save circle items order message"), 'danger')

                    if not order_save_error:
                        flash(_('Only square items order saved successfully'), 'success')

                    order_save_error = True
                else:
                    if order_save_error:
                        flash(_('Only circle items order saved successfully'), 'success')

            if not order_save_error:
                flash(_('Square and circle items order saved successfully'), 'success')

        if trash_selected and square_to_remove or circle_to_remove:
            try:
                s_items = {si.id: si for si in square_items}
                c_items = {ci.id: ci for ci in circle_items}
                items_to_delete = dict(s_items.items() + c_items.items())
                to_remove = square_to_remove + circle_to_remove

                for tr in to_remove:
                    obj = items_to_delete.get(tr)
                    if obj:
                        db.session.delete(obj)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash(_("Can't delete selected popup items message"), 'danger')
            else:
                flash(_('Selected popup items was deleted successfully message'), 'success')

        return redirect(url_for('admin.manage_navigation_group_popup_items_list', nav_type=nav_type, group_id=group_id))

    return render_template('admin/manage_nav_group_popup_items.html',
                           group=group,
                           group_id=group_id,
                           square_items=square_items,
                           circle_items=circle_items,
                           nav_type=nav_type,
                           popup_items_tab=popup_items_tab)


@admin.route('/navigation/<navtype:nav_type>/<int:group_id>/popup-items/<any(square,circle):item_type>/add', methods=['GET', 'POST'])
@admin.route('/navigation/<navtype:nav_type>/<int:group_id>/popup-items/<any(square,circle):item_type>/<int:item_id>', methods=['GET', 'POST'])
@login_required
def manage_navigation_group_popup_item(nav_type, group_id, item_type='square', item_id=None):
    group = _get_navigation_group_basequery(nav_type, group_id).first()

    if not group:
        flash(_('Group with id %(id)s popup items not found message', id=group_id), 'warning')
        return redirect(url_for('admin.navigation_list', nav_type=nav_type))

    if item_id:
        item = NavigationGroupPopupItem.query.filter(NavigationGroupPopupItem.id.__eq__(item_id)).first()

        if item.group != group:
            flash(_('No popup item in current navigation group message', 'warning'))
            return redirect(url_for('admin.manage_navigation_group_popup_items_list', nav_type=nav_type, group_id=group_id))
    else:
        item = NavigationGroupPopupItem(group=group,
                                        itemtype_=item_type)

    form = NavigationGroupPopupItemForm(obj=item)
    only_switch = False

    if form.is_submitted():
        if not any(ch in request.form for ch in ['submit', 'submit_and_continue']):
            only_switch = True

    if not only_switch:
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'nav_icons')
        file_to_remove = None

        if form.validate_on_submit():
            try:
                form.populate_obj(item)

                if not item_id:
                    db.session.add(item)

                form_icon = None
                icon_type = None
                model_icon = None

                if 'sicon' in form:
                    form_icon = form.sicon
                    icon_type = 'square_icon'

                    if item.square_icon and os.path.isfile(os.path.join(folder, item.stored_sicon)):
                        file_to_remove = os.path.join(folder, item.stored_sicon)
                elif 'cicon' in form:
                    form_icon = form.cicon
                    icon_type = 'circle_icon'

                    if item.circle_icon and os.path.isfile(os.path.join(folder, item.stored_cicon)):
                        file_to_remove = os.path.join(folder, item.stored_cicon)

                if form_icon:
                    if form_icon.data:
                        if isinstance(form_icon.data, basestring):
                            file_to_remove = False
                        else:
                            if FileHelper.check_file_extension(form_icon.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                                stored_filename, original_filename = FileHelper.generate_filename_hash(form_icon.data.filename)
                                form_icon.data.save(os.path.join(folder, stored_filename))
                                item.set_icon(icon_type, [stored_filename, original_filename])
                    else:
                        item.set_icon(icon_type, [])

                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash('Error', 'danger')  # TODO: ERROR
            else:
                if file_to_remove:
                    try:
                        os.unlink(file_to_remove)
                    except Exception as e:
                        current_app.logger.exception(e)

                flash('Ok', 'success')  # TODO: OK

            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_navigation_group_popup_item', nav_type=nav_type, group_id=group_id, item_type=item.itemtype_, item_id=item.id))

            return redirect(url_for('admin.manage_navigation_group_popup_items_list', nav_type=nav_type, group_id=group_id))

    return render_template('admin/manage_nav_group_popup_item.html',
                           group=group,
                           group_id=group_id,
                           item=item,
                           item_id=item_id,
                           form=form,
                           item_type=item_type,
                           nav_type=nav_type)


@admin.route('/navigation/<navtype:nav_type>/<int:group_id>/items/add', methods=['GET', 'POST'])
@admin.route('/navigation/<navtype:nav_type>/<int:group_id>/items/<int:item_id>', methods=['GET', 'POST'])
@login_required
def manage_navigation_group_item(nav_type, group_id, item_id=None):
    group = _get_navigation_group_basequery(nav_type, group_id).first()

    if not group:
        flash(_('Group with id %(id)s items not found message', id=group_id), 'warning')
        return redirect(url_for('admin.navigation_list', nav_type=nav_type))

    if item_id:
        item = NavigationGroupItem.query.filter(NavigationGroupItem.id.__eq__(item_id)).first()

        if item.group != group:
            flash(_('No item in current navigation group message', 'warning'))
            return redirect(url_for('admin.manage_navigation_group_items_list', nav_type=nav_type, group_id=group_id))
    else:
        item = NavigationGroupItem(group=group)

    form = NavigationGroupItemForm(obj=item)

    only_switch = False

    if form.is_submitted():
        if not any(ch in request.form for ch in ['submit', 'submit_and_continue']):
            only_switch = True

    if not only_switch:
        if form.validate_on_submit():
            try:
                form.populate_obj(item)

                if not item_id:
                    db.session.add(item)

                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash('Error', 'danger')  # TODO: ERROR
            else:
                flash('Ok', 'success')

            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_navigation_group_item', nav_type=nav_type, group_id=group_id, item_id=item.id))

            return redirect(url_for('admin.manage_navigation_group_items_list', nav_type=nav_type, group_id=group_id))

    return render_template('admin/manage_nav_group_item.html',
                           group=group,
                           group_id=group_id,
                           item=item,
                           item_id=item_id,
                           form=form,
                           nav_type=nav_type)


with current_app.app_context():
    import block_views
    import site_views
