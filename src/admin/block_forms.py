# coding: utf-8
from brpr.flask.widgets import WidgetPrebind
from core.forms import Form, wForm
from core.forms.fields import ColorSelect, UploadField
from flask.ext.babel import lazy_gettext as __
from wtforms import BooleanField, SelectField, TextField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import Required
from wtforms.widgets import Select, TextArea, TextInput

from karcher.models import Tb, ColorDictionary, Hd1Sl1Slide


class Hd1Tx2Ft1Form(Form):
    header = TextField(u'Заголовок',
                       widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))
    left_text = TextField(u'Слева',
                          widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    right_text = TextField(u'Справа',
                           widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    footer = TextField(u'Нижний текст',
                       widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))


class Hd1In2Tx2In2Form(Form):
    header = TextField(u'Заголовок',
                       widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))
    lt_incut = TextField(u'Левая верхняя врезка',
                         widget=WidgetPrebind(TextArea(), rows=5, class_='form-control uk-width-1-1'))
    rt_incut = TextField(u'Правая верхняя врезка',
                         widget=WidgetPrebind(TextArea(), rows=5, class_='form-control uk-width-1-1'))
    lb_incut = TextField(u'Нижняя левая врезка',
                         widget=WidgetPrebind(TextArea(), rows=5, class_='form-control uk-width-1-1'))
    rb_incut = TextField(u'Нижняя правая врезка',
                         widget=WidgetPrebind(TextArea(), rows=5, class_='form-control uk-width-1-1'))
    left_text = TextField(u'Текст слева',
                          widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    right_text = TextField(u'Текст справа',
                           widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))


class Tx4In1Form(Form):
    lt_text = TextField(u'Левый верхний текст',
                        widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    rt_text = TextField(u'Правый верхний текст',
                        widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    lb_text = TextField(u'Нижний левый текст',
                        widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    rb_text = TextField(u'Нижний правый текст',
                        widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    incut = TextField(u'Текст врезки',
                      widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))


class Tx4InIm1Form(Form):
    lt_text = TextField(u'Левый верхний текст',
                        widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    rt_text = TextField(u'Правый верхний текст',
                        widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    lb_text = TextField(u'Нижний левый текст',
                        widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    rb_text = TextField(u'Нижний правый текст',
                        widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    incut = TextField(u'Заголовок',
                      widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))


class Hd1Im1Tx1RForm(Form):
    header = TextField(u'Заголовок',
                       widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))
    content = TextField(u'Текст',
                        widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))


class Hd1Tx2Hd1Im1Form(Form):
    header_1 = TextField(u'Заголовок',
                         widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))
    left_text = TextField(u'Слева',
                          widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    right_text = TextField(u'Справа',
                           widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    header_2 = TextField(u'Заголовок 2',
                         widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))


class Hd1Tx2CalcForm(Form):
    header_1 = TextField(u'Заголовок',
                         widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))
    left_text = TextField(u'Слева',
                          widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    right_text = TextField(u'Справа',
                           widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))


class Hd1Tx1ContactsForm(Form):
    header = TextField(u'Заголовок',
                       widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))
    content = TextField(u'Текст',
                        widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))


class Tx2Form(Form):
    left_text = TextField(u'Текст слева', widget=WidgetPrebind(TextArea(), rows=10, class_='form-control'))
    right_text = TextField(u'Текст справа', widget=WidgetPrebind(TextArea(), rows=10, class_='form-control'))


class Hd1Tx1Tx2Form(Form):
    header = TextField(u'Заголовок', widget=WidgetPrebind(TextInput(), class_='form-control'))
    content = TextField(u'Основной текст', widget=WidgetPrebind(TextArea(), rows=10, class_='form-control'))
    left_text = TextField(u'Текст слева', widget=WidgetPrebind(TextArea(), rows=10, class_='form-control'))
    right_text = TextField(u'Текст справа', widget=WidgetPrebind(TextArea(), rows=10, class_='form-control'))


class Hd1Mp1Form(Form):
    header = TextField(u'Заголовок', widget=WidgetPrebind(TextInput(), class_='form-control'))
    type_ = SelectField(u'Объекты на карте', choices=[], widget=WidgetPrebind(Select(), class_='form-control'))

    def __init__(self, *args, **kwargs):
        super(Hd1Mp1Form, self).__init__(*args, **kwargs)
        from karcher.models import Hd1Mp1
        self.type_.choices = Hd1Mp1.types_listing()


class Hd1Mp2Form(Form):
    header = TextField(u'Заголовок', widget=WidgetPrebind(TextInput(), class_='form-control'))
    type_ = SelectField(u'Объекты на карте', choices=[], widget=WidgetPrebind(Select(), class_='form-control'))

    def __init__(self, *args, **kwargs):
        super(Hd1Mp2Form, self).__init__(*args, **kwargs)
        from karcher.models import Hd1Mp1
        self.type_.choices = Hd1Mp1.types_listing()


# ===
class CustomHd1Tx2Sp2Form(Form):
    header = TextField(u'Заголовок', widget=WidgetPrebind(TextInput(), class_='form-control'))
    left_text = TextField(u'Слева',
                          widget=WidgetPrebind(TextArea(), rows=10, class_='form-control'))
    right_text = TextField(u'Справа',
                           widget=WidgetPrebind(TextArea(), rows=10, class_='form-control'))


#
# Tb block forms
#
class CustomTbForm(Form):
    type_ = SelectField(u'Тип вкладок', choices=Tb.type_listing(),
                        widget=WidgetPrebind(Select(), class_='form-control'))


class CustomTbBlockTabForm(Form):
    caption = TextField(u'Заголовок',
                        validators=[Required()],
                        widget=WidgetPrebind(TextInput(), class_='form-control'))
    published = BooleanField(u'Активная вкладка')


#
# Slides block
#
class CustomHd1Sl1Form(Form):
    header = TextField(u'Заголовок', widget=WidgetPrebind(TextInput(), class_='form-control'))


class CustomHd1Sl1SlideForm(Form):
    description = TextField(u'Текст',
                            widget=WidgetPrebind(TextArea(), rows=5, class_='form-control'))
    font_color = QuerySelectField(__('Font color label'),
                                  query_factory=ColorDictionary.font_colors,
                                  widget=WidgetPrebind(ColorSelect(), class_='form-control color-selector'))
    textposition = SelectField(u'Положение текста', choices=Hd1Sl1Slide.textposition_listing(), widget=WidgetPrebind(Select(), class_='form-control'))
    published = BooleanField(u'Опубликован')
    slide_image = UploadField(u'Изображение', validators=[Required()])
