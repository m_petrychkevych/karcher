# coding: utf-8
import sqlalchemy as sa

from brpr.flask.widgets import WidgetPrebind
from core.forms import Form, wForm
from core.forms.fields import ColorSelect, SlugField, UploadField
from core.forms.validators import IntRequired
from flask import flash
from flask.ext.babel import gettext as _, lazy_gettext as __
from wtforms import BooleanField, HiddenField, FileField, PasswordField, SelectField, TextField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import Email, Required
from wtforms.widgets import PasswordInput, RadioInput, Select, TextInput, TextArea
from wtforms.widgets.html5 import EmailInput

from karcher.api.helpers import LanguageHelper
from karcher.models import (BlockCollection,
                            ColorDictionary,
                            NavigationGroup,
                            NavigationGroupItem,
                            NavigationGroupPopupItem,
                            NavHelper,
                            Page,
                            PageBlock)
from users.models import User


class LoginForm(Form):
    login = TextField(__('Login label'),
                      validators=[Required(__('Please, provide your login message')),
                                  Email(__('Invalid email message'))],
                      widget=WidgetPrebind(EmailInput(),
                                           class_='form-control uk-form-large',
                                           placeholder=__('Login placeholder'),
                                           autofocus='on',
                                           size=30))
    password = PasswordField(__('Password label'),
                             validators=[Required()],
                             widget=WidgetPrebind(PasswordInput(),
                                                  class_='form-control uk-form-large',
                                                  placeholder=__('Password placeholder'),
                                                  size=30,
                                                  autocomplete='off'))
    remember_me = BooleanField(__('Remember me label'), default=False)
    next = HiddenField()

    def validate(self):
        if not Form.validate(self):
            return False

        user = User.query.filter(sa.func.lower(User.login) == self.login.data.lower(),
                                 User.activity.is_(True)).first()

        if not user or not user.check_password(self.password.data):
            self.errors.update({'form': [__('Invalid username or password message')]})
            return False

        self.user = user
        return True


class PageForm(Form):
    title = TextField(__('Page title label'),
                      validators=[Required()],
                      description=__('Page title description'),
                      widget=WidgetPrebind(TextInput(),
                                           class_='form-control'))
    slug = SlugField(__('Page slug label'),
                     validators=[Required()],
                     widget=WidgetPrebind(TextInput(), class_='form-control'))
    language = SelectField(__('Page language label'),
                           choices=LanguageHelper.listing(),
                           widget=WidgetPrebind(Select(), class_='form-control'))
    main = BooleanField(__('Page main label'))
    published = BooleanField(__('Page published label'))

    meta_title = TextField(__('Page meta title label'),
                           widget=WidgetPrebind(TextInput(), class_='form-control'))
    meta_description = TextField(__('Page meta description label'),
                                 widget=WidgetPrebind(TextArea(), rows=3, class_='noresize form-control'))
    meta_keywords = TextField(__('Page meta keywords label'),
                              widget=WidgetPrebind(TextArea(), rows=3, class_='noresize form-control'))

    def __init__(self, *args, **kwargs):
        obj = kwargs.get('obj')
        self._oid = obj.id if obj else None

        super(PageForm, self).__init__(*args, **kwargs)

    def validate(self):
        rv = Form.validate(self)

        if not rv:
            return False

        obj = Page.query.filter(sa.func.lower(Page.language).__eq__(self.language.data.lower()),
                                Page.slug.__eq__(self.slug.data)).first()

        if obj and obj.id != self._oid:
            self.slug.errors.append(_('Slug already exists for current language message'))
            return False

        return True


class PageBlockForm(Form):
    comment = TextField(__('Pageblock comment label'),
                        validators=[Required()],
                        description=__('Pageblock comment description'),
                        widget=WidgetPrebind(TextInput(), class_='form-control'))
    published = BooleanField(__('Pageblock published label'))
    type_ = SelectField(__('Pageblock type label'), choices=BlockCollection.listing(),
                        widget=WidgetPrebind(Select(), class_='form-control'))

    # Common block attributes
    font_color = QuerySelectField(__('Font color label'),
                                  query_factory=ColorDictionary.font_colors,
                                  widget=WidgetPrebind(ColorSelect(), class_='form-control color-selector'))
    background_color = QuerySelectField(__('Background color label'),
                                        query_factory=ColorDictionary.background_colors,
                                        widget=WidgetPrebind(ColorSelect(), class_='form-control color-selector'))
    background_position = SelectField(__('Pageblock background-position label'),
                                      choices=PageBlock.positionBackgrounds,
                                      widget=WidgetPrebind(Select(), class_='form-control'))
    background_size = SelectField(__('Pageblock background-size label'),
                                  choices=PageBlock.sizeBackgrounds,
                                  widget=WidgetPrebind(Select(), class_='form-control'))
    bimage = UploadField(__('Pageblock background-image label'))
    content_auto_height = BooleanField(__('Pageblock content auto height label'))
    height_padding = TextField(__('Pageblock height padding label'),
                               validators=[IntRequired(positive=True)],
                               widget=WidgetPrebind(TextInput(), class_='form-control'))

    def __init__(self, *args, **kwargs):
        self.update = kwargs.get('update')
        self.embedded = kwargs.get('embedded', False)
        super(PageBlockForm, self).__init__(*args, **kwargs)

        if self.update:
            self.type_.widget = WidgetPrebind(Select(), class_='form-control', disabled='disabled')
        if self.embedded:
            self.type_.choices = BlockCollection.listing(embedded=self.embedded)

    def process(self, formdata=None, obj=None, **kwargs):
        super(PageBlockForm, self).process(formdata, obj, **kwargs)


class NavigationGroupForm(Form):
    caption = TextField(__('Navigation group caption label'),
                        validators=[Required()],
                        widget=WidgetPrebind(TextInput(), class_='form-control'))
    slug = SlugField(__('Navigation group slug label'),
                     validators=[Required()],
                     widget=WidgetPrebind(TextInput(), class_='form-control'))
    published = BooleanField(__('Navigation group published label'))
    behavior = SelectField(__('Navigation group behavior label'), choices=[], widget=WidgetPrebind(Select(), class_='form-control switch_behavior'))

    link_url = TextField(__('Navigation group link url label'),
                         validators=[Required()],
                         widget=WidgetPrebind(TextInput(), class_='form-control'))
    link_blank = BooleanField(__('Navigation group link target blank label'))
    page = QuerySelectField(__('Navigation group page label'),
                            query_factory=lambda: Page.available(),
                            get_label='title',
                            widget=WidgetPrebind(Select(), class_='form-control'))

    def process(self, formdata=None, obj=None, **kwargs):
        nav_type = kwargs.get('nav_type')
        behavior = formdata.get('behavior') if formdata else None

        if obj:
            if not nav_type:
                nav_type = obj.navtype_

            if not behavior:
                behavior = obj.behavior

        self.behavior.choices = NavigationGroup.behavior_listing(nav_type)

        if not behavior:
            behavior = self.behavior.choices[0][0]  # Если до сих пор нет, то первый в списке

        fields_to_remove = ['link_url', 'link_blank', 'page']
        fields_to_keep = []

        if behavior == NavHelper.behaviorLink:
            fields_to_keep.append('link_url')
            fields_to_keep.append('link_blank')
        elif behavior == NavHelper.behaviorPage:
            fields_to_keep.append('page')

        for field in fields_to_remove:
            if field not in fields_to_keep and field in self:
                self.__delitem__(field)

        super(NavigationGroupForm, self).process(formdata, obj, **kwargs)

    def populate_obj(self, obj):
        if self.slug.data:
            ex_slug = NavigationGroup.query.filter(NavigationGroup.slug.__eq__(self.slug.data))
            if obj.id:
                ex_slug = ex_slug.filter(~NavigationGroup.id.__eq__(obj.id))
            groups = ex_slug.all()

            if groups:
                flash(_('Slug %(slug)s exist in %(groups)s message', slug=self.slug.data, groups=', '.join([gr.caption for gr in groups])), 'info')

        super(NavigationGroupForm, self).populate_obj(obj)


class NavigationGroupItemForm(Form):
    caption = TextField(__('Navigation item caption label'),
                        validators=[Required()],
                        widget=WidgetPrebind(TextInput(), class_='form-control'))
    slug = SlugField(__('Navigation item slug label'),
                     validators=[Required()],
                     widget=WidgetPrebind(TextInput(), class_='form-control'))
    published = BooleanField(__('Navigation item published label'))
    behavior = SelectField(__('Navigation item behavior label'), choices=[], widget=WidgetPrebind(Select(), class_='form-control switch_behavior'))

    link_url = TextField(__('Navigation group link url label'),
                         validators=[Required()],
                         widget=WidgetPrebind(TextInput(), class_='form-control'))
    link_blank = BooleanField(__('Navigation group link target blank label'))
    page = QuerySelectField(__('Navigation item page label'),
                            query_factory=lambda: Page.available(),
                            get_label='title',
                            widget=WidgetPrebind(Select(), class_='form-control'))

    def process(self, formdata=None, obj=None, **kwargs):
        behavior = formdata.get('behavior') if formdata else None

        if obj:
            if not behavior:
                behavior = obj.behavior

        self.behavior.choices = NavigationGroupItem.behavior_listing()

        if not behavior:
            behavior = self.behavior.choices[0][0]  # Если нет до сих пор, то первый в списке

        fields_to_remove = ['link_url', 'link_blank', 'page']
        fields_to_keep = []

        if behavior == NavHelper.behaviorLink:
            fields_to_keep.append('link_url')
            fields_to_keep.append('link_blank')
        elif behavior == NavHelper.behaviorPage:
            fields_to_keep.append('page')

        for field in fields_to_remove:
            if field not in fields_to_keep and field in self:
                self.__delitem__(field)

        super(NavigationGroupItemForm, self).process(formdata, obj, **kwargs)


class NavigationSwitchtypeForm(wForm):
    nav_type = SelectField(choices=NavigationGroup.nav_types_listing(),
                           widget=WidgetPrebind(Select(), class_='form-control switch_type'))


class NavigationGroupPopupItemForm(Form):
    caption = TextField(__('Navigation item caption label'),
                        validators=[Required()],
                        widget=WidgetPrebind(TextInput(), class_='form-control'))
    description = TextField(__('Navigation item description label'),
                            widget=WidgetPrebind(TextInput(), class_='form-control'))
    slug = SlugField(__('Navigation item slug label'),
                     validators=[Required()],
                     widget=WidgetPrebind(TextInput(), class_='form-control'))

    sicon = UploadField(__('Navigation square icon label'))
    cicon = UploadField(__('Navigation circle icon label'))

    published = BooleanField(__('Navigation item published label'))

    behavior = SelectField(__('Navigation item behavior label'), choices=[], widget=WidgetPrebind(Select(), class_='form-control switch_behavior'))

    link_url = TextField(__('Navigation group link url label'),
                         validators=[Required()],
                         widget=WidgetPrebind(TextInput(), class_='form-control'))
    link_blank = BooleanField(__('Navigation group link target blank label'))
    page = QuerySelectField(__('Navigation item page label'),
                            query_factory=lambda: Page.available(),
                            get_label='title',
                            widget=WidgetPrebind(Select(), class_='form-control'))

    def __init__(self, *args, **kwargs):
        super(NavigationGroupPopupItemForm, self).__init__(*args, **kwargs)
        obj = kwargs.get('obj', None)

        if obj:
            fields_to_remove = ['sicon', 'cicon', 'description']
            fields_to_keep = []

            if obj.itemtype_ == NavigationGroupPopupItem.itemSquare:
                fields_to_keep.append('sicon')
                fields_to_keep.append('description')
            elif obj.itemtype_ == NavigationGroupPopupItem.itemCircle:
                fields_to_keep.append('cicon')

            for field in fields_to_remove:
                if field not in fields_to_keep and field in self:
                    self.__delitem__(field)

    def process(self, formdata=None, obj=None, **kwargs):
        behavior = formdata.get('behavior') if formdata else None

        if obj:
            if not behavior:
                behavior = obj.behavior

        self.behavior.choices = NavigationGroupPopupItem.behavior_listing()

        if not behavior:
            behavior = self.behavior.choices[0][0]  # Если нет до сих пор, то первый в списке

        fields_to_remove = ['link_url', 'link_blank', 'page']
        fields_to_keep = []

        if behavior == NavHelper.behaviorLink:
            fields_to_keep.append('link_url')
            fields_to_keep.append('link_blank')
        elif behavior == NavHelper.behaviorPage:
            fields_to_keep.append('page')

        for field in fields_to_remove:
            if field not in fields_to_keep and field in self:
                self.__delitem__(field)

        super(NavigationGroupPopupItemForm, self).process(formdata, obj, **kwargs)


class FileUploadForm(wForm):
    upload = UploadField()
