$(function() {
    $('.buttons-bar').each(function() { $(this).buttonsBar() });
    $('.table-container__filters').each(function() { $(this).tableFilters() });
    /* Removable */
    $('.mark-to-remove').on('click', function() {
        var tr = $(this).closest('tr');
        if (tr) {
            if ($(this).prop('checked')) { $(tr).addClass('danger'); } else { $(tr).removeClass('danger'); }
        }
    });
    $('.color-selector').each(function() { $(this).colorselector() });
});
