# coding: utf-8
from brpr.flask.widgets import WidgetPrebind
from core.forms import Form, wForm
from core.forms.fields import TagSelectMultipleField, UploadField
from core.forms.validators import IntRequired
from core.forms.widgets import SelectMultipleCheckbox, DateInput
from flask.ext.babel import lazy_gettext as __
from wtforms import BooleanField, DateField, HiddenField, SelectField, SelectMultipleField, TextField
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import Required, URL, Email, Optional
from wtforms.widgets import HiddenInput, Select, TextArea, TextInput

from karcher.site_models import CarwashProgram, CarwashNetwork, MapObject, MediaCollection


class CarwashForm(Form):
    title = TextField(__('Carwash title label'),
                      validators=[Required()],
                      widget=WidgetPrebind(TextInput(), class_='form-control'))
    network = QuerySelectField(__('Carwash network label'),
                               get_label='title',
                               allow_blank=True,
                               blank_text=__('Select carwash network option'),
                               query_factory=lambda: CarwashNetwork.admin_list(),
                               widget=WidgetPrebind(Select(), class_='form-control'))
    published = BooleanField(__('Carwash published label'))
    latitude = TextField(widget=WidgetPrebind(HiddenInput(), class_='data_latitude'))
    longitude = TextField(widget=WidgetPrebind(HiddenInput(), class_='data_longitude'))
    zoom = TextField(widget=WidgetPrebind(HiddenInput(), class_='data_zoom'))
    address = TextField(__('Carwash address label'),
                        widget=WidgetPrebind(TextInput(), class_='form-control search_address'))
    map_tag = HiddenField(widget=WidgetPrebind(HiddenInput()))  # Используется только для того, чтобы знать куда вставить карту
    posts_count = TextField(__('Carwash posts count label'),
                            validators=[Required(), IntRequired(positive=True)],
                            widget=WidgetPrebind(TextInput(), class_='form-control'))
    enhanced_post = BooleanField(__('Carwash enhanced post label'))
    high_body_vihicle = BooleanField(__('Carwash high body vihicle label'))

    action = TextField(__('Carwash action label'),
                       widget=WidgetPrebind(TextInput(), class_='form-control'))
    programs = QuerySelectMultipleField(__('Carwash programs label'),
                                        get_label='title',
                                        query_factory=lambda: CarwashProgram.admin_list(),
                                        widget=WidgetPrebind(SelectMultipleCheckbox()))


class CarwashProgramForm(Form):
    title = TextField(__('Carwash program title label'),
                      validators=[Required()],
                      widget=WidgetPrebind(TextInput(), class_='form-control'))
    short_description = TextField(__('Carwash program short description label'),
                                  validators=[Required()],
                                  widget=WidgetPrebind(TextArea(), rows=2, class_='form-control'))
    description = TextField(__('Carwash program description label'),
                            widget=WidgetPrebind(TextArea(), rows=10, class_='form-control'))
    imsmicon = UploadField(__('Carwash small icon label'))
    imicon = UploadField(__('Carwash icon label'))


class CarwashNetworkForm(Form):
    title = TextField(__('Carwash network title label'),
                      validators=[Required()],
                      widget=WidgetPrebind(TextInput(), class_='form-control'))


class MapObjectForm(Form):
    title = TextField(__('Map object title label'),
                      validators=[Required()],
                      widget=WidgetPrebind(TextInput(), class_='form-control'))
    type_ = SelectField(__('Map object type label'),
                        choices=MapObject.types_listing(),
                        widget=WidgetPrebind(Select(), class_='form-control'))
    description = TextField(__('Map object description label'),
                            widget=WidgetPrebind(TextArea(), rows=7, class_='form-control'))
    mophoto = UploadField(__('Map object photo label'))
    latitude = TextField(widget=WidgetPrebind(HiddenInput(), class_='data_latitude'))
    longitude = TextField(widget=WidgetPrebind(HiddenInput(), class_='data_longitude'))
    zoom = TextField(widget=WidgetPrebind(HiddenInput(), class_='data_zoom'))
    address = TextField(__('Map object address label'),
                        widget=WidgetPrebind(TextInput(), class_='form-control search_address'))
    map_tag = HiddenField(widget=WidgetPrebind(HiddenInput()))  # Используется только для того, чтобы знать куда вставить карту
    site_link = TextField(__('Map object url label'), validators=[Optional(), URL()], widget=WidgetPrebind(TextInput(), class_='form-control'))
    email = TextField(__('Map object email label'), validators=[Optional(), Email()], widget=WidgetPrebind(TextInput(), class_='form-control'))
    phone = TextField(__('Map object phone label'), widget=WidgetPrebind(TextInput(), class_='form-control'))


class FAQCategoryForm(Form):
    title = TextField(__('FAQ Category title label'),
                      validators=[Required()],
                      widget=WidgetPrebind(TextInput(), class_='form-control'))
    published = BooleanField(__('FAQ Category published label'))


class FAQForm(Form):
    question = TextField(__('FAQ question label'),
                         validators=[Required()],
                         widget=WidgetPrebind(TextInput(), class_='form-control'))
    answer = TextField(__('FAQ answer label'),
                       validators=[Required()],
                       widget=WidgetPrebind(TextArea(), rows=10, class_='form-control'))
    published = BooleanField(__('FAQ published label'))


class NewsArticleForm(Form):
    title = TextField(__('News article title label'),
                      validators=[Required()],
                      widget=WidgetPrebind(TextInput(), class_='form-control'))
    an_image = UploadField(__('Announcement image label'))
    announcement = TextField(__('News article announcement label'),
                             widget=WidgetPrebind(TextArea(), rows=2, class_='form-control'))
    publish_date = DateField(__('News article publish date label'),
                             validators=[Required()],
                             format='%d.%m.%Y',
                             widget=WidgetPrebind(DateInput(), class_='form-control', **{'data-date-format': 'dd.mm.yyyy'}))
    content = TextField(__('News article content label'),
                        widget=WidgetPrebind(TextArea(), rows=10, class_='form-control'))


class MediaVideoForm(Form):
    youtubeid = TextField(__('Youtube id label'), validators=[Required()], widget=WidgetPrebind(TextInput(), class_='form-control'))
    tags = TagSelectMultipleField(__('Media tags label'), choices=[], coerce=unicode, widget=WidgetPrebind(Select(multiple=True), class_='form-control js-tags'))

    def __init__(self, *args, **kwargs):
        super(MediaVideoForm, self).__init__(*args, **kwargs)
        self.tags.choices = MediaCollection.tags_listing()


class MediaPhotoForm(Form):
    mphoto = UploadField(__('Media photo label'), validators=[Required()])
    tags = TagSelectMultipleField(__('Media tags label'), choices=[], coerce=unicode, widget=WidgetPrebind(Select(multiple=True), class_='form-control js-tags'))

    def __init__(self, *args, **kwargs):
        super(MediaPhotoForm, self).__init__(*args, **kwargs)
        self.tags.choices = MediaCollection.tags_listing()


class CleanserForm(Form):
    title = TextField(__('Cleanser title label'),
                      validators=[Required()],
                      widget=WidgetPrebind(TextInput(), class_='form-control'))
    sku = TextField(__('Cleanser sku label'),
                      validators=[Required()],
                      widget=WidgetPrebind(TextInput(), class_='form-control'))
    published = BooleanField(__('Cleanser published label'))
    cphoto = UploadField(__('Cleanser photo label'))
    list_description = TextField(__('Cleanser list description label'),
                                 validators=[Required()],
                                 widget=WidgetPrebind(TextArea(), rows=5, class_='form-control'))
    overlay_description = TextField(__('Cleanser overlay description label'),
                                    validators=[Required()],
                                    widget=WidgetPrebind(TextArea(), rows=10, class_='form-control'))
