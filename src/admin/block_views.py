# coding: utf-8
import os

from core.utils.files import FileHelper
from flask import abort, Blueprint, current_app, flash, redirect, render_template, request, url_for
from flask.ext.babel import gettext as _, lazy_gettext as __
from flask.ext.login import current_user, login_required

from karcher.models import Tb, TbBlockTabs, EmbeddedPage, PageBlock
from karcher.models import Hd1Sl1, Hd1Sl1Slide

from .block_forms import CustomTbBlockTabForm
from .block_forms import CustomHd1Sl1SlideForm
from .forms import PageBlockForm
from .models import db
from .views import admin, _pageblock_helper


#
# Tb block additional views
#
tab_tabs = [dict(alias='general', title=__('Block tb tabs general tab')),
            dict(alias='blocks', title=__('Block tb tabs blocks tab'))]


@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>/data/<string:active_subtab>/add', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>/data/<string:active_subtab>/<int:blocktab_id>', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>/data/<string:active_subtab>/<int:blocktab_id>/<any(general, blocks):active_tab_tab>', methods=['GET', 'POST'])
@login_required
def manage_tb_block_tab(page_id, pageblock_id, active_subtab='general', active_tab='tabs', blocktab_id=None, active_tab_tab='general', *args, **kwargs):
    page, pageblock = _pageblock_helper(page_id, pageblock_id, active_tab)
    tb_block = pageblock.get_data_object()
    epage = None
    epage_id = None

    if not tb_block or not isinstance(tb_block, Tb):
        abort(404)

    if blocktab_id:
        blocktab = TbBlockTabs.query.filter(TbBlockTabs.block_id.__eq__(tb_block.id),
                                            TbBlockTabs.id.__eq__(blocktab_id)).first()

        if not blocktab:
            flash(_('Tab for block not found message'), 'warning')
            return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

        epage = blocktab.epage
        epage_id = blocktab.epage_id
    else:
        blocktab = TbBlockTabs(block=tb_block)

    form = None

    if active_tab_tab == 'general':
        form = CustomTbBlockTabForm(obj=blocktab)

    if request.method == 'POST':
        if form:
            if form.validate_on_submit():
                try:
                    form.populate_obj(blocktab)

                    if not blocktab_id:
                        blocktab.epage = EmbeddedPage(title=blocktab.caption)
                        db.session.add(blocktab)

                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    current_app.logger.exception(e)
                    flash('Error adding or saving tab data message', 'danger')
                else:
                    flash('Added or save successfully message', 'success')
                if 'submit_and_continue' in request.form:
                    return redirect(url_for('admin.manage_tb_block_tab', page_id=page.id, pageblock_id=pageblock_id, active_subtab=active_subtab, blocktab_id=blocktab.id))
                return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))
        else:
            if active_tab_tab == 'blocks':
                positions = request.form.getlist('positions', type=int)
                to_remove = request.form.getlist('toremove', type=int)
                trash_selected = 'trash_selected' in request.form
                restore_selected = 'restore_selected' in request.form

                if positions and not trash_selected and not restore_selected:
                    try:
                        for b in epage.blocks:
                            if b.id in positions:
                                b.position = positions.index(b.id) + 1
                        db.session.commit()
                    except Exception as e:
                        db.session.rollback()
                        current_app.logger.exception(e)
                    else:
                        flash(_('Blocks order saved successfully message'), 'success')
                if to_remove and trash_selected:
                    try:
                        blocks = {b.id: b for b in epage.blocks}
                        for tr in to_remove:
                            obj = blocks.get(tr)
                            if obj:
                                if obj.status == PageBlock.statusActive:
                                    obj.status = PageBlock.statusTrash
                                elif obj.status == PageBlock.statusTrash:
                                    db.session.delete(obj)
                        db.session.commit()
                    except Exception as e:
                        db.session.rollback()
                        current_app.logger.exception(e)
                        flash(_("Can't delete selected items"), 'danger')
                    else:
                        flash(_('Selected blocks moved to trash message'), 'success')

                    return redirect(url_for('admin.manage_tb_block_tab', page_id=page.id, pageblock_id=pageblock_id, active_subtab=active_subtab, active_tab_tab=active_tab_tab, blocktab_id=blocktab.id))
                elif to_remove and restore_selected:
                    try:
                        blocks = {b.id: b for b in epage.blocks if b.status == 'Trash'}
                        for tr in to_remove:
                            obj = blocks.get(tr)
                            if obj:
                                if obj.status == PageBlock.statusTrash:
                                    obj.status = PageBlock.statusActive
                        db.session.commit()
                    except Exception as e:
                        db.session.rollback()
                        current_app.logger.exception(e)
                        flash(_("Can't restore selected blocks message"), 'error')
                    else:
                        flash(_('Selected blocks restored successfully message'), 'success')
                    return redirect(url_for('admin.manage_tb_block_tab', page_id=page.id, pageblock_id=pageblock_id, active_subtab=active_subtab, active_tab_tab=active_tab_tab, blocktab_id=blocktab.id))

            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_tb_block_tab', page_id=page.id, pageblock_id=pageblock_id, active_subtab=active_subtab, active_tab_tab=active_tab_tab, blocktab_id=blocktab.id))

            return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

    return render_template('admin/tb/manage_tab.html',
                           page=page,
                           page_id=page_id,
                           pageblock=pageblock,
                           pageblock_id=pageblock_id,
                           active_tab=active_tab,
                           active_subtab=active_subtab,
                           blocktab=blocktab,
                           blocktab_id=blocktab_id,
                           epage=epage,
                           epage_id=epage_id,
                           form=form,
                           tab_tabs=tab_tabs,
                           active_tab_tab=active_tab_tab)


@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>/data/<string:active_subtab>/<int:blocktab_id>/<any(blocks):active_tab_tab>/add', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>/data/<string:active_subtab>/<int:blocktab_id>/<any(blocks):active_tab_tab>/<int:eblock_id>', methods=['GET', 'POST'])
@login_required
def manage_tb_block_tab_blocks(page_id, pageblock_id, blocktab_id, active_subtab='general', active_tab='tabs', active_tab_tab='blocks', eblock_id=None, *args, **kwargs):
    """
    Редактирование данных встроенных в табы блоков.
    """
    page, pageblock = _pageblock_helper(page_id, pageblock_id, active_tab)
    tb_block = pageblock.get_data_object()

    if not tb_block or not isinstance(tb_block, Tb):
        abort(404)

    blocktab = TbBlockTabs.query.filter(TbBlockTabs.block_id.__eq__(tb_block.id),
                                        TbBlockTabs.id.__eq__(blocktab_id)).first()

    if not blocktab:
        flash(_('Tab for block not found message'), 'warning')
        return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

    epage = blocktab.epage
    epage_id = blocktab.epage_id

    if eblock_id:
        query = PageBlock.query.filter(PageBlock.id.__eq__(eblock_id))
        epageblock = query.first()

        if not epageblock:
            flash(_('Embedded pageblock with id %(id)s not found message', id=eblock_id), 'warning')
            return redirect(url_for('admin.manage_tb_block_tab', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab, blocktab_id=blocktab_id, active_tab_tab=active_tab_tab))

        if epageblock.embedded_page != epage:
            flash(_('Embedded pageblock with id %(id)s not found in current embedded page message', id=eblock_id), 'warning')
            return redirect(url_for('admin.manage_tb_block_tab', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab, blocktab_id=blocktab_id, active_tab_tab=active_tab_tab))
    else:
        epageblock = PageBlock()

    form = PageBlockForm(obj=epageblock, update=True if eblock_id else False, embedded=True)

    if form.validate_on_submit():
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'blocks_bg')
        file_to_remove = None

        try:
            form.populate_obj(epageblock)

            if not eblock_id:
                epageblock.embedded_page = epage
                db.session.add(epageblock)
                db.session.flush()
                attrs = epageblock.create_all_data()
                db.session.add(attrs)

            if epageblock.background_image and os.path.isfile(os.path.join(folder, epageblock.stored_background_image)):
                file_to_remove = os.path.join(folder, epageblock.stored_background_image)

            if form.bimage.data:
                if isinstance(form.bimage.data, basestring):
                    file_to_remove = False
                else:
                    if FileHelper.check_file_extension(form.bimage.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                        stored_filename, original_filename = FileHelper.generate_filename_hash(form.bimage.data.filename)
                        form.bimage.data.save(os.path.join(folder, stored_filename))
                        epageblock.background_image = [stored_filename, original_filename]
            else:
                epageblock.background_image = []

            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)

            if not eblock_id:
                flash(_("Can't add embedded pageblock message"), 'danger')
            else:
                flash(_("Can't save embedded pageblock message"), 'danger')
        else:
            if file_to_remove:
                try:
                    os.unlink(file_to_remove)
                except Exception as e:
                    current_app.logger.exception(e)

            if not eblock_id:
                flash(_('Embedded pageblock added successfully message'), 'success')
            else:
                flash(_('Embedded pageblock saved successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_tb_block_tab_blocks', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab, blocktab_id=blocktab_id, active_tab_tab=active_tab_tab, eblock_id=epageblock.id))

        return redirect(url_for('admin.manage_tb_block_tab', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab, blocktab_id=blocktab_id, active_tab_tab=active_tab_tab))

    return render_template('admin/tb/manage_tab_block.html',
                           page=page,
                           page_id=page_id,
                           pageblock=pageblock,
                           pageblock_id=pageblock_id,
                           active_tab=active_tab,
                           active_subtab=active_subtab,
                           blocktab=blocktab,
                           blocktab_id=blocktab_id,
                           epage=epage,
                           epage_id=epage_id,
                           epageblock=epageblock,
                           epageblock_id=eblock_id,
                           form=form,
                           tab_tabs=tab_tabs,
                           active_tab_tab=active_tab_tab)


@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>/data/<string:active_subtab>/<int:blocktab_id>/<any(blocks):active_tab_tab>/<int:eblock_id>/data', methods=['GET', 'POST'])
@login_required
def manage_tb_block_tab_blocks_data(page_id, pageblock_id, blocktab_id, eblock_id, active_subtab='general', active_tab='tabs', active_tab_tab='blocks', *args, **kwargs):
    """
    Редактирование контента встроенных в табы блоков.
    """
    page, pageblock = _pageblock_helper(page_id, pageblock_id, active_tab)
    tb_block = pageblock.get_data_object()

    if not tb_block or not isinstance(tb_block, Tb):
        abort(404)

    blocktab = TbBlockTabs.query.filter(TbBlockTabs.block_id.__eq__(tb_block.id),
                                        TbBlockTabs.id.__eq__(blocktab_id)).first()

    if not blocktab:
        flash(_('Tab for block not found message'), 'warning')
        return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

    epage = blocktab.epage
    epage_id = blocktab.epage_id

    query = PageBlock.query.filter(PageBlock.id.__eq__(eblock_id))
    epageblock = query.first()

    if not epageblock:
        flash(_('Embedded pageblock with id %(id)s not found message', id=eblock_id), 'warning')
        return redirect(url_for('admin.manage_tb_block_tab', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab, blocktab_id=blocktab_id, active_tab_tab=active_tab_tab))

    if epageblock.embedded_page != epage:
        flash(_('Embedded pageblock with id %(id)s not found in current embedded page message', id=eblock_id), 'warning')
        return redirect(url_for('admin.manage_tb_block_tab', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab, blocktab_id=blocktab_id, active_tab_tab=active_tab_tab))

    pb_data = epageblock.get_data_object()
    pageblock_form = epageblock.get_form()
    form = None

    if pageblock_form:
        form = pageblock_form(obj=pb_data)

        if form and form.validate_on_submit():
            try:
                form.populate_obj(pb_data)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash(_("Can't save embedded pageblock message"), 'danger')
            else:
                flash(_('Embedded pageblock saved successfully message'), 'success')

            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_tb_block_tab_blocks_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab, blocktab_id=blocktab_id, active_tab_tab=active_tab_tab, eblock_id=epageblock.id))

            return redirect(url_for('admin.manage_tb_block_tab', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab, blocktab_id=blocktab_id, active_tab_tab=active_tab_tab))

    return render_template('admin/tb/manage_tab_block_data.html',
                           page=page,
                           page_id=page_id,
                           pageblock=pageblock,
                           pageblock_id=pageblock_id,
                           active_tab=active_tab,
                           active_subtab=active_subtab,
                           blocktab=blocktab,
                           blocktab_id=blocktab_id,
                           epage=epage,
                           epage_id=epage_id,
                           epageblock=epageblock,
                           epageblock_id=eblock_id,
                           form=form,
                           tab_tabs=tab_tabs,
                           active_tab_tab=active_tab_tab)


@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>/data/slides/add', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>/data/slides/<int:slide_id>', methods=['GET', 'POST'])
@login_required
def manage_hd1sl1_slide(page_id, pageblock_id, active_subtab='slides', active_tab='blocks', slide_id=None, *args, **kwargs):
    page, pageblock = _pageblock_helper(page_id, pageblock_id, active_tab)
    hd1sl1_block = pageblock.get_data_object()

    if not hd1sl1_block or not isinstance(hd1sl1_block, Hd1Sl1):
        abort(404)

    if slide_id:
        slide = Hd1Sl1Slide.query.filter(Hd1Sl1Slide.block_id.__eq__(hd1sl1_block.id),
                                         Hd1Sl1Slide.id.__eq__(slide_id)).first()

        if not slide:
            flash(_('Hd1Sl1 Slide not found message'), 'warning')
            return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))
    else:
        slide = Hd1Sl1Slide(block=hd1sl1_block)

    form = CustomHd1Sl1SlideForm(obj=slide)

    if form.validate_on_submit():
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'blocks_ct')
        file_to_remove = None
        try:
            form.populate_obj(slide)
            if not slide_id:
                db.session.add(slide)

            if slide.slide and os.path.isfile(os.path.join(folder, slide.stored_slide)):
                file_to_remove = os.path.join(folder, slide.stored_slide)
            if form.slide_image.data:
                if isinstance(form.slide_image.data, basestring):
                    file_to_remove = False
                else:
                    if FileHelper.check_file_extension(form.slide_image.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                        stored_filename, original_filename = FileHelper.generate_filename_hash(form.slide_image.data.filename)
                        form.slide_image.data.save(os.path.join(folder, stored_filename))
                        slide.slide = [stored_filename, original_filename]
            else:
                slide.slide = []
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)
            flash(_('Error saving slide') if slide_id else _('Error adding slide'), 'danger')
            FileHelper.delete_file(os.path.join(folder, form.slide_image.data.filename if form.slide_image.data else ''))
        else:
            if file_to_remove:
                FileHelper.delete_file(file_to_remove)
            flash(_('Hd1Sl1 slide saved successfully message') if slide_id else _('Hd1Sl1 slide added successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_hd1sl1_slide', page_id=page.id, pageblock_id=pageblock_id, slide_id=slide.id))

        return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

    return render_template('admin/hd1sl1/manage_slide.html',
                           page=page,
                           page_id=page_id,
                           pageblock=pageblock,
                           pageblock_id=pageblock_id,
                           active_tab=active_tab,
                           active_subtab=active_subtab,
                           slide=slide,
                           slide_id=slide_id,
                           form=form)
