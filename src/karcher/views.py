# coding: utf-8
import os

from core.utils.text import text_2_html
from flask import abort, current_app, g, render_template, request, send_from_directory
from flask.ext.assets import Bundle
from jinja2 import evalcontextfilter
from users.models import User

from .init import app, db, assets, lm
from .models import NavHelper, NavigationGroup, NavigationGroupItem, NavigationGroupPopupItem
from .models import Page, PageBlock
from .site_models import Carwash, MapObject


@app.route('/')
def index():
    query = Page.query.filter(Page._main.is_(True), Page.language.__eq__('ru'))  # TODO: language.__eq__(g.language) instead of 'ru'
    page = query.first()

    if page:
        return page.render()

    abort(404)


"""
@app.route('/contacts')  # Временное представление для контактов TODO: Удалить
def contacts():
    query = Page.query.filter(Page.slug.__eq__('contacts'), Page.language.__eq__('ru'))
    page = query.first()

    if not page:
        abort(404)

    return page.render()
"""


@app.route('/api/map_object/<any(carwashes,services,affiliates,servicesandaffiliates):type_>', methods=['GET'])
def ajax_map_object_selected(type_):
    """
    Подгрузка карточки мойки (для Hd1Mp1
    """
    service_id = request.args.get('service_id', 0)
    if type_ == 'carwashes':
        item = Carwash.query.filter(Carwash.id.__eq__(service_id)).first()
    else:
        item = MapObject.query.filter(MapObject.id.__eq__(service_id)).first()

    if not item:
        abort(404)

    return render_template('elements/blocks/hd1mp1_card.html',
                           item=item)


@app.route('/<path:page_path>')
def nav_page_view(page_path):
    query = NavigationGroup.available()
    query = query.filter(NavigationGroup.page.has(Page.publish_ready.is_(True)))
    query = query.filter(NavigationGroup.path.__eq__(page_path),
                         NavigationGroup.behavior_.__eq__(NavHelper.behaviorPage))
    option = query.first()

    if not option:
        query = NavigationGroupItem.available()
        query = query.filter(NavigationGroupItem.page.has(Page.publish_ready.is_(True)))
        query = query.filter(NavigationGroupItem.path.__eq__(page_path),
                             NavigationGroupItem.behavior_.__eq__(NavHelper.behaviorPage))
        option = query.first()

    if not option:
        query = NavigationGroupPopupItem.available()
        query = query.filter(NavigationGroupPopupItem.page.has(Page.publish_ready.is_(True)))
        query = query.filter(NavigationGroupPopupItem.path.__eq__(page_path),
                             NavigationGroupPopupItem.behavior_.__eq__(NavHelper.behaviorPage))
        option = query.first()

    if not option:
        abort(404)

    return option.page.render()


@app.before_request
def before_request_callback():
    """
    TODO: Надо, чтобы это выполнялось только на фронте!
    """
    all_nav = NavigationGroup.available().all()
    g.footer_nav = [nav for nav in all_nav if nav.navtype_ == NavigationGroup.navtypeFooter]
    g.header_nav = [nav for nav in all_nav if nav.navtype_ == NavigationGroup.navtypeHeader]
    g.service_nav = [nav for nav in all_nav if nav.navtype_ == NavigationGroup.navtypeService]


@app.route('/public/<path:filename>')
def public_dir(filename):
    path = os.path.join(current_app.config.get('PROJECT_ROOT', ''), 'public')
    return send_from_directory(path, filename)


@app.template_filter('text_to_html')
def text_to_html_filter(value):
    return text_2_html(value)


@app.errorhandler(401)
def not_authorized(error):
    return render_template('401.html', error=error), 401


@app.errorhandler(404)
def error_404(error):
    return render_template('404.html', error=error), 404


@app.errorhandler(500)
def error_500(error):
    return render_template('500.html', error=error), 500


@app.errorhandler(501)
def not_implemented(error):
    return render_template('501.html', error=error), 501


@lm.user_loader
def load_user(user_id):
    return User.load_user(user_id)


assets.register('css_admin_common',
                Bundle('admin/css/bootstrap.css',
                       'admin/css/bootstrap-theme.css',
                       'admin/css/dataTables.bootstrap.css',
                       'admin/css/rowReorder.bootstrap.css',
                       'admin/css/bootstrap-colorselector.css',
                       'admin/css/bootstrap-datepicker.css',
                       'admin/css/select2.css',
                       'admin/css/common.css',
                       filters='jinja2',
                       output='css/final/admin.css'))

assets.register('js_admin_common',
                Bundle('js/jquery.min.js',
                       'admin/js/bootstrap.js',
                       'admin/js/jquery.dataTables.js',
                       'admin/js/dataTables.bootstrap.js',
                       'admin/js/dataTables.rowReorder.js',
                       'admin/js/bootstrap-colorselector.js',
                       'admin/js/bootstrap-datepicker.js',
                       'admin/js/buttonsbar.jquery.js',
                       'admin/js/tablefilters.jquery.js',
                       'admin/js/select2.js',
                       'admin/js/common.js',
                       filters='jinja2',
                       output='js/final/admin.js'))

assets.register('css_ie9_common',
                Bundle('css/ie9.css',
                       output='css/final/ie9.css'))

assets.register('css_front_common',
                Bundle('css/components/jquery.fancybox.css',
                       'css/common.css',
                       'css/fotorama.css',
                       'css/content.css',
                       filters='jinja2',
                       output='css/final/karcher.css'))

assets.register('js_front_common',
                Bundle('js/jquery.min.js',
                       'js/fotorama.js',
                       'js/components/jquery.fancybox.pack.js',
                       'js/init.js',
                       'js/move_on_scroll.js',
                       filters='jinja2',
                       output='js/final/karcher.js'))

assets.register('css_front_with_admin_common',
                Bundle('admin/css/bootstrap.css',
                       'admin/css/bootstrap-theme.css',
                       'admin/css/common.css',
                       'css/fotorama.css',
                       'css/content.css',
                       filters='jinja2',
                       output='css/final/akarcher.css'))

assets.register('js_front_with_admin_common',
                Bundle('js/jquery.min.js',
                       'admin/js/bootstrap.js',
                       'js/uikit.min.js',
                       'js/components/notify.min.js',
                       'js/fotorama.js',
                       'js/move_on_scroll.js',
                       filters='jinja2',
                       output='js/final/akarcher.js'))
