INSERT INTO sm_countries (id, title) VALUES (1, 'Центральный');
INSERT INTO sm_countries (id, title) VALUES (2, 'Южный');
INSERT INTO sm_countries (id, title) VALUES (3, 'Северо-Западный');
INSERT INTO sm_countries (id, title) VALUES (4, 'Дальневосточный');
INSERT INTO sm_countries (id, title) VALUES (5, 'Сибирский');
INSERT INTO sm_countries (id, title) VALUES (6, 'Уральский');
INSERT INTO sm_countries (id, title) VALUES (7, 'Приволжский');
INSERT INTO sm_countries (id, title) VALUES (8, 'Северо-Кавказский');
INSERT INTO sm_countries (id, title) VALUES (9, 'Крымский');

SELECT setval('sm_countries_id_seq', (SELECT MAX(id) FROM sm_countries));