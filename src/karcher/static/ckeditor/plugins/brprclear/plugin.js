CKEDITOR.plugins.add( 'brprclear', {
    requires: 'widget',
    icons: 'brprclear',
    init: function( editor ) {
        editor.widgets.add( 'brprclear', {
            button: "-----",
            template: '<div class="clear"></div>',
            allowedContent: 'div(!clear)',
            requiredContent: 'div(clear)',
            upcast: function( element ) { 
                return element.name == 'div' && element.hasClass( 'clear' );
            }
        });
    }
});
