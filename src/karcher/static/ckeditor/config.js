﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// config.plugins = 'dialogui,dialog,about,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,clipboard,button,panelbutton,panel,floatpanel,colorbutton,colordialog,templates,menu,contextmenu,div,resize,toolbar,elementspath,enterkey,entities,popup,filebrowser,find,fakeobjects,flash,floatingspace,listblock,richcombo,font,forms,format,horizontalrule,htmlwriter,iframe,wysiwygarea,image,indent,indentblock,indentlist,smiley,justify,menubutton,language,link,list,liststyle,magicline,maximize,newpage,pagebreak,pastetext,pastefromword,preview,print,removeformat,save,selectall,showblocks,showborders,sourcearea,specialchar,scayt,stylescombo,tab,table,tabletools,undo,wsc,lineutils,widget,filetools,notification,notificationaggregator,image2'; //,uploadwidget,uploadimage';
	config.plugins = 'dialogui,dialog,about,a11yhelp,dialogadvtab,basicstyles,menu,contextmenu,div,resize,toolbar,elementspath,enterkey,entities,popup,filebrowser,fakeobjects,floatingspace,listblock,richcombo,font,format,htmlwriter,wysiwygarea,image,indent,indentblock,indentlist,justify,menubutton,language,link,list,liststyle,magicline,maximize,preview,print,removeformat,save,selectall,showblocks,showborders,sourcearea,stylescombo,tab,table,tabletools,undo,wsc,lineutils,widget,filetools,notification,notificationaggregator,image2';
	config.skin = 'moono';

    config.toolbarGroups=[
        /*
        {name:"document", groups:["mode","document"]},
        {name:"clipboard",groups:["clipboard","undo"]},
        {name:"editing",groups:["find","selection"]},
        {name:"tools"},
        "/",
        {name:"paragraph",groups:["list","indent","blocks","align"]},
        {name:"links"},
        {name:"insert"},
        "/",
        {name:"styles"},
        {name:"basicstyles",groups:["basicstyles","cleanup"]},
        {name:"colors"},
        {name:"others"},
        {name:"about"},
        */
        {name:"basicstyles",groups:["basicstyles","cleanup"]},
        {name:"paragraph",groups:["list","indent","align"]},
        "/",
        {name:"links"},
        {name:"insert"},
        {name:"styles"},
        {name:"undo",groups:["undo"]},
    ];  

    config.docType = "<!doctype html>";
    config.bodyClass = "ckeditor";
    config.indentOffset = 2;
    config.indentUnit = "rem";

    config.image2_alignClasses = ["common-float_left", "common-align_center", "common-float_right"];
    // config.image2_captionedClass = "blockimage";
    config.justifyClasses = ["common-align_left", "common-align_center", "common-align_right", "common-align_justify"];
    config.fontSize_sizes = "10/1rem; 11/1.1rem; 12/1.2rem; 14/1.4rem; 16/1.6rem; 18/1.8rem; 20/2rem; " +
                            "22/2.2rem; 24/2.4rem; 28/2.8rem; 30/3rem; 36/3.6rem; 40/4rem; 48/4.8rem; 72/7.2rem;";
    config.stylesSet = [
        {name: 'MarkedHead',       element: 'h4', attributes: {'class': 'common-marked_head' }},
        {name: 'Marker',           element: 'span', attributes: {'class': 'common-marker' }},
        {name: 'Big',              element: 'big'},
        {name: 'Small',            element: 'small'},
        {name: 'Link with border', element: 'a', attributes: {'class': 'confirm_button' }},
    ];

    config.extraPlugins = "brprclear";
    config.removeDialogTabs = 'link:advanced';
    config.removeButtons = "Save,Smiley,CreateDiv,Underline,Strike,Iframe,Flash,HorizontalRule,SpecialChar,PageBreak,Font,FontSize";
    config.format_tags = "p;h2;h3;h4;h5;h6;address";
    config.disallowedContent = 'h1'

    config.filebrowserUploadUrl = '/admin/api/upload';
    // config.uploadUrl = '/uploader'; // To configure Uploading Dropped or Pasted Files

};

/*
for (var key in CKEDITOR.instances) {
    console.log(key);
    var editor = CKEDITOR.instances[key];
    if (key == "global_block_1_right_text") {
        if (editor) editor.destroy(true);
        CKEDITOR.replace(key, {
            toolbarGroups:[
                {name: 'links'}
            ],
            height: 500
        });
        console.log(editor.config);
    }
}
*/
