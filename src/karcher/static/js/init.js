$(function() {
	var kr = new Karcher();

    kr.ajaxLoader();
    kr.fotoramaGallery();
    kr.uncheckHeaderMenuItem();
    kr.bindTabs();
    kr.bindScrolline();
    kr.bindMap();
    kr.bindCnfr();
});


function Karcher() {
    this.idLoader = "ajax_indicator";
    this.ajaxIsOver = true;
}

Karcher.prototype.ajaxLoader = function() {
    var self = this;
 
    $(document).ajaxStart(function () {
        self.ajaxIsOver = false;
        setTimeout(function(){
            if (self.ajaxIsOver == false) {
                $("#" + self.idLoader).fadeIn(200);
            }
        }, 500);
    });
    $(document).ajaxStop(function(){
        self.ajaxIsOver = true;
        $("#" + self.idLoader).fadeOut(200);
    });
    $(document).ajaxError(function(e, xhr, settings, exept){
        $.error("При выполнении ajax-запроса страницы " + settings.url + " произошла ошибка.");
    });
}

Karcher.prototype.bindCnfr = function() {
    $(".js-cnfr-next_button").on("click", function() {
        var id = $(this).attr("for");
        if (typeof(id) == "undefined") return;
        $("#" + id).removeAttr("disabled");
        $("label[for=" + id + "]").removeClass("disabled");
    });

    function get_items(items, n, total) {
        var size = items.length;
        var count = Math.floor(size/total);
        var noun = size % total;
        var sums = $.map(new Array(noun + 1), function(val, i) {
            return i;
        });

        function get(arr, i) {
            return arr.slice(i, i+1).concat(arr.slice(-1))[0];
        }

        var start = n*count + get(sums, n);
        var finish = (n+1)*count + get(sums, n+1);
        return items.slice(start, finish);
    }

    $(".js-cnfr-tabs-select:last").on("change", function() {
        var $select = $(this);
        var $tab = $select.next(".js-cnfr-tabs-item");
        var $tabs = $(".js-cnfr-tabs-item").not($tab);
        var $columns = $(".content-column", $tab);

        var count = $columns.length;
        for (var i = 0; i < count; ++i) {
            var $column = $columns.eq(i).empty();
            get_items($tabs, i, count).each(function() {
                $(".cnfr-row", $(this)).each(function() {
                    var $row = $(this);
                    var $selects = $(".cnfr-item-select:checked", $row);
                    if ($selects.length == 0) return;
                    var $list = $("<ul></ul>");
                    $selects.each(function() {
                        var text = $(this).siblings(".cnfr-item-name, .cnfr-item-label, .cnfr-item-count").first().text();
                        $list.append($("<li></li>").text(text));
                    });
                    $column.append($row.children("h4").first().clone());
                    $column.append($list);
                });
            });
        }
    });
}

Karcher.prototype.bindMap = function() {
    var map_id = "service_yandex_map";

    var $map = $("#" + map_id);
    var $list = $(".map-list");
    var $form = $(".map-form");

    if ($map.length == 0 || $list.length == 0 || $form.length == 0) return;

    var scale = 10;
    var options = {
        iconLayout: 'default#image',
        iconImageHref: "{{ url_for('static', filename='images/blocks/map/icon.png') }}",
        iconImageSize: [35, 42],
        iconImageOffset: [-17, -35]
    }
    var coord_center = [55.75, 37.62];
    var map;

    function get_coord($item) {
        return $.map($item.data("coord").split(","), function(n, i) {
            return parseFloat(n);
        });
    }

    function set_placemarks() {
        map.geoObjects.removeAll();
        $list.find(".map-item-label").each(function(n) {
            var $item = $(this);
            var placemark = new ymaps.Placemark(get_coord($item), {
                hintContent: $item.attr("title"),
                balloonContent: $item.next(".map-item-content").html()
            }, options);
            map.geoObjects.add(placemark);
        });
    }

    ymaps.ready(function(){
        map = new ymaps.Map(map_id, {
            center: coord_center,
            zoom: scale
        });

        set_placemarks();
    });

    $list.on("click", ".map-item-label", function(event) {
        // передвинули
        var $item = $(this);
        map.panTo(get_coord($item));

        // отметили пункт
        $item.parent().addClass("current").siblings().removeClass("current");

        // получили статью
        var service_id = $item.data("service")*1;
        $.get($form.data("get_service"), {service_id: service_id}, function(html) {
            $(".js-map-article").html(html);
        });

        event.preventDefault();
    });

    $(".js-map-region", $form).on("change", function() {
        var region_id = $(this).val();
        $.getJSON($form.data("change_region"), {region_id: region_id}, function(data) {
            var options = $();
            for (var i = 0; i < data.cities.length; ++i) {
                var city = data.cities[i];
                var option = $("<option></option>").text(city[1]).val(city[0]);
                if (city[0] == data.current_city) option.prop("selected", true);
                options = options.add(option);
            } 
            $(".js-map-city", $form).html(options).find(":selected").change();
        });
        /*
        var data = {
            cities: [
                [1, "Москва"],
                [2, "Пенза"],
                [3, "Ульяновск"]
            ],
            current_city: 3
        };
        var options = $();
        for (var i = 0; i < data.cities.length; ++i) {
            var city = data.cities[i];
            var option = $("<option></option>").text(city[1]).val(city[0]);
            if (city[0] == data.current_city) option.prop("selected", true);
            options = options.add(option);
        } 
        $(".js-map-city", $form).html(options).find(":selected").change();
        */
    });

    $(".js-map-city", $form).on("change", function() {
        var city_id = $(this).val();
        $.get($form.data("change_city"), {city_id: city_id}, function(html) {
            $list.html(html);
            set_placemarks();
        });
        /*
        var html = '<h4>Другой город</h4>\
                   <ul>\
                        <li class="map-item">\
                            <a data-service="1" data-coord="55.86,37.5" class="map-item-label" href="#">«АвтоЛидер», улица Свердлова, 30, Балашиха</a>\
                            <div class="map-item-content">content</div>\
                        </li>\
                    </ul>';
        $list.html(html);
        set_placemarks();
        */
    });

    $(".js-map-net", $form).on("change", function() {
        var has_net = $(this).val();
        var city_id = $(".js-map-city", $form).val();
        $.get($form.data("change_city"), {
            city_id: city_id,
            has_net: has_net
        }, function(html) {
            $list.html(html);
            set_placemarks();
        });
    });
}

Karcher.prototype.bindTabs = function() {
    $("body").on("change", ".js-tabs-select", function(event) {
        var select = $(this);
        var id  = select.attr("id");
        if (typeof(id) == "undefined") return;
        var label = $("label[for=" + id + "]");
        if (label.parent().is("li")) label = label.parent();
        label.addClass("current").siblings().removeClass("current");
    });
}

Karcher.prototype.bindScrolline = function() {
    var $pages = $(".js-scrolline-page");
    var $items = $(".js-scrolline-item");
    if ($pages.length == 0 || $items.length != $pages.length) return;

    /*
    $("a", $items).on("click", function() {
        $(this).closest(".js-scrolline-item").addClass("current").siblings().removeClass("current");
    });
    */

    var scroll_timer = null;
    var timer_delay = 200;
    $(window).on("scroll", function(event) {
        clearTimeout(scroll_timer);
        scroll_timer = setTimeout(function() {
            var window_center = $(window).scrollTop() + 
                ((window.innerHeight > 0) ? window.innerHeight : screen.height)/2;
            var min_center = $(document).height();
            var current_n = 0;
            $pages.each(function(n) {
                var $page = $(this);
                var page_center = Math.floor($page.offset().top + $page.height()/2);
                var dt = Math.abs(page_center - window_center);
                if (dt < min_center) {
                    min_center = dt;
                    current_n = n;
                }
            });
            $items.removeClass("current").eq(current_n).addClass("current");
        }, timer_delay);
    });
}

Karcher.prototype.uncheckHeaderMenuItem = function() {
    var is_open = false;
    $(".header-menu-item-cell").on("click", function() {
        var $label = $(this);
        var attr_for = $label.attr("for");
        var $radio = $("#" + attr_for);
        if ($radio.prop("checked") === true) {
            is_open = true;
        }
    });
    $(".header-menu-item-radio").on("click", function() {
        var $radio = $(this);
        if (is_open === true) {
            $radio.prop("checked", false);
            is_open = false;
        }
    });
    $("body").on("click", function(event) {
        var $menu = $(event.target).closest(".header-menu");
        if ($menu.length == 1) return;
        $(".header-menu-item-radio:checked").prop("checked", false);
    });

    $(window).on("resize", function() {
        var $bar = $(".fixedbar");
        if ($bar.length == 0) return;
        var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        height -= ($bar.height() + 20);
        if (height > 0) {
            $(".header-menu-item-content").css("max-height", height);
        }
    }).resize();
}

Karcher.prototype.fotoramaGallery = function() {
    $(".gallery-item, .big_gallery-item").on("click", function(e) {
        e.preventDefault();
        var item = $(this);
        var n = item.index();
        var clone = $("<div></div>").addClass("fotorama").appendTo("body");
        item.parent().children().each(function() {
            clone.append($(this).clone());
        }); 
        var gallery = clone.fotorama({
            allowfullscreen: true,
            nav: "thumbs",
            fit: "scaledown",
            startindex: n
        }).on("fotorama:fullscreenexit", function(e, fotorama, extra) {
            fotorama.destroy();
            clone.remove();
        }); 
        gallery.data("fotorama").requestFullScreen();
            
    }); 
};

$(document).ready(function(){
    $("a.fancybox").fancybox();
});

