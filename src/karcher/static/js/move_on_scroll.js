$(function() {
	var mos = new MoveOnScroll();

    mos.bindScroll();
    mos.bindResizeTabs();
    mos.bindFixHeader();
});


function MoveOnScroll() {
    this.pages = $("body > .page");
    this.selector = "body, .header, .navbar";

    var size = this.pages.length;
    this.pages.each(function() {
        var $page = $(this);
        if (typeof($page.data("epage")) != "undefined") {
            $page.addClass("disabled");
        }
        $page.css("z-index", size);
        size -= 1;
    });

    var is_admin = window.location.pathname.indexOf("/admin") == 0;
    var self = this;
    $(".blocksplit, .blocksplit-arrow").on("click", function(e) {
        var $block = $(this);
        if (!$block.is(".blocksplit")) $block = $block.prev();
        var id = $block.data("epage");
        if (typeof(id) == "undefined") return;
        else id *= 1;
        var $first_enabled_page = self.pages.filter(function() {
            var $page = $(this);
            var epage_id = $page.data("epage");
            if (typeof(epage_id) == "undefined") return false;
            if (epage_id != id) {
                $page.addClass("disabled");
                return false;
            }
            return true;
        }).removeClass("disabled").first();
        if (is_admin !== true) {
            $("html, body").animate({
                scrollTop: Math.floor($first_enabled_page.offset().top) + "px"
            }, 200);
        }
        $block.addClass("current").parent().siblings().children(".blocksplit").removeClass("current");
    });
}

MoveOnScroll.prototype.bindFixHeader = function() {
    var $header = $(".header").first();
    var $toolbar = $(".toolbar").first();
    var class_name = "fixedbar";

    if ($header.length == 0) return;
    if ($toolbar.length == 0) {
        $header.addClass(class_name);
        return;
    }

    var height = $toolbar.height();
    var is_added = false;
    $(window).on("scroll", function() {
        var y = $(this).scrollTop();
        if (y >= height) {
            if (is_added == false) {
                $header.addClass(class_name);
                is_added = true;
            }
        }
        else {
            if (is_added == true) {
                $header.removeClass(class_name);
                is_added = false;
            }
        }
    });
}

MoveOnScroll.prototype.bindResizeTabs = function() {
    var self = this;
    var $tabs = $(".js-tabs-item");
    if ($tabs.length == 0) return;

    $(window).on("resize", function() {
        var window_height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        $tabs.each(function() {
            var $tab = $(this);
            var fake_height = parseFloat($tab.closest(".page-content").css("padding-top"));
            var tabshead_height = $tab.parent().children().first().outerHeight();
            $("> .page", $tab).first().height(
                window_height - fake_height - tabshead_height
            ).siblings(".page").height(window_height - fake_height);
        });
    }).resize();
}

MoveOnScroll.filterPages = function($page) {
    return $page.is(":visible");
}

MoveOnScroll.prototype.getScrollWidth = function() {
    var pr = $("<div></div>").addClass("move_on_scroll-test").appendTo("body");
    var ch = $("<div></div>").appendTo(pr);
    var dx = pr.width() - ch.width();
    pr.remove();
    return dx;
}

MoveOnScroll.prototype.disableScroll = function() {
    var self = this;
    var dx = self.getScrollWidth();
    $(self.selector).each(function() {
        var $item = $(this);
        if ($item.is("body") || $item.css("position") == "fixed") {
            $item.css({
                "border-right": dx + "px transparent solid"
            }).addClass("move_on_scroll-noscroll");
        }
    });
}
MoveOnScroll.prototype.enableScroll = function() {
    var self = this;
    $(self.selector).css({
        "border-right": "none"
    }).removeClass("move_on_scroll-noscroll");
}


MoveOnScroll.prototype.bindScroll = function() {
    var self = this;

    if (self.pages.length == 0) return;

    var $window = $(window);

    var is_animated = false;
    var is_scrolled = false;

    var timer = null;
    var timer_delay = 50;
    var scroll_delay = 500;

    function get_scroll_top() {
        var scroll_top = false;

        var w_height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        var w_top = $(window).scrollTop();

        self.pages.filter(function() {
            return MoveOnScroll.filterPages($(this));
        }).each(function(n) {
            var $page = $(this);

            var page_top = Math.floor($page.offset().top);
            var fake_height = Math.abs(parseFloat($page.css("margin-top")));
            var scroll_height = page_top;

            var w_bot = w_top + w_height - fake_height;

            if (page_top > w_top && page_top < w_bot) {
                var dy_top = page_top - w_top;
                var dy_bot = w_bot - page_top;
                if (dy_top > dy_bot) {
                    $prev_page = $page.prev(".page");
                    if ($prev_page.length > 0) {
                        // scroll_height = Math.floor($prev_page.offset().top) + $prev_page.height() - w_height;
                        scroll_height = page_top + fake_height - w_height;
                    }
                }
                scroll_top = scroll_height;
                return false;
            }
        });

        return scroll_top;
    }

    var resize_timer = null;
    $window.on("resize.on_low_height", function(event) {
        clearTimeout(resize_timer);
        resize_timer = setTimeout(function() {
            $window.scroll();
        }, timer_delay);
    });

    var limit_height = 1080;
    $window.on("scroll.on_low_height", function(event) {
        clearTimeout(timer);
        is_scrolled = true;

        var w_height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        if (w_height > limit_height) return;

        timer = setTimeout(function() {
            is_scrolled = false;
            if (is_animated == false) {
                var scroll_top = get_scroll_top();
                if (scroll_top === false) return;
                is_animated = true;
                self.disableScroll();
                $("html, body").animate({
                    scrollTop: scroll_top + "px"
                }, scroll_delay, function() {
                });
            }
            else {
                is_animated = false;
                self.enableScroll();
            }
        }, timer_delay);
    });

}
