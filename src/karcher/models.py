# coding: utf-8
import os
import re
import sqlalchemy as sa
import sqlamp
import uuid
import site_models

from core.models import BaseTreeNode, classproperty
from core.utils.files import FileHelper
from flask import abort, current_app, flash, redirect, render_template, request, url_for
from flask.ext.babel import gettext as _, lazy_gettext as __
from jinja2.exceptions import TemplateNotFound
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property
from werkzeug.routing import BaseConverter

from .init import db


class BlockCollection(object):
    class __metaclass__(type):
        def __init__(cls, name, bases, attrs):
            super(type, cls).__init__(name, bases, attrs)

            for c in bases:
                if isinstance(c, cls):
                    raise TypeError(str(c.__name__) + ' is final')

    collection = dict()

    @classmethod
    def register_block_class(cls, reg_cls):
        cls.collection.setdefault(reg_cls.__name__,
                                  dict(class_=reg_cls,
                                       type_=reg_cls.__name__,
                                       may_be_embedded=reg_cls.may_be_embedded,
                                       description=getattr(reg_cls, '__description__', __('Block default description'))))
        return reg_cls

    @classmethod
    def listing(cls, embedded=False):
        if not embedded:
            return [(c['type_'], c['description']) for c in cls.collection.values()]

        a = [(c['type_'], c['description']) for c in cls.collection.values() if c['may_be_embedded'] is True]
        return a

    @classmethod
    def check_block(cls, clsname):
        """ TODO: Maybe useless??? """
        return clsname in cls.collection.keys()


def register_as_block(cls):
    """ Decorator for register block classes """
    return BlockCollection.register_block_class(cls)


class ColorDictionary(db.Model):
    typeFont = 'Font'
    typeBackground = 'Background'

    __tablename__ = 'color_dictionary'

    id = db.Column(db.Integer, primary_key=True)
    color = db.Column(db.String(7))
    type_ = db.Column(db.Enum(typeFont, typeBackground, name='color_types'), db.DefaultClause(typeFont))
    position = db.Column(db.Integer, default=0, server_default='0')

    __table_args__ = (db.UniqueConstraint('color', 'type_'), )

    def __unicode__(self):
        return u'{0}'.format(self.color)

    @classmethod
    def font_colors(cls):
        query = cls.query
        query = query.filter(cls.type_.__eq__(cls.typeFont))
        query = query.order_by(cls.position.asc())
        return query

    @classmethod
    def background_colors(cls):
        query = cls.query
        query = query.filter(cls.type_.__eq__(cls.typeBackground))
        query = query.order_by(cls.position.asc())
        return query


class AbstractBlock(object):
    """ Base class for all other types of block. """
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = db.Column(db.Integer, primary_key=True)

    @declared_attr
    def pageblock_id(cls):
        return db.Column('pageblock_id', db.ForeignKey('page_blocks.id'))

    @declared_attr
    def block(cls):
        return db.relationship('PageBlock', backref=db.backref('%s_attributes' % cls.__name__.lower(), uselist=False, lazy='joined', cascade='all, delete-orphan'))

    @classproperty
    def may_be_embedded(self):
        return True

    @classproperty
    def not_use_common_render(self):
        return False

    def extended_js_block(self, editable=False):  # TODO: Нужно ли это?
        """
        Plain text for block js.
        """
        return None

    def extended_css_block(self, editable=False):  # TODO: Нужно ли это?
        """
        Plain text for block css.
        """
        return None

    # Usefull methods (BlockAPI)
    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready:
            return

        # Collect data
        kwargs.setdefault('block_data', self._get_columns_data())

        try:
            return render_template(self._default_template,
                                   editable=editable,
                                   global_block_id=self.block.global_id if self.block else None,
                                   block_auto_height=self.block.content_auto_height,
                                   block_height_padding=self.block.height_padding,
                                   **kwargs)
        except TemplateNotFound as e:
            current_app.logger.exception(e)

        return 'TODO: Abstract block Default template or selfname template'
        # raise NotImplementedError()

    def embedded_render(self, editable=False, *args, **kwargs):
        return None

    def preview_icon(self):
        # TODO: Make default preview image
        fname = url_for('admin.static', filename='images/blocks/{0}.png'.format(cls.__name__.lower()))
        return fname

    @property
    def publish_ready(self):
        # TODO: Check template exists!
        return True

    def __get_columns(self):
        """ Returns table columns for self block class. """
        cls = self.__class__
        ready = {}

        if hasattr(cls, '__table__'):
            for item in cls.__table__.columns.items():
                ready.setdefault(item[0], item[1])

        return ready

    def _get_columns_data(self, exclude=[]):
        """ Returns table columns data for self block class. """
        columns = self.__get_columns()
        ready = {}

        for col in columns.items():
            if col not in exclude:
                ready.setdefault(col[0], getattr(self, col[0]))

        return ready

    def get_form(self):
        """
        Возвращает форму для редактирования данных блока или None.
        Случай если None, может говорить о том, что:
            1. Данные блока имеют сложную структуру и редактирование в админке имеет свой механизм редактирования
               со своими шаблонами, для этого вызывается метод custom_manage_view
            2. Блок не имеет данных для редактирования (по идее очень редкая ситуация)
        """
        form = None

        try:
            mod = __import__('admin.block_forms')
            form = getattr(mod.block_forms, '{0}Form'.format(self.__class__.__name__), None)
        except ImportError as e:
            current_app.logger.exception(e)

        return form

    def custom_manage_view(self, *args, **kwargs):
        """
        Возвращает произвольную страницу редактирования
        """
        return None

    @property
    def _admin_manage_template(self):
        fname = 'manage.html'
        return os.path.join('admin', '{0}'.format(self.__class__.__name__.lower()), fname)

    @property
    def _default_template(self):
        fname = '{0}.html'.format(self.__class__.__name__.lower())
        return os.path.join('elements', 'blocks', fname)


class EmbeddedPage(db.Model):
    """
    Встраиваемая страница.
    """
    __tablename__ = 'epages'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    blocks = db.relationship('PageBlock', uselist=True, cascade='save-update, merge, delete', backref=db.backref('embedded_page', uselist=False))

    def render(self, editable=False, *args, **kwargs):
        r = []
        blocks_total = len([b.id for b in self.blocks if b.status == PageBlock.statusActive])

        for b_idx, block in enumerate(self.blocks):
            if block.status == PageBlock.statusActive:
                kwargs.setdefault('epage_id', self.id)
                block_render = block.render(editable=editable, *args, **kwargs)

                if block_render:
                    r.append(block_render)

        block_collection = '\n'.join(r)
        kwargs.setdefault('editable', editable)

        return render_template('embedded_page.html',
                               e_block_collection=block_collection,
                               **kwargs)


class Page(db.Model):
    """
    Самодостаточная страница
        - имеет свой адрес, цепляется к элементам навигации (меню, ссылка, и т.д)
    """
    __tablename__ = 'pages'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    published = db.Column(db.Boolean, default=True, server_default='true')

    slug = db.Column(db.String, nullable=False)
    language = db.Column(db.String(2), nullable=False)

    _main = db.Column(db.Boolean, default=False, server_default='false')

    meta_title = db.Column(db.Text)
    meta_description = db.Column(db.Text)
    meta_keywords = db.Column(db.Text)

    blocks = db.relationship('PageBlock', uselist=True, cascade='save-update, merge, delete', backref=db.backref('page', uselist=False))

    __table_args__ = (
        db.UniqueConstraint('slug', 'language'),
        db.Index('only_one_main_page_for_language', _main, language, unique=True, postgresql_where=(_main))
    )

    @hybrid_property
    def publish_ready(self):
        return self.published

    @publish_ready.expression
    def publish_ready(cls):
        return cls.published.is_(True)

    @property
    def main(self):
        return self._main

    @main.setter
    def main(self, value):
        # TODO: cls.language.__eq__(g.language) instead of
        if value is True:
            cls = self.__class__
            stmt = cls.__table__.update().where(db.and_(cls._main.is_(True), cls.language.__eq__('ru'), ~cls.id.__eq__(self.id))).values(_main=False)
            db.session.execute(stmt)

        self._main = value

    @classmethod
    def admin_list(cls, language=current_app.config['BABEL_DEFAULT_LOCALE'], with_count=False):
        query = cls.query
        count = query.count() if with_count else 0

        if with_count:
            return query, count

        return query

    @classmethod
    def available(cls):
        query = cls.query
        query = query.filter(cls.published.is_(True),
                             cls.language.__eq__(current_app.config.get('BABEL_DEFAULT_LOCALE')))
        return query

    def render(self, editable=False, *args, **kwargs):
        r = []
        # Count blocks
        blocks_total = len([b.id for b in self.blocks if b.status == PageBlock.statusActive])

        for b_idx, block in enumerate(self.blocks):
            if block.status == PageBlock.statusActive:
                block_render = block.render(editable=editable, block_index=b_idx, blocks_total=blocks_total, page_data=self, *args, **kwargs)

                if block_render is not None:
                    r.append(block_render)

        if not r:
            if not editable:
                abort(404)

            r.append(u'<h1>{0}</h1>'.format(_('No blocks were added to page message')))

        block_collection = '\n'.join(r)
        kwargs.setdefault('editable', editable)

        kwargs.setdefault('page_data', {'page': self,
                                        'page_id': self.id})

        return render_template('page.html',
                               block_collection=block_collection,
                               **kwargs)


class PageBlock(db.Model):
    statusActive = 'Active'
    statusTrash = 'Trash'
    sizeBackgrounds = [
        ('auto', __('Background-size auto option')),
        ('contain', __('Background-size contain option')),
        ('cover', __('Background-size cover option'))
    ]
    positionBackgrounds = [
        ('center bottom', __('Background-position center bottom option')),
        ('center center', __('Background-position center center option')),
        ('center top', __('Background-position center top option')),
        ('left bottom', __('Background-position left bottom option')),
        ('left center', __('Background-position left center option')),
        ('left top', __('Background-position left top option')),
        ('right bottom', __('Background-position right bottom option')),
        ('right center', __('Background-position right center option')),
        ('right top', __('Background-position right top option'))
    ]

    __tablename__ = 'page_blocks'

    id = db.Column(db.Integer, primary_key=True)
    page_id = db.Column(db.Integer, db.ForeignKey(Page.id))
    embedded_page_id = db.Column(db.Integer, db.ForeignKey(EmbeddedPage.id))
    position = db.Column(db.Integer, default=0, server_default='0')
    comment = db.Column(db.String)
    published = db.Column(db.Boolean, default=False, server_default='false')
    status = db.Column(db.Enum(statusActive, statusTrash, name='block_status'), db.DefaultClause(statusActive))
    type_ = db.Column(db.String, nullable=False)

    # Common block design
    font_color_id = db.Column(db.Integer, db.ForeignKey('color_dictionary.id'), nullable=True)
    background_color_id = db.Column(db.Integer, db.ForeignKey('color_dictionary.id'), nullable=True)

    font_color = db.relationship('ColorDictionary', foreign_keys=[font_color_id], lazy='joined')
    background_color = db.relationship('ColorDictionary', foreign_keys=[background_color_id], lazy='joined')

    background_size = db.Column(db.Enum(*[sb[0] for sb in sizeBackgrounds], name='background_sizes'), db.DefaultClause(sizeBackgrounds[0][0]))
    background_position = db.Column(db.Enum(*[pb[0] for pb in positionBackgrounds], name='background_positions'), db.DefaultClause(positionBackgrounds[1][0]))

    content_auto_height = db.Column(db.Boolean, default=True, server_default='true')
    height_padding = db.Column(db.Integer, default=200, server_default='200')

    """
    Массив:
        первое значение - название файла на диске.
        второе значение - оригинальное название файла.
    """
    background_image = db.Column(sa.dialects.postgresql.ARRAY(db.Text), server_default='{}')
    # TODO: background height param

    __mapper_args__ = {'order_by': position.asc()}
    __table_args__ = (
        db.CheckConstraint(~db.and_(page_id.is_(None), embedded_page_id.is_(None))),
    )

    def get_data_object(self):
        rel = self._get_attr_relation()

        if rel:
            return rel

        return None

    @property
    def original_background_image(self):
        """
        Возвращает оригинальное название файла изображения, если его нет!!! - название файла на диске,
        если изображения нет - None.
        """
        if self.background_image:
            if len(self.background_image) > 1:
                return self.background_image[1]

            return self.stored_background_image

        return None

    @property
    def stored_background_image(self):
        """
        Возвращает название файла на диске для фонового изображения. Или None.
        """
        if self.background_image:
            return self.background_image[0]

        return None

    def _get_relationship_name(self):
        """ Returns current block type attribute relationship name. """
        return '%s_attributes' % (self.type_.lower())

    def create_all_data(self):
        """ Create and return specified type object  """
        return eval(self.type_)(block=self)

    def _get_attr_relation(self):
        """ Returns current block type attribute relationship. """
        attr = self._get_relationship_name()

        if hasattr(self, attr):
            return getattr(self, attr)

        return None

    def _get_block_inline_style(self):
        attrs = [u'background-size: {0}'.format(self.background_size),
                 u'background-position: {0}'.format(self.background_position),
                 u'background-repeat: no-repeat']

        if self.font_color:
            attrs.append(u'color: {0}'.format(self.font_color.color))

        if self.background_color:
            attrs.append(u'background-color: {0}'.format(self.background_color.color))

        if self.stored_background_image:
            attrs.append(u'background-image: url({0})'.format(url_for('public_dir', filename=os.path.join('images', 'blocks_bg', self.stored_background_image))))

        if attrs:
            return u' style="{0}"'.format(u';'.join(attrs))

        return u''

    def _generate_block_token(self):
        return uuid.uuid4().hex

    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready and not editable:
            return

        rel = self._get_attr_relation()

        inner_block_content = ''
        embedded = ''

        if rel:
            inner_block_content = rel.render(editable=editable, *args, **kwargs)

            if inner_block_content is None:
                return

            embedded = rel.embedded_render(editable=editable, *args, **kwargs)  # Render embedded block data
            kwargs.setdefault('not_use_common_block_render', rel.not_use_common_render)

        return render_template('elements/common_block.html',
                               editable=editable,
                               global_block_id=self.global_id,
                               block_only_id=self.id,
                               block_inline_style=self._get_block_inline_style(),
                               inner_block_content=inner_block_content,
                               block_token=self._generate_block_token(),
                               embedded=embedded,
                               block_auto_height=self.content_auto_height,
                               block_height_padding=self.height_padding,
                               **kwargs)

    @property
    def publish_ready(self):
        return self.status == self.statusActive and self.published is True

    @property
    def global_id(self):
        return u'global_block_{0}'.format(self.id)

    def get_form(self):
        rel = self._get_attr_relation()
        form = None

        if rel:
            form = rel.get_form()

        return form

    def custom_manage_view(self, *args, **kwargs):
        rel = self._get_attr_relation()
        view = None

        if rel:
            view = rel.custom_manage_view(*args, **kwargs)

        return view

    @classmethod
    def before_insert_listener(cls, mapper, connection, target):  # TODO: Make a mixin or another def ?
        count = db.session.query(sa.func.count(cls.id))
        if target.page_id:
            count = count.filter(cls.page_id.__eq__(target.page_id))
        elif target.embedded_page_id:
            count = count.filter(cls.embedded_page_id.__eq__(target.embedded_page_id))
        count = count.first()
        count = count[0] if count else 0
        target.position = count + 1


sa.event.listen(PageBlock, 'before_insert', PageBlock.before_insert_listener)


# -- BLOCKS --
#
# Сложный блок с табами
#

class TbBlockTabs(db.Model):
    __tablename__ = 'tb_tabs'

    id = db.Column(db.Integer, primary_key=True)
    caption = db.Column(db.Text)
    published = db.Column(db.Boolean, default='true', server_default='true')
    position = db.Column(db.Integer, default=0, server_default='0')

    epage_id = db.Column(db.Integer, db.ForeignKey(EmbeddedPage.id), nullable=False)
    epage = db.relationship(EmbeddedPage, cascade='save-update, merge, delete')

    block_id = db.Column(db.Integer, db.ForeignKey('tb.id'), nullable=False)
    block = db.relationship('Tb', uselist=False, backref=db.backref('tabs', lazy='dynamic', uselist=True))

    @classmethod
    def before_insert_listener(cls, mapper, connection, target):  # TODO: Make a mixin or another def ?
        count = db.session.query(sa.func.count(cls.id))
        count = count.filter(cls.block_id.__eq__(target.block_id))
        count = count.first()
        count = count[0] if count else 0
        target.position = count + 1

    @hybrid_property
    def publish_ready(self):
        return self.published

    def render(self, editable=False, *args, **kwargs):
        kwargs.setdefault('render_in_tab', True)  # Параметр говорит о том что блоки рендерятся в табе
        return self.epage.render(editable=editable, *args, **kwargs)


sa.event.listen(TbBlockTabs, 'before_insert', TbBlockTabs.before_insert_listener)


@register_as_block
class Tb(db.Model, AbstractBlock):
    __description__ = _('Tb block description')

    typeSimple = 'Simple'
    typeMetroHorizontal = 'MetroHorizontal'
    typeMetroVertical = 'MetroVertical'
    typeIcons = 'Icons'

    type_ = db.Column(db.Enum(typeSimple, typeMetroHorizontal, typeMetroVertical, typeIcons, name='tb_block_types'), db.DefaultClause(typeSimple))

    @classproperty
    def may_be_embedded(self):
        return False

    @hybrid_property
    def available_tabs(self):
        return self.tabs.filter(TbBlockTabs.publish_ready.is_(True)).order_by(TbBlockTabs.position.asc())

    @classmethod
    def type_listing(cls):
        return [(cls.typeSimple, __('Tabs block simple type block option')),
                (cls.typeMetroHorizontal, __('Tabs block metro horizontal type block option'))]
        # (cls.typeMetroVertical, __('Tabs block metro vertical type block option')),
        # (cls.typeIcons, __('Tabs block icons type block option'))]

    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready:
            return

        kwargs.setdefault('tb_block_data', self._get_columns_data())
        kwargs.setdefault('render_block_in_tab', True)  # Флаг для шаблонов - указывает что блок рендерится внутри табов

        tabs = self.available_tabs.all()
        kwargs.setdefault('block_tabs', tabs)
        kwargs.setdefault('block_tab_renders', {t.id: t.render(editable=editable, *args, **kwargs) for t in tabs})

        try:
            return render_template(self._default_template,
                                   editable=editable,
                                   global_block_id=self.block.global_id if self.block else None,
                                   **kwargs)
        except TemplateNotFound as e:
            current_app.logger.exception(e)

        return 'TODO: Tb block template'

    def custom_manage_view(self, page, page_id, pageblock, pageblock_id, active_subtab='general', active_tab=None, *args, **kwargs):
        """
        Запускается из стандартного представления для редактирования
        Сюда должны передаваться
        """
        subtabs = [{'alias': 'general', 'title': _('General Tb tab')},
                   {'alias': 'tabs', 'title': _('Tabs Tb tab')}]
        allowed_subtabs = [st['alias'] for st in subtabs]
        active_subtab = active_subtab.lower()

        if active_subtab not in allowed_subtabs:
            abort(404)

        pb_data = pageblock.get_data_object()
        form = None
        block_tabs = []

        if active_subtab == 'general':
            from admin.block_forms import CustomTbForm
            form = CustomTbForm(obj=pb_data)
        elif active_subtab == 'tabs':
            block_tabs = pb_data.tabs

        if form and form.validate_on_submit():
            try:
                form.populate_obj(pb_data)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash(_("Can't save pageblock data message"), 'danger')
            else:
                flash(_('Pageblock data saved successfully message'), 'success')

            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id))

            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))
        # elif not form and request.method == 'POST':
        #     if active_tab == 'tabs':
        #         pass

            return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

        return render_template(self._admin_manage_template,
                               page=page,
                               page_id=page_id,
                               pageblock=pageblock,
                               pageblock_id=pageblock_id,
                               form=form,
                               subtabs=subtabs,
                               active_tab=active_tab,
                               active_subtab=active_subtab,
                               current_block=self,
                               block_tabs=block_tabs,
                               **kwargs)


#
# Сложный первый блок для главной
#
@register_as_block
class Hd1Tx2Sp2(db.Model, AbstractBlock):
    __description__ = _('Hd1Tx2Sp2 block description')

    header = db.Column(db.Text)
    left_text = db.Column(db.Text)
    right_text = db.Column(db.Text)

    left_epage_id = db.Column(db.Integer, db.ForeignKey(EmbeddedPage.id), nullable=False)
    right_epage_id = db.Column(db.Integer, db.ForeignKey(EmbeddedPage.id), nullable=False)

    left_epage = db.relationship(EmbeddedPage, foreign_keys=[left_epage_id], cascade='save-update, merge, delete')
    right_epage = db.relationship(EmbeddedPage, foreign_keys=[right_epage_id], cascade='save-update, merge, delete')

    def __init__(self, *args, **kwargs):
        super(Hd1Tx2Sp2, self).__init__(*args, **kwargs)
        self.left_epage = EmbeddedPage(title='Hd1Tx2Sp2 Left embedded page')
        self.right_epage = EmbeddedPage(title='Hd1Tx2Sp2 Right embedded page')

    @classproperty
    def may_be_embedded(self):
        return False

    def __translations(self):
        """ Внутренние константы блока """
        _('Hd1Tx2Sp2 Left embedded page')
        _('Hd1Tx2Sp2 Right embedded page')

    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready:
            return

        kwargs.setdefault('block_data', self._get_columns_data(exclude=['left_epage_id', 'right_epage_id']))

        try:
            return render_template(self._default_template,
                                   editable=editable,
                                   global_block_id=self.block.global_id if self.block else None,
                                   **kwargs)
        except TemplateNotFound as e:
            current_app.logger.exception(e)

        return 'TODO: Hd1Tx2Sp2 block template'

    def embedded_render(self, editable=False, *args, **kwargs):
        embedded = ''

        if self.left_epage:
            embedded += self.left_epage.render(editable=editable, *args, **kwargs)
        if self.right_epage:
            embedded += self.right_epage.render(editable=editable, *args, **kwargs)

        return embedded

    def custom_manage_view(self, page, page_id, pageblock, pageblock_id, active_subtab='general', active_tab=None, *args, **kwargs):
        """
        Запускается из стандартного представления для редактирования
        Сюда должны передаваться
        """
        subtabs = [{'alias': 'general', 'title': _('General Hd1Tx2Sp2 tab')},
                   {'alias': 'blocks', 'title': _('Blocks Hd1Tx2Sp2 tab')}]
        allowed_subtabs = [st['alias'] for st in subtabs]
        active_subtab = active_subtab.lower()

        if active_subtab not in allowed_subtabs:
            abort(404)

        pb_data = pageblock.get_data_object()
        form = None
        left_blocks = []
        right_blocks = []

        if active_subtab == 'general':
            from admin.block_forms import CustomHd1Tx2Sp2Form
            form = CustomHd1Tx2Sp2Form(obj=pb_data)
        elif active_subtab == 'blocks':
            left_blocks = self.left_epage.blocks if self.left_epage else []
            right_blocks = self.right_epage.blocks if self.right_epage else []

        if form and form.validate_on_submit():
            try:
                form.populate_obj(pb_data)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash(_("Can't save pageblock data message"), 'danger')
            else:
                flash(_('Pageblock data saved successfully message'), 'success')

            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id))

            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))
        elif not form and request.method == 'POST':
            if active_tab == 'blocks':
                left_positions = request.form.getlist('left_positions', type=int)
                right_positions = request.form.getlist('right_positions', type=int)
                to_remove = request.form.getlist('toremove', type=int)
                trash_selected = 'trash_selected' in request.form

                if (left_positions or right_positions) and not trash_selected:
                    try:
                        for b in left_blocks:
                            if b.id in left_positions:
                                b.position = left_positions.index(b.id) + 1

                        for b in right_blocks:
                            if b.id in right_positions:
                                b.position = right_positions.index(b.id) + 1

                        db.session.commit()
                    except Exception as e:
                        db.session.rollback()
                        current_app.logger.exception(e)
                    else:
                        flash(_('Blocks order saved successfully message'), 'success')

                if to_remove and trash_selected:
                    try:
                        lb = {b.id: b for b in left_blocks}
                        rb = {b.id: b for b in right_blocks}
                        blocks = dict(lb.items() + rb.items())

                        for tr in to_remove:
                            obj = blocks.get(tr)

                            if obj:
                                if obj.status == PageBlock.statusActive:  # Mark as Trashed if active
                                    obj.status = PageBlock.statusTrash
                                elif obj.status == PageBlock.statusTrash:  # Physically remove object if trashed
                                    db.session.delete(obj)

                        db.session.commit()
                    except Exception as e:
                        db.session.rollback()
                        current_app.logger.exception(e)
                    else:
                        flash(_('Selected pageblocks moved to trash message'), 'success')

            return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

        return render_template(self._admin_manage_template,
                               page=page,
                               page_id=page_id,
                               pageblock=pageblock,
                               pageblock_id=pageblock_id,
                               form=form,
                               subtabs=subtabs,
                               active_tab=active_tab,
                               active_subtab=active_subtab,
                               current_block=self,
                               left_blocks=left_blocks,
                               right_blocks=right_blocks,
                               **kwargs)


@register_as_block
class Hd1Tx2Ft1(db.Model, AbstractBlock):
    __description__ = _('Hd1Tx2Ft block description')

    header = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    left_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    right_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    footer = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)


@register_as_block
class Hd1Tx1Contacts(db.Model, AbstractBlock):
    __description__ = _('Hd1Tx1Contacts')

    header = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')
    content = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')


@register_as_block
class Hd1In2Tx2In2(db.Model, AbstractBlock):
    __description__ = _('Hd1In2Tx2In2 block description')

    header = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')
    lt_incut = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')
    rt_incut = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')
    left_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')
    right_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')
    lb_incut = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')
    rb_incut = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')


@register_as_block
class Tx4In1(db.Model, AbstractBlock):
    """ 4 + 1 """
    __description__ = _('Tx4In1 block description')

    lt_text = db.Column(db.Text)
    rt_text = db.Column(db.Text)
    lb_text = db.Column(db.Text)
    rb_text = db.Column(db.Text)
    incut = db.Column(db.Text)


@register_as_block
class Tx4InIm1(db.Model, AbstractBlock):
    """ 4 + 1 image """
    __description__ = _('Tx4InIm1 block description')

    lt_text = db.Column(db.Text)
    rt_text = db.Column(db.Text)
    lb_text = db.Column(db.Text)
    rb_text = db.Column(db.Text)
    incut_image = db.Column(sa.dialects.postgresql.ARRAY(db.Text), default=[], server_default='{}')
    incut = db.Column(db.Text)


@register_as_block
class Hd1Im1Tx1R(db.Model, AbstractBlock):
    """ Заголовок + 2 колонки в обратном порядке """
    __description__ = _('Hd1Im1Tx1R block description')

    header = db.Column(db.Text)
    content = db.Column(db.Text)


@register_as_block
class Hd1Tx2Hd1Im1(db.Model, AbstractBlock):
    __description__ = _('Hd1Tx2Hd1Im1 block description')

    header_1 = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    left_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    right_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    header_2 = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)


@register_as_block
class Hd1Tx1Tx2(db.Model, AbstractBlock):
    __description__ = _('Hd1Tx1Tx2 block description')

    header = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')
    content = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')
    left_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')
    right_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;')


@register_as_block
class Hd1Tx2Calc(db.Model, AbstractBlock):
    __description__ = _('Hd1Tx2Hd1Im1 block description')

    header = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    left_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    right_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)


@register_as_block
class Tx2(db.Model, AbstractBlock):
    __description__ = _('Tx2 block description')

    left_text = db.Column(db.Text)
    right_text = db.Column(db.Text)


@register_as_block
class Fq1(db.Model, AbstractBlock):
    __description__ = _('Fq1 block description')

    @property
    def available_faqs(self):
        from .site_models import FAQ, FAQCategory
        query = FAQCategory.available()
        query = query.options(db.subqueryload(FAQCategory.questions))
        return query

    @property
    def publish_ready(self):
        return True

    def _get_columns_data(self, exclude=[]):
        ready = super(Fq1, self)._get_columns_data(exclude=exclude)
        ready.setdefault('available_faqs', self.available_faqs.all())
        return ready


class Hd1Sl1Slide(db.Model):
    textpositionLeft = 'Left'
    textpositionCenter = 'Center'
    textpositionRight = 'Right'

    __tablename__ = 'hd1sl1_slides'

    id = db.Column(db.Integer, primary_key=True)
    slide = db.Column(sa.dialects.postgresql.ARRAY(db.Text), default=[], server_default='{}')
    description = db.Column(db.Text, default='', server_default='')
    published = db.Column(db.Boolean, default=True, server_default='true')
    position = db.Column(db.Integer, default=0, server_default='0')
    block_id = db.Column(db.Integer, db.ForeignKey('hd1sl1.id'), nullable=False)
    font_color_id = db.Column(db.Integer, db.ForeignKey('color_dictionary.id'), nullable=True)
    textposition = db.Column(db.Enum(textpositionLeft, textpositionCenter, textpositionRight, name='slide_text_positions'), db.DefaultClause(textpositionRight))

    block = db.relationship('Hd1Sl1', uselist=False, backref=db.backref('slides', lazy='dynamic', uselist=True))
    font_color = db.relationship('ColorDictionary', lazy='joined')

    @classmethod
    def textposition_listing(cls):
        return [(cls.textpositionLeft, __('Slide text position left option')),
                (cls.textpositionCenter, __('Slide text position center option')),
                (cls.textpositionRight, __('Slide text position right option'))]

    @hybrid_property
    def publish_ready(self):
        return self.published

    @classmethod
    def before_insert_listener(cls, mapper, connection, target):  # TODO: Make a mixin or another def ?
        count = db.session.query(sa.func.count(cls.id))
        count = count.filter(cls.block_id.__eq__(target.block_id))
        count = count.first()
        count = count[0] if count else 0
        target.position = count + 1

    @property
    def original_slide(self):
        if self.slide:
            if len(self.slide) > 1:
                return self.slide[1]
            return self.stored_slide
        return None

    @property
    def stored_slide(self):
        if self.slide:
            return self.slide[0]
        return None

    @property
    def slide_url(self):
        if self.stored_slide:
            return url_for('public_dir', filename=os.path.join('images', 'blocks_ct', self.stored_slide))
        return '#'


sa.event.listen(Hd1Sl1Slide, 'before_insert', Hd1Sl1Slide.before_insert_listener)


@register_as_block
class Hd1Sl1(db.Model, AbstractBlock):
    __description__ = _('Hd1Sl1 block description')

    header = db.Column(db.Text, default='', server_default='')

    @hybrid_property
    def available_slides(self):
        return self.slides.filter(Hd1Sl1Slide.publish_ready.is_(True)).order_by(Hd1Sl1Slide.position.asc())

    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready:
            return

        slides = self.available_slides.all()
        kwargs.setdefault('block_data', self._get_columns_data())
        kwargs.setdefault('available_slides', slides)

        try:
            return render_template(self._default_template,
                                   editable=editable,
                                   global_block_id=self.block.global_id if self.block else None,
                                   **kwargs)
        except TemplateNotFound as e:
            current_app.logger.exception(e)

        return 'TODO: Slides block template'

    def custom_manage_view(self, page, page_id, pageblock, pageblock_id, active_subtab='general', active_tab=None, *args, **kwargs):
        subtabs = [{'alias': 'general', 'title': _('Hd1Sl1 general tab')},
                   {'alias': 'slides', 'title': _('Hd1Sl1 slides tab')}]
        allowed_subtabs = [st['alias'] for st in subtabs]
        active_subtab = active_subtab.lower()
        if active_subtab not in allowed_subtabs:
            abort(404)
        pb_data = pageblock.get_data_object()
        form = None
        block_slides = []

        if active_subtab == 'general':
            from admin.block_forms import CustomHd1Sl1Form
            form = CustomHd1Sl1Form(obj=pb_data)
        elif active_subtab == 'slides':
            block_slides = pb_data.slides

        if form and form.validate_on_submit():
            try:
                form.populate_obj(pb_data)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.exception(e)
                flash(_("Can't save pageblock data message"), 'danger')
            else:
                flash(_('Pageblock data saved successfully message'), 'success')

            if 'submit_and_continue' in request.form:
                return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id))
            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))
        if not form and request.method == 'POST':
            if active_subtab == 'slides':
                positions = request.form.getlist('positions', type=int)
                to_remove = request.form.getlist('toremove', type=int)
                trash_selected = 'trash_selected' in request.form
                if positions and not trash_selected:
                    try:
                        for s in block_slides:
                            if s.id in positions:
                                s.position = positions.index(s.id) + 1
                        db.session.commit()
                    except Exception as e:
                        db.session.rollback()
                        current_app.logger.exception(e)
                    else:
                        flash(_('Slides order saved successfully message'), 'success')
                if to_remove and trash_selected:
                    try:
                        slides = {s.id: s for s in block_slides}
                        for tr in to_remove:
                            obj = slides.get(tr)
                            if obj:
                                db.session.delete(obj)
                        db.session.commit()
                    except Exception as e:
                        db.session.rollback()
                        current_app.logger.exception(e)
                    else:
                        flash(_('Selected slides deleted successfully message'), 'success')
            return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock_id, active_subtab=active_subtab))

        return render_template(self._admin_manage_template,
                               page=page,
                               page_id=page_id,
                               pageblock=pageblock,
                               pageblock_id=pageblock_id,
                               form=form,
                               subtabs=subtabs,
                               active_tab=active_tab,
                               active_subtab=active_subtab,
                               current_block=self,
                               block_slides=block_slides,
                               **kwargs)


class Im1SpmsPrograms(db.Model):
    __tablename__ = 'im1spms_programs'

    id = db.Column(db.Integer, primary_key=True)
    icon = db.Column(sa.dialects.postgresql.ARRAY(db.Text), default=[], server_default='{}')
    program_id = db.Column(db.Integer, db.ForeignKey('sm_carwash_programs.id'))
    block_id = db.Column(db.Integer, db.ForeignKey('im1spms.id'), nullable=False)
    position = db.Column(db.Integer)

    program = db.relationship('CarwashProgram', lazy='joined')
    block = db.relationship('Im1SPms', uselist=False, backref=db.backref('programs', lazy='dynamic', uselist=True))

    __table_args__ = (db.UniqueConstraint('block_id', 'position'), )
    # cascade='save-update, merge, delete'


@register_as_block
class Im1SPms(db.Model, AbstractBlock):
    __description__ = _('Im1SPms block description')

    def __init__(self, *args, **kwargs):
        super(Im1SPms, self).__init__(*args, **kwargs)
        # 11 программ доступно на крутилке (надо создать их все сразу)
        for pos in range(-5, 6):
            self.programs.append(Im1SpmsPrograms(position=pos))

    @property
    def available_programs(self):
        return self.programs.filter(Im1SpmsPrograms.program.has()).order_by(Im1SpmsPrograms.position.asc())

    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready:
            return

        programs = self.available_programs.all()
        kwargs.setdefault('block_data', self._get_columns_data())
        kwargs.setdefault('block_programs', programs)

        try:
            return render_template(self._default_template,
                                   editable=editable,
                                   global_block_id=self.block.global_id if self.block else None,
                                   **kwargs)
        except TemplateNotFound as e:
            current_app.logger.exception(e)

        return 'TODO: Programs block template'


@register_as_block
class Ns(db.Model, AbstractBlock):
    __description__ = _('Ns block description')

    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready:
            return

        from .site_models import NewsArticle

        news = NewsArticle.available().all()
        kwargs.setdefault('block_data', self._get_columns_data())
        kwargs.setdefault('block_news', news)

        try:
            return render_template(self._default_template,
                                   editable=editable,
                                   global_block_id=self.block.global_id if self.block else None,
                                   **kwargs)
        except TemplateNotFound as e:
            current_app.logger.exception(e)

        return 'TODO: News block template'


@register_as_block
class Hd1Mp1(db.Model, AbstractBlock):
    """
    Блок карт
    """
    typeCarwashes = 'carwashes'
    typeServices = 'services'
    typeAffiliates = 'affiliates'
    typeServicesAndAffiliates = 'servicesandaffiliates'

    __description__ = _('Hd1Mp1 block description')

    header = db.Column(db.Text)
    type_ = db.Column(db.Enum(typeCarwashes, typeServices, typeAffiliates, typeServicesAndAffiliates, name='hd1mp1_map_types'), db.DefaultClause(typeCarwashes))

    @classmethod
    def types_listing(cls):
        return [(cls.typeCarwashes, __('Map carwashes option')),
                (cls.typeServices, __('Map services option')),
                (cls.typeAffiliates, __('Map affiliates option')),
                (cls.typeServicesAndAffiliates, __('Map services and affiliates option'))]

    @classmethod
    def object_switch_url(cls, type_):
        return url_for('ajax_map_object_selected', type_=type_)

    def available_objects(self, filters=None):
        objects = []
        if self.type_ == self.typeCarwashes:
            from .site_models import Carwash
            objects = Carwash.available(filters=filters).all()
        if self.type_ in [self.typeServices, self.typeAffiliates, self.typeServicesAndAffiliates]:
            from .site_models import MapObject
            type_ = None
            if self.type_ == self.typeServices:
                type_ = MapObject.typeService
            elif self.type_ == self.typeAffiliates:
                type_ = MapObject.typeAffiliate
            objects = MapObject.available(filters=filters, type_=type_).all()
        return objects

    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready:
            return
        filters = kwargs.get('filters')  # TODO: Как передавать фильтры?
        map_objects = self.available_objects(filters)
        kwargs.setdefault('block_data_get_service', self.object_switch_url(self.type_))
        kwargs.setdefault('block_data', self._get_columns_data())
        kwargs.setdefault('block_map_items', map_objects)

        try:
            return render_template(self._default_template,
                                   editable=editable,
                                   global_block_id=self.block.global_id if self.block else None,
                                   **kwargs)
        except TemplateNotFound as e:
            current_app.logger.exception(e)

        return 'TODO: Map block template'

    @classproperty
    def not_use_common_render(self):
        return True


@register_as_block
class Hd1Mp2(db.Model, AbstractBlock):
    """
    Блок карт
    """
    typeCarwashes = 'carwashes'
    typeServices = 'services'
    typeAffiliates = 'affiliates'
    typeServicesAndAffiliates = 'servicesandaffiliates'

    __description__ = _('Hd1Mp2 block description')

    header = db.Column(db.Text)
    type_ = db.Column(db.Enum(typeCarwashes, typeServices, typeAffiliates, typeServicesAndAffiliates, name='hd1mp1_map_types'), db.DefaultClause(typeCarwashes))

    @classmethod
    def types_listing(cls):
        return [(cls.typeCarwashes, __('Map carwashes option')),
                (cls.typeServices, __('Map services option')),
                (cls.typeAffiliates, __('Map affiliates option')),
                (cls.typeServicesAndAffiliates, __('Map services and affiliates option'))]

    @classmethod
    def object_switch_url(cls, type_):
        return url_for('ajax_map_object_selected', type_=type_)

    def available_objects(self, filters=None):
        objects = []
        if self.type_ == self.typeCarwashes:
            from .site_models import Carwash
            objects = Carwash.available(filters=filters).all()
        if self.type_ in [self.typeServices, self.typeAffiliates, self.typeServicesAndAffiliates]:
            from .site_models import MapObject
            type_ = None
            if self.type_ == self.typeServices:
                type_ = MapObject.typeService
            elif self.type_ == self.typeAffiliates:
                type_ = MapObject.typeAffiliate
            objects = MapObject.available(filters=filters, type_=type_).all()
        return objects

    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready:
            return
        filters = kwargs.get('filters')  # TODO: Как передавать фильтры?
        map_objects = self.available_objects(filters)
        kwargs.setdefault('block_data_get_service', self.object_switch_url(self.type_))
        kwargs.setdefault('block_data', self._get_columns_data())
        kwargs.setdefault('block_map_items', map_objects)

        try:
            return render_template(self._default_template,
                                   editable=editable,
                                   global_block_id=self.block.global_id if self.block else None,
                                   **kwargs)
        except TemplateNotFound as e:
            current_app.logger.exception(e)

        return 'TODO: Map block template'

    @classproperty
    def not_use_common_render(self):
        return False


@register_as_block
class CleanserCatalog(db.Model, AbstractBlock):
    __description__ = _('CleanserCatalog block description')

    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready:
            return
        from .site_models import Cleanser
        cleansers = Cleanser.available().all()
        kwargs.setdefault('block_data', self._get_columns_data())
        kwargs.setdefault('block_cleanser', cleansers)
        try:
            return render_template(self._default_template,
                                   editable=editable,
                                   global_block_id=self.block.global_id if self.block else None,
                                   **kwargs)
        except TemplateNotFound as e:
            current_app.logger.exception(e)

        return 'TODO: CleanserCatalog block template'

#
# Структура сайта
#
# class NavTypeConverter(BaseConverter):
class NavTypeConverter(BaseConverter):
    """
    Конвертер для урлов управления навигацией
    """
    def __init__(self, map):
        BaseConverter.__init__(self, map)
        self.regex = '(?:%s)' % '|'.join([re.escape(x) for x in NavigationGroup.nav_types_list()])


class NavHelper(object):
    behaviorPopup = 'Popup'
    behaviorLink = 'Link'
    behaviorPage = 'Page'
    behaviorOnlyHeader = 'OnlyHeader'


class NavigationGroup(db.Model):
    navtypeHeader = 'header'
    navtypeFooter = 'footer'
    navtypeService = 'service'

    __tablename__ = 'navigation_groups'

    id = db.Column(db.Integer, primary_key=True)
    caption = db.Column(db.Text)
    slug = db.Column(db.Text)
    published = db.Column(db.Boolean, default=True, server_default='true')
    position = db.Column(db.Integer, default=0, server_default='0')
    behavior_ = db.Column(db.Enum(NavHelper.behaviorLink, NavHelper.behaviorPage, NavHelper.behaviorPopup, NavHelper.behaviorOnlyHeader, name='navigation_behaviors'),
                          db.DefaultClause(NavHelper.behaviorLink))
    navtype_ = db.Column(db.Enum(navtypeHeader, navtypeFooter, navtypeService, name='navigation_types'), db.DefaultClause(navtypeHeader))
    language = db.Column(db.String(2), nullable=False)

    # behavior specific data
    # for Link
    link_url = db.Column(db.Text)
    link_blank = db.Column(db.Boolean, default=False, server_default='false')
    # for Page
    page_id = db.Column(db.Integer, db.ForeignKey('pages.id'), nullable=True)
    page = db.relationship('Page', lazy='joined')  # FIXME: Some settings needed?
    page_slug = association_proxy('page', 'slug')  # Associated page slug

    __mapper_args__ = {'order_by': position.asc()}
    __table_args__ = (
        # db.UniqueConstraint('slug', 'language'),  # Пока убрал, чтобы была возможность продублировать slug первого уровня в верхнем и нижнем меню
        # TODO: Constraints
        # Header -> Popup, Link, Page; Footer -> Link, Page, OnlyHeader
    )

    @property
    def group_url(self):
        if self.behavior_ == NavHelper.behaviorOnlyHeader:
            return '#'
        elif self.behavior_ == NavHelper.behaviorLink:
            return self.link_url
        elif self.behavior_ == NavHelper.behaviorPage:
            if self.page and self.page.main:
                return url_for('index')

            return url_for('nav_page_view', page_path=self.path)
        elif self.behavior_ == NavHelper.behaviorPopup:
            return 'popup'

    @property
    def behavior(self):
        return self.behavior_

    @behavior.setter
    def behavior(self, value):
        self.behavior_ = value

    @hybrid_property
    def path(self):
        if self.behavior_ == NavHelper.behaviorPage:
            if self.page:
                if self.page.main is True:
                    return None

                return self.page_slug

        return None

    @path.expression
    def path(cls):
        # Для страницы, если она не главная
        return cls.page_slug

    @classmethod
    def available(cls, navtype=None):
        query = cls.query
        query = query.filter(cls.published.is_(True))

        if navtype:
            query = query.filter(cls.navtype_.__eq__(navtype))

        query = query.order_by(cls.position.asc())
        return query

    @hybrid_property
    def available_items(self):
        return self.items.filter(NavigationGroupItem.publish_ready.is_(True)).order_by(NavigationGroupItem.position.asc())

    @hybrid_property
    def available_popup_items(self):
        return self.popup_items.filter(NavigationGroupPopupItem.publish_ready.is_(True)).order_by(NavigationGroupPopupItem.position.asc())

    @hybrid_property
    def publish_ready(self):
        return self.published

    @classmethod
    def admin_list(cls, navtype, with_count=False):
        query = cls.query
        query = query.filter(cls.navtype_.__eq__(navtype))
        count = query.count() if with_count else 0

        if with_count:
            return query, count

        return query

    @classmethod
    def nav_types_listing(cls):
        return [(cls.navtypeHeader, __('Navigation header type option')),
                (cls.navtypeFooter, __('Navigation footer type option')),
                (cls.navtypeService, __('Navigation service type option'))]

    @property
    def nav_type_title(self):
        for type_ in self.__class__.nav_types_listing():
            if type_[0] == self.navtype_:
                return type_[1]

    @classmethod
    def nav_types_list(cls):
        navtype_list = [e[0] for e in cls.nav_types_listing()]
        return [t.lower() for t in navtype_list]

    @classmethod
    def behavior_listing(cls, navtype_=None):
        listing = [(NavHelper.behaviorLink, _('Navigation behavior link option')),
                   (NavHelper.behaviorPage, _('Navigation behavior page option'))]

        if navtype_ == cls.navtypeHeader:
            listing.append((NavHelper.behaviorPopup, _('Navigation behavior popup option')))
        elif navtype_ == cls.navtypeFooter:
            listing.append((NavHelper.behaviorOnlyHeader, _('Navigation behavior only header option')))

        return listing

    @hybrid_property
    def has_items(self):
        return self.behavior_ == NavHelper.behaviorOnlyHeader

    @has_items.expression
    def has_items(cls):
        return db.and_(cls.behavior_.__eq__(NavHelper.behaviorOnlyHeader))

    @hybrid_property
    def has_popup_items(self):
        return self.behavior_ == NavHelper.behaviorPopup

    @has_popup_items.expression
    def has_popup_items(cls):
        return db.and_(cls.behavior_.__eq__(NavHelper.behaviorPopup))

    @classmethod
    def before_insert_listener(cls, mapper, connection, target):  # TODO: Make a mixin or another def ?
        count = db.session.query(sa.func.count(cls.id))
        count = count.filter(cls.navtype_.__eq__(target.navtype_))
        count = count.first()
        count = count[0] if count else 0
        target.position = count + 1


sa.event.listen(NavigationGroup, 'before_insert', NavigationGroup.before_insert_listener)


class NavigationGroupPopupItem(db.Model):
    itemSquare = 'square'
    itemCircle = 'circle'
    itemSide = 'side'

    __tablename__ = 'navigation_group_popup_items'

    id = db.Column(db.Integer, primary_key=True)
    group_id = db.Column(db.Integer, db.ForeignKey('navigation_groups.id'), nullable=False)
    caption = db.Column(db.Text)
    itemtype_ = db.Column(db.Enum(itemSquare, itemCircle, itemSide, name='popup_items_types'), db.DefaultClause(itemSquare))
    slug = db.Column(db.Text)
    behavior_ = db.Column(db.Enum(NavHelper.behaviorLink, NavHelper.behaviorPage, name='popup_items_navigation_behaviors'), db.DefaultClause(NavHelper.behaviorPage))

    # itemtype dependent columns
    description = db.Column(db.Text)
    circle_icon = db.Column(sa.dialects.postgresql.ARRAY(db.Text), server_default='{}')
    square_icon = db.Column(sa.dialects.postgresql.ARRAY(db.Text), server_default='{}')

    published = db.Column(db.Boolean, default=True, server_default='true')
    position = db.Column(db.Integer, default=0, server_default='0')

    # for Link
    link_url = db.Column(db.Text)
    link_blank = db.Column(db.Boolean, default=False, server_default='false')

    # for Page
    page_id = db.Column(db.Integer, db.ForeignKey('pages.id'), nullable=True)
    page = db.relationship('Page', lazy='joined', backref=db.backref('navigation_popup', uselist=False))  # FIXME: Some settings needed?

    group = db.relationship(NavigationGroup, backref=db.backref('popup_items', lazy='dynamic', uselist=True, cascade='all, delete-orphan'))

    __mapper_args__ = {'order_by': position.asc()}
    __table_args__ = (
        db.UniqueConstraint('slug', 'group_id'),
    )

    def _original_icon(self, type_):
        """
        Возвращает оригинальное название файла изображения, если его нет - название файла на диске, иначе None
        type_ = circle,square
        """
        img = None
        if type_ == 'circle':
            img = self.circle_icon
        elif type_ == 'square':
            img = self.square_icon

        if img:
            if len(img) > 1:
                return img[1]

            return self._stored_icon(self, type_)

        return None

    def _stored_icon(self, type_):
        img = None
        if type_ == 'circle':
            img = self.circle_icon
        elif type_ == 'square':
            img = self.square_icon

        if img:
            return img[0]

        return None

    def set_icon(self, type_, data):
        if type_ == 'circle_icon':
            self.circle_icon = data
        elif type_ == 'square_icon':
            self.square_icon = data

    @property
    def original_cicon(self):
        return self._original_icon('circle')

    @property
    def stored_cicon(self):
        return self._stored_icon('circle')

    @property
    def original_sicon(self):
        return self._original_icon('square')

    @property
    def stored_sicon(self):
        return self._stored_icon('square')

    @classmethod
    def available(cls):
        query = cls.query
        query = query.filter(cls.published.is_(True))
        query = query.order_by(cls.position.asc())
        return query

    @property
    def behavior(self):
        return self.behavior_

    @behavior.setter
    def behavior(self, value):
        self.behavior_ = value

    @hybrid_property
    def path(self):
        if self.behavior_ == NavHelper.behaviorPage:
            path_elems = [self.group_slug]

            if self.page:
                if self.page.main is True:
                    return None

                path_elems.append(self.slug)

            return os.path.join(*path_elems)

        return None

    @path.expression
    def path(cls):
        # Для страницы, если она не главная
        return db.case([
            (db.and_(cls.behavior_.__eq__(NavHelper.behaviorPage), cls.page.has(Page._main.is_(False))), db.func.concat(cls.group_slug, '/', cls.slug)),
        ])

    @hybrid_property
    def group_slug(self):
        if self.group:
            return self.group.slug

        return None

    @group_slug.expression
    def group_slug(cls):
        return NavigationGroup.slug

    @property
    def item_url(self):
        if self.behavior_ == NavHelper.behaviorLink:
            return self.link_url
        elif self.behavior_ == NavHelper.behaviorPage:
            if self.page and self.page.main:
                return url_for('index')

            return url_for('nav_page_view', page_path=self.path)

    @classmethod
    def behavior_listing(cls):
        listing = [(NavHelper.behaviorLink, _('Navigation behavior link option')),
                   (NavHelper.behaviorPage, _('Navigation behavior page option'))]

        return listing

    @property
    def item_url(self):
        if self.behavior_ == NavHelper.behaviorLink:
            return self.link_url
        elif self.behavior_ == NavHelper.behaviorPage:
            if self.page and self.page.main:
                return url_for('index')

            return url_for('nav_page_view', page_path=self.path)

    @hybrid_property
    def publish_ready(self):
        return self.published

    @classmethod
    def before_insert_listener(cls, mapper, connection, target):  # TODO: Make a mixin or another def ?
        count = db.session.query(sa.func.count(cls.id))
        count = count.filter(cls.group_id.__eq__(target.group_id),
                             cls.itemtype_.__eq__(target.itemtype_))
        count = count.first()
        count = count[0] if count else 0
        target.position = count + 1

    @classmethod
    def receive_after_delete(cls, mapper, connection, target):
        files_to_delete = []

        if target.stored_sicon:
            files_to_delete.append(target.stored_sicon)
        if target.stored_cicon:
            files_to_delete.append(target.stored_cicon)

        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'nav_icons')

        for filename in files_to_delete:
            FileHelper.delete_file(os.path.join(folder, filename))


sa.event.listen(NavigationGroupPopupItem, 'before_insert', NavigationGroupPopupItem.before_insert_listener)
sa.event.listen(NavigationGroupPopupItem, 'after_delete', NavigationGroupPopupItem.receive_after_delete)


class NavigationGroupItem(db.Model):
    __tablename__ = 'navigation_group_items'

    id = db.Column(db.Integer, primary_key=True)
    group_id = db.Column(db.Integer, db.ForeignKey('navigation_groups.id'), nullable=False)
    caption = db.Column(db.Text)
    slug = db.Column(db.Text)
    behavior_ = db.Column(db.Enum(NavHelper.behaviorLink, NavHelper.behaviorPage, name='items_navigation_behaviors'), db.DefaultClause(NavHelper.behaviorPage))
    published = db.Column(db.Boolean, default=True, server_default='true')
    position = db.Column(db.Integer, default=0, server_default='0')

    # Item behaviors specific fields
    # for Link
    link_url = db.Column(db.Text)
    link_blank = db.Column(db.Boolean, default=False, server_default='false')
    # for Page
    page_id = db.Column(db.Integer, db.ForeignKey('pages.id'), nullable=True)
    page = db.relationship('Page', lazy='joined')  # FIXME: Some settings needed

    group = db.relationship(NavigationGroup, backref=db.backref('items', lazy='dynamic', uselist=True, cascade='all, delete-orphan'))

    __mapper_args__ = {'order_by': position.asc()}
    __table_args__ = (
        db.UniqueConstraint('slug', 'group_id'),
    )

    @classmethod
    def available(cls):
        query = cls.query
        query = query.filter(cls.published.is_(True))
        query = query.order_by(cls.position.asc())
        return query

    @property
    def item_url(self):
        if self.behavior_ == NavHelper.behaviorLink:
            return self.link_url
        elif self.behavior_ == NavHelper.behaviorPage:
            if self.page and self.page.main:
                return url_for('index')

            return url_for('nav_page_view', page_path=self.path)

    @property
    def behavior(self):
        return self.behavior_

    @behavior.setter
    def behavior(self, value):
        self.behavior_ = value

    @hybrid_property
    def path(self):
        if self.behavior_ == NavHelper.behaviorPage:
            path_elems = [self.group_slug]

            if self.page:
                if self.page.main is True:
                    return None

                path_elems.append(self.slug)

            return os.path.join(*path_elems)

        return None

    @hybrid_property
    def group_slug(self):
        if self.group:
            return self.group.slug

        return None

    @group_slug.expression
    def group_slug(cls):
        return NavigationGroup.slug

    @path.expression
    def path(cls):
        # Для страницы, если она не главная
        return db.case([
            (db.and_(cls.behavior_.__eq__(NavHelper.behaviorPage), cls.page.has(Page._main.is_(False))), db.func.concat(cls.group_slug, '/', cls.slug)),
        ])

    @hybrid_property
    def publish_ready(self):
        return self.published

    @classmethod
    def behavior_listing(cls):
        listing = [(NavHelper.behaviorLink, _('Navigation behavior link option')),
                   (NavHelper.behaviorPage, _('Navigation behavior page option'))]

        return listing

    @classmethod
    def before_insert_listener(cls, mapper, connection, target):  # TODO: Make a mixin or another def ?
        count = db.session.query(sa.func.count(cls.id))
        count = count.filter(cls.group_id.__eq__(target.group_id))
        count = count.first()
        count = count[0] if count else 1
        target.position = count + 1


sa.event.listen(NavigationGroupItem, 'before_insert', NavigationGroupItem.before_insert_listener)
