--
-- Color dictionary
--

-- Background colors
INSERT INTO color_dictionary (id, color, type_, position) VALUES (1, '#ffee3b', 'Background', 1);
INSERT INTO color_dictionary (id, color, type_, position) VALUES (2, '#ffffff', 'Background', 2);
INSERT INTO color_dictionary (id, color, type_, position) VALUES (3, '#eeeeee', 'Background', 3);

-- Font colors
INSERT INTO color_dictionary (id, color, type_, position) VALUES (4, '#000000', 'Font', 1);
INSERT INTO color_dictionary (id, color, type_, position) VALUES (5, '#5e5e5e', 'Font', 2);
INSERT INTO color_dictionary (id, color, type_, position) VALUES (6, '#474747', 'Font', 3);
INSERT INTO color_dictionary (id, color, type_, position) VALUES (7, '#ffee3b', 'Font', 4);
INSERT INTO color_dictionary (id, color, type_, position) VALUES (8, '#ffffff', 'Font', 5);

SELECT setval('color_dictionary_id_seq', (SELECT MAX(id) FROM color_dictionary));


INSERT INTO faq_categories (id, common_, title, published, position) VALUES (
    1, true, 'Общие вопросы', true, 1
);
SELECT setval('faq_categories_id_seq', (SELECT MAX(id) + 1 FROM faq_categories));
