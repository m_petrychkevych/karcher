# coding: utf-8
import os
import sqlalchemy as sa

from core.utils.files import FileHelper
from core.utils.youtube import YoutubeHelper
from flask import current_app, url_for
from flask.ext.babel import lazy_gettext as __
from sqlalchemy.ext.hybrid import hybrid_property

from .init import db


class FAQCategory(db.Model):
    """
    Категории ответов вопросов
    """
    __tablename__ = 'faq_categories'

    id = db.Column(db.Integer, primary_key=True)
    common_ = db.Column(db.Boolean, default=False, server_default='false')
    title = db.Column(db.Text)
    published = db.Column(db.Boolean, default=True, server_default='true')
    position = db.Column(db.Integer, default=0, server_default='0')

    @classmethod
    def available(cls):
        query = cls.query
        query = query.filter(cls.publish_ready.is_(True))
        return query

    @hybrid_property
    def publish_ready(self):
        qestions = self.questions
        questions_ready = False
        for q in questions:
            if q.publish_ready:
                questions_ready = True
                break
        return self.published and questions_ready

    @publish_ready.expression
    def publish_ready(cls):
        return db.and_(cls.published.is_(True),
                       cls.questions.any(FAQ.publish_ready.is_(True)))

    @classmethod
    def admin_list(cls, with_count=False):
        query = cls.query
        count = query.count() if with_count else 0

        query = query.order_by(cls.position.asc())

        if with_count:
            return query, count

        return query

    @classmethod
    def before_insert_listener(cls, mapper, connection, target):  # TODO: Make a mixin or another def ?
        count = db.session.query(sa.func.count(cls.id))
        count = count.first()
        count = count[0] if count else 1
        target.position = count + 1


sa.event.listen(FAQCategory, 'before_insert', FAQCategory.before_insert_listener)


class FAQ(db.Model):
    """
    Вопрос-ответ
    """
    __tablename__ = 'faq'

    id = db.Column(db.Integer, primary_key=True)
    category_id = db.Column(db.Integer, db.ForeignKey('faq_categories.id'), nullable=False)
    question = db.Column(db.Text)
    answer = db.Column(db.Text)
    published = db.Column(db.Boolean, default=True, server_default='true')
    position = db.Column(db.Integer, default=0, server_default='0')

    category = db.relationship(FAQCategory, backref=db.backref('questions', uselist=True, order_by=position.asc(), cascade='all, delete-orphan'))

    @classmethod
    def before_insert_listener(cls, mapper, connection, target):  # TODO: Make a mixin or another def ?
        count = db.session.query(sa.func.count(cls.id))
        count = count.filter(cls.category_id.__eq__(target.category_id))
        count = count.first()
        count = count[0] if count else 1
        target.position = count + 1

    @hybrid_property
    def publish_ready(self):
        return self.published is True

    @publish_ready.expression
    def publish_ready(cls):
        return cls.published.is_(True)


sa.event.listen(FAQ, 'before_insert', FAQ.before_insert_listener)


carwash_program_association = db.Table('sm_carwash_program_association',
                                       db.Column('carwash_id', db.Integer, db.ForeignKey('sm_carwashes.id')),
                                       db.Column('program_id', db.Integer, db.ForeignKey('sm_carwash_programs.id')))


class MapObject(db.Model):
    """
    Объект на карте (Сервис-центр, Филиал)
    """
    typeService = 'service'
    typeAffiliate = 'affiliate'

    __tablename__ = 'sm_map_objects'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    type_ = db.Column(db.Enum(typeService, typeAffiliate, name='mapobject_types'), db.DefaultClause(typeService))
    photo = db.Column(sa.dialects.postgresql.ARRAY(db.Text), default=[], server_default='{}')
    description = db.Column(db.Text)

    address = db.Column(db.Text)
    longitude = db.Column(db.Float(precision=8))
    latitude = db.Column(db.Float(precision=8))
    zoom = db.Column(db.Integer, default=10, server_default='10')

    site_link = db.Column(db.Text)
    email = db.Column(db.Text)
    phone = db.Column(db.Text)

    @property
    def original_photo(self):
        if self.photo:
            if len(self.photo) > 1:
                return self.photo[1]
            return self.stored_photo
        return None

    @property
    def stored_photo(self):
        if self.photo:
            return self.photo[0]
        return None

    def render_photo(self):
        return url_for('public_dir', filename=os.path.join('images', 'map_objects', self.stored_photo))

    @classmethod
    def available(cls, filters=None, type_=None):
        query = cls.query
        # query = query.filter(cls.published.is_(True))

        if type_:
            query = query.filter(cls.type_.__eq__(type_))

        return query

    @classmethod
    def admin_list(cls, with_count=False):
        query = cls.query
        count = query.count() if with_count else 0

        if with_count:
            return query, count

        return query

    @classmethod
    def types_listing(cls):
        return [(cls.typeService, __('Map object service option')),
                (cls.typeAffiliate, __('Map object affiliate option'))]


class Carwash(db.Model):
    """
    Основная карточка мойки
    """
    __tablename__ = 'sm_carwashes'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    network_id = db.Column(db.Integer, db.ForeignKey('sm_carwash_networks.id'))

    published = db.Column(db.Boolean, default=True, server_default='true')

    posts_count = db.Column(db.Integer)  # Количество постов
    enhanced_post = db.Column(db.Boolean, default=False, server_default='false')  # Наличие усиленного поста
    high_body_vihicle = db.Column(db.Boolean, default=False, server_default='false')  # Заезд автомобилей с высоким кузовом

    address = db.Column(db.Text)
    longitude = db.Column(db.Float(precision=8))
    latitude = db.Column(db.Float(precision=8))
    zoom = db.Column(db.Integer, default=10, server_default='10')

    action = db.Column(db.Text)
    city_id = db.Column(db.Integer, db.ForeignKey('sm_cities.id'))

    programs = db.relationship('CarwashProgram', secondary=carwash_program_association)
    network = db.relationship('CarwashNetwork')
    city = db.relationship('City')

    @classmethod
    def available(cls, filters=None):
        query = cls.query
        query = query.filter(cls.published.is_(True))

        return query

    @classmethod
    def admin_list(cls, with_count=False):
        query = cls.query
        count = query.count() if with_count else 0

        if with_count:
            return query, count

        return query


class CarwashNetwork(db.Model):
    __tablename__ = 'sm_carwash_networks'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)

    @classmethod
    def admin_list(cls, with_count=False):
        query = cls.query
        count = query.count() if with_count else 0

        if with_count:
            return query, count

        return query


class CarwashProgram(db.Model):
    """
    Программа мойки
    """
    __tablename__ = 'sm_carwash_programs'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    short_description = db.Column(db.Text)
    description = db.Column(db.Text)
    small_icon = db.Column(sa.dialects.postgresql.ARRAY(db.Text), server_default='{}', default=[])
    icon = db.Column(sa.dialects.postgresql.ARRAY(db.Text), server_default='{}', default=[])

    @classmethod
    def admin_list(cls, with_count=False):
        query = cls.query
        count = query.count() if with_count else 0

        if with_count:
            return query, count

        return query

    @property
    def original_small_icon(self):
        if self.small_icon:
            if len(self.small_icon) > 1:
                return self.small_icon[1]
            return self.stored_small_icon
        return None

    @property
    def stored_small_icon(self):
        if self.small_icon:
            return self.small_icon[0]
        return None

    @property
    def original_icon(self):
        if self.icon:
            if len(self.icon) > 1:
                return self.icon[1]
            return self.stored_icon
        return None

    @property
    def stored_icon(self):
        if self.icon:
            return self.icon[0]
        return None

    def render_icon(self, small=False):
        return url_for('public_dir', filename=os.path.join('images', 'carwashes', self.stored_icon if not small else self.stored_small_icon))

    @classmethod
    def receive_after_delete(cls, mapper, connection, target):
        files_to_delete = []

        if target.stored_icon:
            files_to_delete.append(target.stored_icon)
        if target.stored_small_icon:
            files_to_delete.append(target.stored_small_icon)

        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'news')

        for filename in files_to_delete:
            FileHelper.delete_file(os.path.join(folder, filename))


sa.event.listen(CarwashProgram, 'after_delete', CarwashProgram.receive_after_delete)


class CarwashImage(db.Model):
    __tablename__ = 'sm_carwash_images'

    id = db.Column(db.Integer, primary_key=True)
    carwash_id = db.Column(db.Integer, db.ForeignKey('sm_carwashes.id'), nullable=False)

    image = db.Column(sa.dialects.postgresql.ARRAY(db.Text), server_default='{}', default=[])
    youtubeid = db.Column(db.String(11))

    published = db.Column(db.Boolean, default=True, server_default='true')
    position = db.Column(db.Integer, default=0, server_default='0')

    carwash = db.relationship(Carwash, backref=db.backref('gallery', cascade='all, delete-orphan'))


class NewsArticle(db.Model):
    __tablename__ = 'news'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    content = db.Column(db.Text)

    announcement = db.Column(db.Text)
    announcement_image = db.Column(sa.dialects.postgresql.ARRAY(db.Text), server_default='{}', default=[])

    published = db.Column(db.Boolean, default=True, server_default='true')
    publish_date = db.Column(db.Date)

    __mapper_args__ = {'order_by': publish_date.desc()}

    @classmethod
    def available(cls):
        query = cls.query
        query = query.filter(cls.published.is_(True))

        return query

    @classmethod
    def admin_list(cls, with_count=False):
        query = cls.query
        count = query.count() if with_count else 0

        if with_count:
            return query, count

        return query

    @property
    def original_image(self):
        if self.announcement_image:
            if len(self.announcement_image) > 1:
                return self.announcement_image[1]
            return self.stored_icon
        return None

    @property
    def stored_image(self):
        if self.announcement_image:
            return self.announcement_image[0]
        return None

    def render_image(self):
        return url_for('public_dir', filename=os.path.join('images', 'news', self.stored_image))


class MediaCollection(db.Model):
    __tablename__ = 'media_collection'

    id = db.Column(db.Integer, primary_key=True)
    youtubeid = db.Column(db.String)
    image = db.Column(sa.dialects.postgresql.ARRAY(db.String), default=[], server_default='{}')
    tags = db.Column(sa.dialects.postgresql.ARRAY(db.String), default=[], server_default='{}')
    published = db.Column(db.Boolean, default=True, server_default='true')
    created_at = db.Column(db.DateTime, db.DefaultClause(db.func.now()))

    __mapper_args__ = {
        'order_by': created_at.desc()
    }

    @classmethod
    def admin_list(cls, filters=None, with_count=False):
        query = cls.query
        count = query.count if with_count else 0

        if with_count:
            return query, count

        return query

    @classmethod
    def available(cls, filters=None):
        query = cls.query
        query = query.filter(cls.published.is_(True))
        query = query.order_by(cls.created_at.desc())
        return query

    @property
    def original_image(self):
        if self.image:
            if len(self.image) > 1:
                return self.image[1]
            return self.stored_image
        return None

    @property
    def stored_image(self):
        if self.image:
            return self.image[0]
        return None

    def render_image(self):
        if self.is_video:
            return YoutubeHelper.link(self.youtubeid)

        if self.stored_image:
            return url_for('public_dir', filename=os.path.join('images', 'media', self.stored_image))

    @hybrid_property
    def is_video(self):
        if self.youtubeid:
            return True
        return False

    @classmethod
    def tags_collection(cls):
        sq = db.func.distinct(db.func.unnest(cls.tags)).label('tag')
        q = db.select([sq]).order_by(sq.asc())
        return db.session.execute(q).fetchall()

    @classmethod
    def tags_listing(cls):
        tags = cls.tags_collection()
        return [(t[0], t[0]) for t in tags]

    @classmethod
    def receive_after_delete(cls, mapper, connection, target):
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'nav_icons')
        if target.stored_image:
            FileHelper.delete_file(os.path.join(folder, target.stored_image))


sa.event.listen(MediaCollection, 'after_delete', MediaCollection.receive_after_delete)


class Cleanser(db.Model):
    __tablename__ = 'cleansers'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    sku = db.Column(db.Text)
    photo = db.Column(sa.dialects.postgresql.ARRAY(db.String), default=[], server_default='{}')
    list_description = db.Column(db.Text)
    overlay_description = db.Column(db.Text)
    published = db.Column(db.Boolean, default=True, server_default='true')

    @classmethod
    def available(cls):
        query = cls.query
        query = query.filter(cls.published.is_(True))
        return query

    @classmethod
    def admin_list(cls, with_count=False):
        query = cls.query
        count = query.count() if with_count else 0

        if with_count:
            return query, count

        return query

    @property
    def original_photo(self):
        if self.photo:
            if len(self.photo) > 1:
                return self.photo[1]
            return self.stored_photo
        return None

    @property
    def stored_photo(self):
        if self.photo:
            return self.photo[0]
        return None

    def render_image(self):
        if self.stored_photo:
            return url_for('public_dir', filename=os.path.join('images', 'cleansers', self.stored_photo))

    @classmethod
    def recieve_after_delete(cls, mapper, connection, target):
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'cleansers')
        if target.stored_photo:
            FileHelper.delete_file(os.path.join(folder, target.stored_photo))


sa.event.listen(Cleanser, 'after_delete', Cleanser.recieve_after_delete)


#
# Служебные
#

class Region(db.Model):
    __tablename__ = 'sm_countries'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)

    cities = db.relationship('City', backref=db.backref('region'))


class City(db.Model):
    __tablename__ = 'sm_cities'

    id = db.Column(db.Integer, primary_key=True)
    region_id = db.Column(db.Integer, db.ForeignKey(Region.id))
    title = db.Column(db.Text)
