# coding: utf-8
from flask import current_app
from flask.ext.babel import gettext as _


class LanguageHelper:
    @classmethod
    def listing(cls):
        """
        Возвращает список кортежей доступных языков.
        Доступные языки настраиваются в src/karcher/config/base.cfg - LANGUAGES
        Формат:
            [('ru', 'Русский'), ('en': 'English'), ...]
        """
        return [(key.lower(), value) for key, value in current_app.config.get('LANGUAGES', {}).items()]

    @classmethod
    def title_by_code(cls, code):
        """
        Возвращает название языка по коду.
        Если язык не найден возвращается строка Unknown language.
        """
        c = code.lower()
        languages = current_app.config.get('LANGUAGES', {})

        if c in languages.keys():
            return languages[c]

        return _('Unknown language string')

    @classmethod
    def check_code(cls, code):
        return code.lower() in current_app.config.get('LANGUAGES', {}).keys()
