-- Initialize pages
INSERT INTO pages (id, title, published, language, _main, slug) VALUES (1, 'Главная страница', true, 'ru', true, 'main');
SELECT setval('pages_id_seq', (SELECT MAX(id) FROM pages));


--
--
--
INSERT INTO epages (id, title) VALUES (1, 'Hd1Tx2Sp2 Left embedded page');
INSERT INTO epages (id, title) VALUES (2, 'Hd1Tx2Sp2 Right embedded page');
SELECT setval('epages_id_seq', (SELECT MAX(id) FROM epages));

--
-- Test blocks
--
INSERT INTO page_blocks (id, embedded_page_id, position, comment, published, status, type_, font_color_id, background_color_id, background_size, background_position, background_image) VALUES (
    1, 2, 1, 'Первый блок', true, 'Active', 'Hd1Tx2Ft1', null, null, 'cover', 'center bottom', '{1.jpg, 1.jpg}'
);
INSERT INTO page_blocks (id, embedded_page_id, position, comment, published, status, type_, font_color_id, background_color_id, background_size, background_position, background_image) VALUES (
    2, 2, 2, 'Второй блок', true, 'Active', 'Hd1Tx2Hd1Im1', null, null, 'auto', 'center center', '{}'
);
INSERT INTO page_blocks (id, embedded_page_id, position, comment, published, status, type_, font_color_id, background_color_id, background_size, background_position, background_image) VALUES (
    3, 2, 3, 'Третий блок', true, 'Active', 'Hd1Tx2Calc', 5, 2, 'auto', 'center center', '{}'
);
INSERT INTO page_blocks (id, embedded_page_id, position, comment, published, status, type_, font_color_id, background_color_id, background_size, background_position, background_image) VALUES (
    4, 2, 4, 'Четвёртый блок', true, 'Active', 'Tx4In1', 6, 2, 'auto', 'center center', '{}'
);
INSERT INTO page_blocks (id, embedded_page_id, position, comment, published, status, type_, font_color_id, background_color_id, background_size, background_position, background_image) VALUES (
    5, 2, 5, 'Пятый блок', true, 'Active', 'Hd1Im1Tx1R', 6, 2, 'cover', 'center bottom', '{2.jpg, 2.jpg}'
);

INSERT INTO page_blocks (id, page_id, position, comment, published, status, type_) VALUES (
    6, 1, 1, 'Сплит блок', true, 'Active', 'Hd1Tx2Sp2'
);

INSERT INTO page_blocks (id, embedded_page_id, position, comment, published, status, type_) VALUES (
    7, 1, 1, 'Первый блок слева', true, 'Active', 'Hd1In2Tx2In2'
);
INSERT INTO page_blocks (id, embedded_page_id, position, comment, published, status, type_) VALUES (
    8, 1, 2, 'Второй блок слева', true, 'Active', 'Hd1In2Tx2In2'
);
INSERT INTO page_blocks (id, embedded_page_id, position, comment, published, status, type_) VALUES (
    9, 1, 3, 'Третий блок слева', true, 'Active', 'Hd1In2Tx2In2'
);
INSERT INTO page_blocks (id, embedded_page_id, position, comment, published, status, type_, font_color_id, background_color_id) VALUES (
    10, 1, 10, 'Контакты', true, 'Active', 'Hd1Tx1Contacts', 5, 2
);

INSERT INTO hd1tx2sp2 (id, header, left_text, right_text, left_epage_id, right_epage_id, pageblock_id) VALUES (
    1,
    '&nbsp;',
    '<h2>Автовладельцам</h2><p>Средняя стоимость мойки<br><big>100-150 руб</big></p><p>Профессиональное<br>моечное оборудование</p><p>Без очередей</p>',
    '<h2>Инвесторам</h2><p>Высокая рентабельность</p><p>Окупаемость <big>до 2-х лет</big></p><p>Надёжное и эффективное<br>вложение капитала</p>',
    1, 2, 6
);

SELECT setval('page_blocks_id_seq', (SELECT MAX(id) FROM page_blocks));


-- Для embedded
INSERT INTO hd1in2tx2in2 (id, left_text, right_text, pageblock_id) VALUES (
    1,
    '<h2>Без очередей и ожидания</h2><h4>Хотите помыть машину быстро и качественно &ndash; тогда вам на мойку самообслуживания Karcher.</h4><ul><li><h4>Нет очередей</h4>(поскольку машины моются очень быстро)</li><li><h4>Круглосуточно без перерывов</h4>(в темное время мойки полностью освещены)</li><li><h4>Качественная мойка круглый год</h4>(полы на мойке подогреваются)</li></ul>',
    '<h4>Вам нужно всего 10 минут!</h4><h4>Посмотрите,<br />как <a href="/" href="/">просто помыть машину</a> и<br /><a href="/" href="/">найдите на карте</a> ближайшую мойку.</h4><p>Сегодня это самый доступный и удобный формат мойки - Не нужно стоять в долгих очередях, а стоимость мойки вас приятно удивит &ndash; в среднем <strong>100-150 рублей</strong>.</p><p>На специально подготовленной площадке Вам будет доступно профессиональное оборудование Kärcher, а так же высокоэффективные и качественные чистящие средства (автошампунь, воск и т.д.)</p>',
    7
);
INSERT INTO hd1in2tx2in2 (id, lt_incut, right_text, pageblock_id) VALUES (
    2,
    '<h6>Давайте посчитаем!</h6><p>Если мыть машину 3 раза в неделю за 100 рублей &ndash; в месяц вы потратите всего лишь 1000 рублей, но при этом каждый день будете ездить на чистом автомобиле.</p>',
    '<h2>Выгодная экономия<h2><h4>Вы экономите свое время :</h4><ul><li>без ожидания в очереди</li><li>без предварительной записи</li></ul><h4>Вы экономите деньги:</h4><p>Качественно помыть автомобиль можно в среднем за 150 рублей. Не только помыть, но и защитить поверхность на длительное время поможет специальный горячий воск, доступный на всех мойках. А специально подготовленная вода (без солей и примесей) не будет оставлять разводов на кузове автомобиля, даже если вы не будите его вытирать.</p><p>&nbsp;</p><h4>Узнайте о нашей технологии<br />и оборудовании <a href="#">больше</a>.</h4>',
    8
);
INSERT INTO hd1in2tx2in2 (id, header, right_text, rb_incut, pageblock_id) VALUES (
    3,
    'Твои правила при мойке собственного автомобиля… и не только',
    '<p>Вы сами решаете, по какой программе мыть автомобиль: просто ополоснуть чистой водой, помыть с автошампунем или сделать это максимально качественно с нанесением горячего воска или с использованием специального чистящего средства для удаления насекомых.</p><p>Вы сами решаете, мыть протекающий люк или нет, качественно почистить весь кузов или помыть только заднее правое колесо. Вы - король ручной мойки.</p>',
    '<h6>Это интересно!</h6><p>Большинство автовладельцев уверены, что могут помыть машину лучше, чем специализированные работники автомоек. А Вы хотите попробовать?</p>',
    9
);

-- End of Для embedded


INSERT INTO hd1tx2ft1 (id, header, left_text, right_text, footer, pageblock_id) VALUES (
    1,
    'Мойки самообслуживания&nbsp;&mdash; высокорентабельный бизнес с&nbsp;минимальными рисками',
    '<h4>Высокая популярность у клиентов</h4>
                                                        <ul>
                                                                <li><h5>На мойках самообслуживания не бывает очередей</h5></li>
                                                                <li><h5>Стоимость мойки всего 100–150 рублей</h5></li>
                                                                <li><h5>Высокое качество мойки благодаря технологиям Karcher</h5></li>
                                                                <li><h5>Каждый второй клиент возвращается</h5></li>
                                                        </ul>',
    '<h4>Минимальные расходы и высокий доход</h4>
                                                        <ul>
                                                                <li><h5>Низкие операционные расходы</h5><small>(чистящие средства и коммунальные платежи)</small></li>
                                                                <li><h5>Отсутствие персонала на самой мойке</h5></li>
                                                                <li><h5>Стабильный поток клиентов — 250 машин в сутки</h5></li>
                                                                <li><h5>Быстро растущая популярность</h5><small>70% клиентов рассказывают о мойках<br>самообслуживания знакомым)</small></li>
                                                        </ul>',
    'Высокая рентабельность бизнеса. Полная окупаемость за 2 года!', 1
);
SELECT setval('hd1tx2ft1_id_seq', (SELECT MAX(id) FROM hd1tx2ft1));

INSERT INTO hd1tx2hd1im1 (id, header_1, left_text, right_text, header_2, pageblock_id) VALUES (
    1,
    'Потенциал рынка моек самообслуживания',
    '<ul>
								<li><h4>Более 40&nbsp;млн. легковых автомобилей в&nbsp;РФ</h4><small>Тенденция дальнейшего роста</small></li>
								<li><h4>Средняя периодичность мойки&nbsp;— 3&nbsp;раза в&nbsp;месяц</h4><small>Постоянство востребованности услуги</small></li>
							</ul>',
    '<ul>
								<li><h4>Дефицит предложения на рынке моечных услуг</h4><small>Постоянные очереди на автомойку</small></li>
								<li><h4>Рынок МСО только формируется</h4><small>Востребованность необычайно высока</small></li>
							</ul>',
    'Количество моек в России', 2
);
SELECT setval('hd1tx2hd1im1_id_seq', (SELECT MAX(id) FROM hd1tx2hd1im1));


INSERT INTO hd1tx2calc (id, header, left_text, right_text, pageblock_id) VALUES (
    1,
    'Гарантированный поток клиентов<br />и быстрый возврат инвестиций',
    '<h4>Для автовладельцев стоимость мойки составит всего 100 рублей.</h3>
							<p>Это в несколько раз меньше, чем на обычных мойках, а значит, вы всегда будете обеспечены клиентами.<br />Ваши клиенты могут выбрать из множества программ мойки, включая покрытие воском и пылесос.</p>
							<p>Эффективность оборудования Karcher сводит к минимуму затраты расходных материалов. В совокупности с минимальными начальными затратами и большим потоком клиентов это позволяет с уверенностью прогнозировать срок полного возврата инвестиций.</p>
							<p>В среднем мойка из 6 постов <b>полностью окупается за 2 года.</b></p>',
    '<h6>Давайте посчитаем!</h6>
	 <p>Насколько выгодным окажется ваш бизнес? Ключевые цифры вы можете узнать уже сейчас с помощью нашего <a href="#">калькулятора</a>.</p>',
    3
);
SELECT setval('hd1tx2calc_id_seq', (SELECT MAX(id) FROM hd1tx2calc));

INSERT INTO tx4in1 (id, lt_text, rt_text, lb_text, rb_text, incut, pageblock_id) VALUES (
    1,
    '<h4>Новейшие технологии</h4>
    <p>Собственное подразделение по разработке новейших технологий позволяет нам быть и оставаться первыми в своей области.</p>',
    '<h4>Kärcher – немецкое качество и гарантия надёжности</h4>
    <p>Высокий выходной контроль качества позволяет быть уверенным в безотказности работы оборудования в условиях высоких нагрузок.</p>',
    '<h4>Оперативный сервис</h4>
    <p>Качество техники Kärcher соответствует самым высоким требованиям. Для моек самообслуживания крайне важно не допускать простоя оборудования, поэтому крайне важен оперативный и квалифицированный сервис.</p>',
    '<h4>Cобственная разработка и производство чистящих средств</h4>
    <p>Мы применяем только высокоэффективные и экологические чистящие средства, безопасные для здоровья человека и для окружающей среды.</p>
    <p>Широкий ассортимент продукции для решения любых задач.</p>',
    '<h2>Kärcher сегодня</h2>',
    4
);
SELECT setval('tx4in1_id_seq', (SELECT MAX(id) FROM tx4in1));

INSERT INTO hd1im1tx1r (id, header, content, pageblock_id) VALUES (
    1,
    'Контроль в реальном времени',
    '<h4>Учет и контроль моек в реальном времени</h4><p>Возможность удаленного контроля за каждым моечным постом, включая статистику расхода воды, моющих средств и потребления электроэнергии дает вам возможность постоянно &laquo;держать руку на пульсе&raquo; вашего бизнеса, где бы вы ни находились.<br />SMS-сервис и приложения для iOs и Android.<br />&nbsp;</p>',
    5
);
SELECT setval('hd1im1tx1r_id_seq', (SELECT MAX(id) FROM hd1im1tx1r));

INSERT INTO hd1tx1contacts (id, header, content, pageblock_id) VALUES (
    1,
    'Контакты',
    '
<h4>Горячая линия Керхер<br />
8-800-1000-654</h4>
<p>Оформить заказ, получить консультацию.<br />
пн-пт с 9.00 до 19.00 мск<br />
сб-вс с 10.00 до 19.00 мск<br />
<b>info@ru.kaercher.com</b></p>
<p>&nbsp;</p>
<h4>Горячая линия сервисной службы<br />
8-800-770-00-74</h4>
<p>пн-пт с 09.00-18.00 мск<br />
<b>service@ru.kaercher.com</b></p>
    ',
    10
);
SELECT setval('hd1tx1contacts_id_seq', (SELECT MAX(id) FROM hd1tx1contacts));



--
-- Navigaton
--
INSERT INTO navigation_groups (id, caption, slug, published, position, behavior_, navtype_, language) VALUES (
    1, 'Технологии', 'technologies', true, 1, 'OnlyHeader', 'footer', 'ru'
);

SELECT setval('navigation_groups_id_seq', (SELECT MAX(id) FROM navigation_groups));
