command=$1

if [ "$command" = "help" ]; then
    echo '
    Usage:
        deploy.sh command

    Commands:
        init - Runs initdb command
        migrate - Runs migration update command
    '
else
    git pull

    if [ "$command" = "init" ]; then
        bin/flask initdb
    elif [ "$command" = "migrate" ]; then
        bin/flask db upgrade head
    fi

    sudo chown -R serge:devel src/tmp/.webassets-cache/
    bin/flask assets build
    bin/flask collect
    sudo chown -R www:devel src/tmp/.webassets-cache/
    
    bin/flask compilemessages
    sudo ps auwwx | grep karcher | grep fastcgi | awk '{print $2}' | sudo xargs kill
fi