-- Update base objects
update rbac_objects set title = 'RBAC User object' where name ='user';
update rbac_objects set title = 'RBAC Permission object' where name ='permission';
update rbac_objects set title = 'RBAC Role object' where name ='role';

-- Update base operations
update rbac_operations set title = 'RBAC Create operation' where name = 'create';
update rbac_operations set title = 'RBAC Edit operation' where name = 'edit';
update rbac_operations set title = 'RBAC Delete operation' where name = 'delete';
update rbac_operations set title = 'RBAC View operation' where name = 'view';
update rbac_operations set title = 'RBAC Assign operation' where name = 'assign';
update rbac_operations set title = 'RBAC Deassign operation' where name = 'deassign';
update rbac_operations set title = 'RBAC Grant operation' where name = 'grant';
update rbac_operations set title = 'RBAC Revoke operation' where name = 'revoke';

-- Update base permissions
update rbac_permissions set title = 'RBAC Create operation' where obj_name = 'user' and op_name = 'create';
update rbac_permissions set title = 'RBAC Edit operation' where obj_name = 'user' and op_name = 'edit';
update rbac_permissions set title = 'RBAC Delete operation' where obj_name = 'user' and op_name = 'delete';
update rbac_permissions set title = 'RBAC View operation' where obj_name = 'user' and op_name = 'view';

update rbac_permissions set title = 'RBAC Create operation' where obj_name = 'role' and op_name = 'create';
update rbac_permissions set title = 'RBAC Edit operation' where obj_name = 'role' and op_name = 'edit';
update rbac_permissions set title = 'RBAC Delete operation' where obj_name = 'role' and op_name = 'delete';
update rbac_permissions set title = 'RBAC View operation' where obj_name = 'role' and op_name = 'view';
update rbac_permissions set title = 'RBAC Grant operation' where obj_name = 'role' and op_name = 'grant';
update rbac_permissions set title = 'RBAC Revoke operation' where obj_name = 'role' and op_name = 'revoke';

update rbac_permissions set title = 'RBAC Create operation' where obj_name = 'permission' and op_name = 'create';
update rbac_permissions set title = 'RBAC Edit operation' where obj_name = 'permission' and op_name = 'edit';
update rbac_permissions set title = 'RBAC Delete operation' where obj_name = 'permission' and op_name = 'delete';
update rbac_permissions set title = 'RBAC View operation' where obj_name = 'permission' and op_name = 'view';
update rbac_permissions set title = 'RBAC Assign operation' where obj_name = 'permission' and op_name = 'assign';
update rbac_permissions set title = 'RBAC Deassign operation' where obj_name = 'permission' and op_name = 'deassign';

