$(function(){
    clearAjaxStatus();
    $('#ajax_message').text(AJAX_STATUS_UNCHANGED_TEXT);
    $('#ajax_state').show();

	var timer;
	var ajaxInProgress = false;
	
	var scrollTimer;
	
	function clearAjaxStatus(){
		$('#publish_buttons').hide();
		$('#ajax_loader').removeClass('glyphicon-cog glyphicon-spin glyphicon-refresh glyphicon-ok glyphicon-ok-circle glyphicon-remove-circle glyphicon-pencil text-success text-danger');
		$('#ajax_message').text('').removeClass('text-success text-danger');
	}
	
	function showEditBox(where){$(where).css({boxShadow:'#b6b6b6 0 0 10px 5px', outline:'1px solid #b6b6b6'});}
	
	function hideEditBox(where){$(where).css({boxShadow:'none', outline:'none'});}
	
	function notifyError(text){
		text = typeof text !== 'undefined' ? text : '';
		clearAjaxStatus();
		$('#ajax_loader').addClass('glyphicon-remove-circle text-danger');
		$('#ajax_message').text(AJAX_STATUS_ERROR_TEXT).addClass('text-danger');
		if(text != ''){
			UIkit.notify(text, {status:'danger'});
		}
	}
	
	function notifySuccess(text){
		text = typeof text !== 'undefined' ? text : '';
		clearAjaxStatus();
		$('#ajax_loader').addClass('glyphicon-ok text-success');
		$('#ajax_message').text(AJAX_STATUS_SAVED_TEXT).addClass('text-success');
		$('#publish_buttons').show();
		if(text != ''){
			UIkit.notify(text, {status:'success'});
		}
	}
	
	function ajaxSuccess(data, textStatus, jqXHR){
		clearAjaxStatus();

//		console.log('Response:');
//		console.dir(data);

		if(data.global_result.status != 0){
			notifyError(data.global_result.status_msg);
			return;
		} else {
			$.each(data.blocks, function(blockId, blockData){
//				console.log(blockId);
				$.each(blockData.fields, function(fieldName, fieldData){
					if(fieldData.status == 0){
						field = '#' + blockId + '_' + fieldName;
//						console.log(field);
						$(field).attr('data-dirty', 'false');
					} else {
						notifyError(fieldData.status_msg);
					}
//					console.log('    ' + fieldName);
//					console.dir(fieldData);
				});
			});
		}
		if($('[data-dirty="true"]').length == 0){
			notifySuccess();
		} else {
			notifyError(); // TODO: separate errors
		}
	}
	
	function ajaxError(jqXHR, textStatus, errorThrown){
		clearAjaxStatus();
		$('#ajax_loader').addClass('glyphicon-remove-circle text-danger');
		$('#ajax_message').text(AJAX_STATUS_ERROR_TEXT).addClass('text-danger');
	}
	
	function ajaxDone(){ajaxInProgress = false;}
	
	function save(){
		if(ajaxInProgress){
			if(timer) clearTimeout(timer);
			timer = setTimeout(save, 500);
			return; // another request in progress
		}

		if($('[data-dirty="true"]').length == 0){
			return; // nothing to save
		}

		var data = {};
		$('.block').each(function(){
			var blockId = $(this).attr('id');
			console.log(blockId);
			var id = $(this).attr('data-block_id');
			var fields = {};
			$(this).find('[contenteditable="true"]').each(function(){
				if($(this).attr('data-dirty') == 'true'){
					var value = CKEDITOR.instances[$(this).attr('id')].getData();
					fields[$(this).attr('data-field')] = {content: value};
				}
			});
			data[blockId] = {
				id: id,
				fields: fields
			}
		});
		
		ajaxInProgress = true;
		clearAjaxStatus();
		$('#ajax_loader').addClass('glyphicon-spin glyphicon-refresh');
		$('#ajax_message').text(AJAX_STATUS_SAVING_TEXT);
		
		console.log('Request:');
		console.dir(data);
		
		$.ajax(AJAX_CONTENTEDITABLE_URL, {method: 'POST', contentType: 'application/json', data: JSON.stringify(data), dataType: 'json'}).done(ajaxSuccess).fail(ajaxError).always(ajaxDone);
	}
	
	CKEDITOR.on('instanceReady', function(event){
		event.editor.on('focus', function(){
			showEditBox('#'+this.name);
		});
		event.editor.on('blur', function(){
			hideEditBox('#'+this.name);

			if(timer) clearTimeout(timer);
			save();
		});
		event.editor.on('change', function(){
			clearAjaxStatus();
			$('#ajax_loader').addClass('glyphicon-pencil');
			$('#ajax_message').text(AJAX_STATUS_CHANGED_TEXT);
			$('#'+this.name).attr('data-dirty', true);
			
			if(timer) clearTimeout(timer);
			timer = setTimeout(save, 5000);
		});
	});
	
	var oldScroll = $(window).scrollTop();
	var isScrolling = false;
	
	$(document).on('scroll', onScroll);
	
	function alignBlocks(){
		if(isScrolling) return;
		console.log('align');
		$('.block').each(function(i){
			if(Math.abs($(this).offset().top - $(document).scrollTop()) < $(window).height() / 2) {
				isScrolling = true;
				$('html').animate({scrollTop: $(this).offset().top}, {complete: function(){ isScrolling = false; }}, 250);
			}
		});
	}
	
	function onScroll(){
		if(isScrolling) return;
		
		if(scrollTimer) clearTimeout(scrollTimer);
	
		var delta = $(this).scrollTop() - oldScroll;
		oldScroll = $(this).scrollTop();

		var h = $(window).height();
		var h10 = h / 10;
		
		$('.block').each(function(i){
			var top = $(this).offset().top;
			if(delta > 0){
				if((top - h10 > oldScroll) && (top + h10 < oldScroll + h)){
					isScrolling = true;
					$('html').animate({scrollTop: top}, {complete: function(){ isScrolling = false; }}, 250);
					return;
				} else {
					if(scrollTimer) clearTimeout(scrollTimer);
					scrollTimer = setTimeout(alignBlocks, 750);
				}
			} else {
				if((top - h10 > oldScroll - h) && (top + h10 < oldScroll)){
					isScrolling = true;
					$('html').animate({scrollTop: top}, {complete: function(){ isScrolling = false; }}, 250);
					return;
				} else {
					if(scrollTimer) clearTimeout(scrollTimer);
					scrollTimer = setTimeout(alignBlocks, 750);
				}
			}
		});
	}
});