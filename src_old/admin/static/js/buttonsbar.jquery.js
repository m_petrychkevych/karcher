$.fn.buttonsBar = function(data) {
    var $elem = this;
    $('<div class="buttons-bar-holder"></div>').insertBefore($elem).outerHeight($elem.outerHeight()).hide();
	$('<div class="buttons-bar-background"></div>').insertBefore($(".buttons-bar-content", $elem)); //.css("opacity", .7);
	checkFix($elem);
	$(window).on("scroll resize", function() {
		checkFix($elem);
	});
	function checkFix(saveCancelBlock) {
		var form = saveCancelBlock.closest("form");
		var formOffset = form.offset().top;
		var formheight = form.outerHeight();
		var windowHeight = $(window).height();
		var windowScroll = $(window).scrollTop();
		var saveCancelBlockHeight = saveCancelBlock.outerHeight();
		var correction = 0;
		var checkTop = windowHeight + windowScroll - formOffset - saveCancelBlockHeight - correction;
		var checkBottom = windowHeight + windowScroll - formOffset - formheight;
		if (checkTop > 0 && checkBottom < 0){
			setFix($elem);
		} else {
			unsetFix($elem);
		}
	}
	function setFix(saveCancelBlock){
		saveCancelBlock.addClass("buttons-bar__fixed");
		$(".buttons-bar-content", saveCancelBlock).addClass("container");
		$(".buttons-bar-holder", saveCancelBlock.closest("form")).show();
		$(".buttons-bar-background", saveCancelBlock).fadeIn();
	}
	function unsetFix(saveCancelBlock){
		saveCancelBlock.removeClass("buttons-bar__fixed");
		$(".buttons-bar-content", saveCancelBlock).removeClass("container");
		$(".buttons-bar-holder", saveCancelBlock.closest("form")).hide();
		$(".buttons-bar-background", saveCancelBlock).fadeOut();
	}
	return $elem;
}