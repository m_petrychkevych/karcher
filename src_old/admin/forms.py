import sqlalchemy as sa

from brpr.flask.widgets import WidgetPrebind
from core.forms.fields import ColorSelect, UploadField
from core.forms import Form
from flask.ext.babel import lazy_gettext as __
from wtforms import BooleanField, HiddenField, FileField, PasswordField, SelectField, TextField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import Email, Required
from wtforms.widgets import PasswordInput, RadioInput, Select, TextInput
from wtforms.widgets.html5 import EmailInput

from karcher.models import (BlockCollection,
                            ColorDictionary,
                            Page,
                            PageBlock,
                            Site)
from users.models import User


class LoginForm(Form):
    login = TextField(__('Login label'),
                      validators=[Required(__('Please, provide your login message')),
                                  Email(__('Invalid email message'))],
                      widget=WidgetPrebind(EmailInput(),
                                           class_='form-control uk-form-large',
                                           placeholder=__('Login placeholder'),
                                           autofocus='on',
                                           size=30))
    password = PasswordField(__('Password label'),
                             validators=[Required()],
                             widget=WidgetPrebind(PasswordInput(),
                                                  class_='form-control uk-form-large',
                                                  placeholder=__('Password placeholder'),
                                                  size=30,
                                                  autocomplete='off'))
    remember_me = BooleanField(__('Remember me label'), default=False)
    next = HiddenField()

    def validate(self):
        if not Form.validate(self):
            return False

        user = User.query.filter(sa.func.lower(User.login) == self.login.data.lower(),
                                 User.activity.is_(True)).first()

        if not user or not user.check_password(self.password.data):
            self.errors.update({'form': [__('Invalid username or password message')]})
            return False

        self.user = user
        return True


class SitepointForm(Form):
    title = TextField(__('Page title label'),
                      validators=[Required()],
                      widget=WidgetPrebind(TextInput(), class_='uk-width-1-1'))
    parent = QuerySelectField(get_label='tree_title',
                              query_factory=lambda: Site.admin_list())
    slug = TextField(__('Page slug label'))
    published = BooleanField(__('Page published label'))


class PageForm(Form):
    title = TextField(__('Page title label'),
                      validators=[Required()],
                      widget=WidgetPrebind(TextInput(),
                                           class_='form-control uk-width-1-1'))
    published = BooleanField(__('Page published label'))


class PageBlockForm(Form):
    comment = TextField(__('Pageblock comment label'),
                        validators=[Required()],
                        description=__('Pageblock comment description'),
                        widget=WidgetPrebind(TextInput(), class_='form-control'))
    published = BooleanField(__('Pageblock published label'))
    type_ = SelectField(__('Pageblock type label'), choices=BlockCollection.listing(),
                        widget=WidgetPrebind(Select(), class_='form-control'))

    # Common block attributes
    font_color = QuerySelectField(__('Font color label'),
                                  query_factory=ColorDictionary.font_colors,
                                  widget=WidgetPrebind(ColorSelect(), class_='form-control color-selector'))
    background_color = QuerySelectField(__('Background color label'),
                                        query_factory=ColorDictionary.background_colors,
                                        widget=WidgetPrebind(ColorSelect(), class_='form-control color-selector'))
    background_position = SelectField(__('Pageblock background-position label'),
                                      choices=PageBlock.positionBackgrounds,
                                      widget=WidgetPrebind(Select(), class_='form-control'))
    background_size = SelectField(__('Pageblock background-size label'),
                                  choices=PageBlock.sizeBackgrounds,
                                  widget=WidgetPrebind(Select(), class_='form-control'))
    bimage = UploadField(__('Pageblock background-image label'))

    def __init__(self, *args, **kwargs):
        self.update = kwargs.get('update')
        super(PageBlockForm, self).__init__(*args, **kwargs)

        if self.update:
            self.type_.widget = WidgetPrebind(Select(), class_='form-control', disabled='disabled')

    def process(self, formdata=None, obj=None, **kwargs):
        super(PageBlockForm, self).process(formdata, obj, **kwargs)
