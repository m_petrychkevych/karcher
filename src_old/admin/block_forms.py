# coding: utf-8
from brpr.flask.widgets import WidgetPrebind
from core.forms import Form, wForm
from wtforms import TextField
from wtforms.widgets import TextArea, TextInput


class Hd1Tx2Ft1Form(Form):
    header = TextField(u'Заголовок',
                       widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))
    left_text = TextField(u'Слева',
                         widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    right_text = TextField(u'Справа',
                         widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    footer = TextField(u'Нижний текст',
                       widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))


class Hd1Tx2Hd1Im1Form(Form):
    header_1 = TextField(u'Заголовок',
                         widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))
    left_text = TextField(u'Слева',
                         widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    right_text = TextField(u'Справа',
                         widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    header_2 = TextField(u'Заголовок 2',
                         widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))


class Hd1Tx2CalcForm(Form):
    header_1 = TextField(u'Заголовок',
                         widget=WidgetPrebind(TextInput(), class_='form-control uk-width-1-1'))
    left_text = TextField(u'Слева',
                         widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
    right_text = TextField(u'Справа',
                         widget=WidgetPrebind(TextArea(), rows=10, class_='form-control uk-width-1-1'))
