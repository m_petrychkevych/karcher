from brpr.rbac.models import Role
from flask import current_app
from flask.ext.sqlalchemy import SQLAlchemy


db = SQLAlchemy(current_app) if 'sqlalchemy' not in current_app.extensions.keys() else current_app.extensions.get('sqlalchemy').db
rbac = RBAC(current_app) if 'rbac' not in current_app.extensions else current_app.extensions.get('rbac')
