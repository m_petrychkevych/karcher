import json
import os
import re

from core.forms.utils import is_empty, check_file_extension
from core.utils.request import clear_filters, process_filters
from flask import abort, Blueprint, current_app, flash, redirect, render_template, request, url_for
from flask.ext.babel import gettext as _, lazy_gettext as __
from flask.ext.login import current_user, login_required, login_user, logout_user
from werkzeug import secure_filename

from karcher.models import Page, PageBlock, Site
from users.forms import UserFiltersForm, UserForm
from users.models import roles, user_created, User, UserProfile

from .forms import (LoginForm,
                    PageBlockForm,
                    PageForm,
                    SitepointForm)
from .models import db, rbac, Role


admin = Blueprint('admin',
                  __name__,
                  template_folder='templates',
                  static_folder='static',
                  url_prefix='/admin')


current_app.login_manager.login_view = 'admin.login'

pages_tabs = [dict(alias='general', title=__('Page general tab')),
              dict(alias='blocks', title=__('Page blocks tab'))]


@admin.route('/')
@login_required
def dashboard():
    return render_template('admin/dashboard.html')


@admin.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated():
        return redirect(url_for('admin.dashboard'))

    login_form = LoginForm() if request.method == 'POST' else LoginForm(request.args)

    if login_form.validate_on_submit():
        if login_user(login_form.user, remember=login_form.remember_me.data) is True:
            flash(_('Logeed in successfully message'))
        else:
            flash(_('Logging failed message'))

        return redirect(login_form.next.data or url_for('admin.dashboard'))

    return render_template('admin/login.html',
                           login_form=login_form)


@admin.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(request.args.get('next') or url_for('admin.dashboard'))


@admin.route('/users/clear_filters')
@login_required
def users_list_clear_filters():
    clear_filters('user_filters')
    return redirect(url_for('admin.users_list'))


@admin.route('/users')
@login_required
def users_list():
    session_filters = process_filters(request, 'user_filters')
    filters_form = UserFiltersForm(session_filters)
    users, total = User.admin_list(filters_form=filters_form, with_total=False)
    # print(is_empty(filters_form), filters_form.data)  # DEBUG

    return render_template('admin/users.html',
                           users=users,
                           filters_form=filters_form,
                           filtered_list=not is_empty(filters_form))


@admin.route('/users/add', methods=['GET', 'POST'])
@admin.route('/users/<int:user_id>', methods=['GET', 'POST'])
@login_required
def manage_user(user_id=None):
    data = dict()
    is_current_user = current_user.id == user_id

    if user_id:
        user = User.query.get(user_id) if not is_current_user else current_user

        if not user:
            flash(_('User with %(user_id)s not found message', user_id=user_id), 'warning')
            return redirect(url_for('admin.users_list'))

        profile = user.profile
        data = dict(user.__dict__.items() + profile.__dict__.items())
        assigned_roles = rbac.assignedRoles(user.login)

        if assigned_roles:
            data['roles'] = roles.filter(Role.id.in_([role.id for role in rbac.assignedRoles(user.login)]))
    else:
        user = User()
        profile = UserProfile(user=user)

    data['update'] = True if user_id else False
    data['user_id'] = user_id
    data['is_current_user'] = is_current_user
    user_form = UserForm(**data)

    if user_form.validate_on_submit():
        try:
            user_form.populate_obj(user)
            user_form.populate_obj(profile)

            if not user_id:
                db.session.add(user)
                db.session.add(profile)
                db.session.flush()
            else:
                assigned = [role.id for role in rbac.assignedRoles(user.login)]

                for rid in assigned:
                    rbac.deassignUser(user.login, rid)

                for role in user_form.roles.data:
                    rbac.assignUser(user.login, role.id)

            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)

            if user_id:
                flash(_("Can't edit user %(login)s message", login=user.login), 'danger')
            else:
                flash(_("Can't register user %(login)s message", login=user.login), 'danger')
        else:
            if not user_id:
                user_created.send(current_app, user=user, roles=user.roles or [])
                flash(_('User %(login)s added successfully message', login=user.login), 'success')
            else:
                flash(_('User %(login)s saved successfully message', login=user.login), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_user', user_id=user.id))

        return redirect(url_for('admin.users_list'))

    return render_template('admin/manage_user.html',
                           user_id=user_id,
                           user=user,
                           user_form=user_form)


@admin.route('/site')
@login_required
def site():
    site = Site.admin_list().all()

    return render_template('admin/site.html',
                           site=site)


@admin.route('/sites/add', methods=['GET', 'POST'])
@admin.route('/sites/<int:site_point_id>', methods=['GET', 'POST'])
@login_required
def manage_site_point(site_point_id=None):
    if site_point_id:
        site_point = db.session.query(Site).filter(Site.id.__eq__(site_point_id)).first()

        if not site_point:
            flash(_('Site point with id %(id)s not found message', id=site_point_id), 'warning')
            return redirect(url_for('admin.site'))

        page = site_point.page
    else:
        page = Page()
        site_point = Site(page=page)

    data = dict(site_point.__dict__.items() + page.__dict__.items())
    form = SitepointForm(**data)

    if form.validate_on_submit():
        try:
            form.populate_obj(site_point)
            form.populate_obj(page)

            if not site_point_id:
                db.session.add(site_point)
                db.session.add(page)

            db.session.commit()
        except Exception as e:
            db.session.rollback()

            if site_point_id:
                current_app.logger.exception(_("Can't save site point message"), 'error')
            else:
                current_app.logger.exception(_("Can't add site point message"), 'error')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_site_point', site_point_id=site_point_id))

        return redirect(url_for('admin.site'))

    return render_template('admin/manage_sitepoint.html',
                           site_point=site_point,
                           site_point_id=site_point_id,
                           form=form)


@admin.route('/pages')
@login_required
def pages_list():
    pages = Page.admin_list().all()

    return render_template('admin/pages.html',
                           pages=pages)


@admin.route('/pages/<int:page_id>/edit')
@login_required
def manage_page_content(page_id):
    page = Page.query.get(page_id)

    if not page:
        flash(_('Page with id %(id)s not found message', id=page_id), 'warning')
        return redirect(url_for('admin.pages_list'))

    return page.render(editable=True)


@admin.route('/pages/<int:page_id>/api/save', methods=['POST'])
@login_required
def ajaxapi_page_content_save(page_id):
    """ Save pageblock data by ajax. """
    response_data = {'global_result': {'status': 0, 'status_msg': ''}}

    if request.is_xhr:
        page = Page.query.get(page_id)
    
        if not page:
            response_data['global_result']['status'] = 404
            response_data['global_result']['status_msg'] = _('Page %(id)s not found message', id=page_id)
            return json.dumps(response_data)
    
        request_data = request.get_json(force=True)

        if request_data:
            response_data.setdefault('blocks', {})

            for block, detail in request_data.items():
                response_data['blocks'].setdefault(block, {'status': 0, 'status_msg': '', 'fields': {}})
                page_block = None
                block_id = detail.get('id', None)

                if block_id:
                    page_block = PageBlock.query.get(block_id)

                if not page_block:
                    response_data['blocks'][block]['status'] = 404
                    response_data['blocks'][block]['status_msg'] = _('', id=block_id)
                    continue

                # Save
                data_to_save = {field_name: field_content.get('content') for field_name, field_content in detail.get('fields', {}).items()}

                for field_name in detail.get('fields', {}).keys():
                    response_data['blocks'][block]['fields'].setdefault(field_name, {'status': 0, 'status_msg': ''})

                pb_data = page_block.get_data_object()

                if pb_data:
                    for field_name, field_content in data_to_save.items():
                        if hasattr(pb_data, field_name):
                            try:
                                setattr(pb_data, field_name, field_content)
                                db.session.commit()
                            except Exception as e:
                                db.session.rollback()
                                current_app.logger.exception(e)
                                response_data['blocks'][block]['fields'][field_name]['status'] = 500
                                response_data['blocks'][block]['fields'][field_name]['status_msg'] = _("Can't save block field %(name)s message", name=field_name)
                        else:
                            response_data['blocks'][block]['fields'][field_name]['status'] = 404
                            response_data['blocks'][block]['fields'][field_name]['status_msg'] = _('Block field %(name)s not found message', name=field_name)

        return json.dumps(response_data)

    response_data['global_result']['status'] = 403
    return json.dumps(response_data)


@admin.route('/pages/add', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>/<any(general, blocks):active_tab>', methods=['GET', 'POST'])
@login_required
def manage_page(page_id=None, active_tab='general'):
    if page_id:
        query = Page.query.filter(Page.id.__eq__(page_id))
        page = query.first()

        if not page:
            flash(_('Page with id %(id)s not found message', id=page_id), 'warning')
            return redirect(url_for('admin.pages_list'))
    else:
        page = Page()

    form = None

    if active_tab == 'general':
        form = PageForm(obj=page)

    if form and form.validate_on_submit():
        try:
            form.populate_obj(page)

            if not page_id:
                db.session.add(page)

            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)

            if not page_id:
                flash(_("Can't add page message"), 'danger')
            else:
                flash(_("Can't save page message"), 'danger')
        else:
            if not page_id:
                flash(_('Page added successfully message'), 'success')
            else:
                flash(_('Page saved successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_page', page_id=page.id))

        return redirect(url_for('admin.pages_list'))

    if not form and request.method == 'POST':
        if active_tab == 'blocks':
            positions = request.form.getlist('positions', type=int)
            to_remove = request.form.getlist('toremove', type=int)
            trash_selected = 'trash_selected' in request.form

            if positions and not trash_selected:
                try:
                    for b in page.blocks:
                        if b.id in positions:
                            b.position = positions.index(b.id) + 1

                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    current_app.logger.exception(e)
                else:
                    flash(_('Blocks order saved successfully message'), 'success')

            if to_remove and trash_selected:
                try:
                    blocks = {b.id: b for b in page.blocks}

                    for tr in to_remove:
                        obj = blocks.get(tr)

                        if obj:
                            if obj.status == PageBlock.statusActive:  # Mark as Trashed if active
                                obj.status = PageBlock.statusTrash
                            elif obj.status == PageBlock.statusTrash:  # Physically remove object if trashed
                                db.session.delete(obj)

                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    current_app.logger.exception(e)
                else:
                    flash(_('Selected pageblocks moved to trash message'), 'success')

        return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    return render_template('admin/manage_page.html',
                           page=page,
                           page_id=page_id,
                           form=form,
                           tabs=pages_tabs,
                           active_tab=active_tab)


@admin.route('/pages/<int:page_id>/blocks/add', methods=['GET', 'POST'])
@admin.route('/pages/<int:page_id>/blocks/<int:pageblock_id>', methods=['GET', 'POST'])
@login_required
def manage_pageblock(page_id, pageblock_id=None):
    active_tab='blocks'
    page = Page.query.get(page_id)

    if not page:
        flash(_('Page with id %(id)s not found message', id=page_id), 'warning')
        return redirect(url_for('admin.pages_list'))

    if pageblock_id:
        query = PageBlock.query.filter(PageBlock.id.__eq__(pageblock_id))
        pageblock = query.first()

        if not pageblock:
            flash(_('Pageblock with id %(id)s not found message', id=pageblock_id), 'warning')
            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

        if pageblock.page != page:
            flash(_('Pageblock with id %(id)s not found in current page message', id=pageblock_id), 'warning')
            return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))
    else:
        pageblock = PageBlock()

    form = PageBlockForm(obj=pageblock, update=True if pageblock_id else False)

    if form.validate_on_submit():
        folder = os.path.join(current_app.config.get('PUBLIC_IMAGES', ''), 'blocks_bg')
        file_to_remove = None

        try:
            form.populate_obj(pageblock)

            if not pageblock_id:
                pageblock.page = page
                db.session.add(pageblock)
                db.session.flush()
                attrs = pageblock.create_all_data()
                db.session.add(attrs)

            # Mark old file to remove first
            if pageblock.background_image and os.path.isfile(os.path.join(folder, pageblock.background_image)):
                file_to_remove = os.path.join(folder, pageblock.background_image)

            if form.bimage.data:
                if isinstance(form.bimage.data, basestring):
                    file_to_remove = False
                else:
                    if check_file_extension(form.bimage.data.filename, set(['jpg', 'jpeg', 'png', 'gif'])):
                        filename = secure_filename(form.bimage.data.filename)
                        form.bimage.data.save(os.path.join(folder, filename))
                        pageblock.background_image = filename
            else:
                pageblock.background_image = ''

            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)

            if not page_id:
                flash(_("Can't add pageblock message"), 'danger')
            else:
                flash(_("Can't save pageblock message"), 'danger')
        else:
            if file_to_remove:
                try:
                    os.unlink(file_to_remove)
                except Exception as e:
                    current_app.logger.exception(e)

            if not pageblock_id:
                flash(_('Pageblock added successfully message'), 'success')
            else:
                flash(_('Pageblock saved successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_pageblock', page_id=page_id, pageblock_id=pageblock.id))

        return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    return render_template('admin/manage_pageblock.html',
                           page=page,
                           page_id=page_id,
                           pageblock=pageblock,
                           pageblock_id=pageblock_id,
                           form=form,
                           active_tab=active_tab)


@admin.route('/page/<int:page_id>/blocks/<int:pageblock_id>/data', methods=['GET', 'POST'])
@login_required
def manage_pageblock_data(page_id, pageblock_id, active_tab='blocks'):
    page = Page.query.get(page_id)

    if not page:
        flash(_('Page with id %(id)s not found message', id=page_id), 'warning')
        return redirect(url_for('admin.pages_list'))

    query = PageBlock.query.filter(PageBlock.id.__eq__(pageblock_id))
    pageblock = query.first()

    if not pageblock:
        flash(_('Pageblock with id %(id)s not found message', id=pageblock_id), 'warning')
        return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    if pageblock.page != page:
        flash(_('Pageblock with id %(id)s not found in current page message', id=pageblock_id), 'warning')
        return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    pb_data = pageblock.get_data_object()
    pageblock_form = pageblock.get_form()
    form = pageblock_form(obj=pb_data) if pageblock_form else None

    if form and form.validate_on_submit():
        try:
            form.populate_obj(pb_data)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            current_app.logger.exception(e)
            flash(_("Can't save pageblock data message"), 'danger')
        else:
            flash(_('Pageblock data saved successfully message'), 'success')

        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock.id))

        return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))
    elif not form and request.method == 'POST':
        if 'submit_and_continue' in request.form:
            return redirect(url_for('admin.manage_pageblock_data', page_id=page_id, pageblock_id=pageblock.id))

        return redirect(url_for('admin.manage_page', page_id=page_id, active_tab=active_tab))

    return render_template('admin/manage_pageblock_data.html',
                           page=page,
                           page_id=page_id,
                           pageblock=pageblock,
                           pageblock_id=pageblock_id,
                           form=form,
                           active_tab=active_tab)
