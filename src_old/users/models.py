# coding: utf-8
from blinker import Namespace
from brpr.rbac.models import Role, users_roles
from flask import current_app
from flask.ext.babel import gettext as _
from flask.ext.login import current_user, UserMixin
from flask.ext.rbac import RBAC, RbacUser
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy.ext.hybrid import hybrid_property
from werkzeug.security import check_password_hash, generate_password_hash


db = SQLAlchemy(current_app) if 'sqlalchemy' not in current_app.extensions.keys() else current_app.extensions.get('sqlalchemy').db
rbac = RBAC(current_app) if 'rbac' not in current_app.extensions.keys() else current_app.extensions.get('rbac')
roles = db.session.query(Role).order_by(Role.title.asc())


user_signals = Namespace()
user_created = user_signals.signal('user_created')
user_deleted = user_signals.signal('user_deleted')


@user_created.connect_via(current_app)
def create_user(sender, user, roles=['customer']):
    rbac.addUser(user.login, user.fullname)

    for role in roles:
        rbac.assignUser(user.login, role if type(role) is str else role.id)


class User(db.Model, UserMixin, RbacUser):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String, unique=True, nullable=False)
    pw_hash = db.Column(db.String)
    activity = db.Column(db.Boolean, default=False, server_default='false')
    system = db.Column(db.Boolean, default=False, server_default='false')
    created_at = db.Column(db.DateTime, db.DefaultClause(db.func.now()))
    updated_at = db.Column(db.DateTime, db.DefaultClause(db.func.now()), onupdate=db.func.now())

    __mapper_args__ = {
        'order_by': created_at.desc()
    }

    def __unicode__(self):
        return self.profile.firstname

    @classmethod
    def admin_list(cls, filters_form=None, with_total=True):
        query = cls.query

        if filters_form:
            if 'login' in filters_form and filters_form.login.data:
                mask = u'%{0}%'.format(filters_form.login.data)
                query = query.filter(cls.login.ilike(mask))

            if 'fullname' in filters_form and filters_form.fullname.data:
                words = filters_form.fullname.data.split()

                for word in words:
                    mask = u'%{0}%'.format(word)
                    query = query.filter(cls.profile.has(db.or_(UserProfile.firstname.ilike(mask),
                                                                UserProfile.lastname.ilike(mask))))

            if 'activity' in filters_form and filters_form.activity.data in ['True', 'False']:
                mask = True if filters_form.activity.data == 'True' else False
                query = query.filter(cls.activity.is_(mask))

            if 'role' in filters_form and filters_form.role.data:
                subq = db.session.query(users_roles)
                subq = subq.join(Role, Role.id == users_roles.c.role_id)
                subq = subq.filter(Role.id == filters_form.role.data.id).subquery()
                query = query.join(subq, cls.login == subq.c.user_name)

        count = query.count() if with_total else 0

        return query, count

    def check_password(self, password):
        return check_password_hash(self.pw_hash, password)

    @hybrid_property
    def fullname(self):
        return self.profile.fullname

    @classmethod
    def load_user(cls, user_id):
        try:
            user = User.query.filter(User.id == user_id,
                                     User.activity.is_(True)).first()

            if user:
                roles = rbac.assignedRoles(user.rbac_id)
                user.roles = roles or []

            return user
        except Exception as e:
            current_app.logger.exception(e)

    @property
    def password(self):
        return self.pw_hash

    @password.setter
    def password(self, password):
        if password:
            self.pw_hash = generate_password_hash(password)

    def printable_roles(self):
        return ', '.join([r.title for r in self.roles_list()]) or _('User has no role assignment string')

    @property
    def rbac_id(self):
        return self.login

    def roles_list(self):
        return sorted(rbac.assignedRoles(self.login) or [], key=lambda v: v.name)


class UserProfile(db.Model):
    __tablename__ = 'users_profiles'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    lastname = db.Column(db.String)
    firstname = db.Column(db.String)

    user = db.relationship(User,
                           lazy='joined',
                           uselist=False,
                           backref=db.backref('profile', lazy='joined', uselist=False))

    @hybrid_property
    def fullname(self):
        if not self.lastname:
            return self.firstname

        return self.firstname + ' ' + self.lastname
