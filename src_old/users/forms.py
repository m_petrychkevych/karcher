import sqlalchemy as sa

from brpr.flask.widgets import WidgetPrebind
from core import all_list, yesno_list
from core.forms import Form, wForm
from flask.ext.babel import gettext as _, lazy_gettext as __
from wtforms import BooleanField, PasswordField, SelectField, TextField
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import Email, EqualTo, Optional, Required
from wtforms.widgets import CheckboxInput, ListWidget, PasswordInput, Select, TextInput
from wtforms.widgets.html5 import EmailInput

from users.models import User, roles


class UserForm(Form):
    login = TextField(__('Login label'),
                      validators=[Required(), Email()],
                      widget=WidgetPrebind(EmailInput(),
                                           class_='form-control uk-width-1-4'))
    password = PasswordField(__('Password label'),
                             validators=[EqualTo('confirm', message=__('Passwords must match mesage'))],
                             widget=WidgetPrebind(PasswordInput(),
                                                  class_='form-control uk-width-1-6',
                                                  autocomplete='off'))
    confirm = PasswordField(__('Confirm password label'),
                            widget=WidgetPrebind(PasswordInput(),
                                                 class_='form-control uk-width-1-6',
                                                 autocomplete='off'))
    activity = BooleanField(__('User activity label'))
    firstname = TextField(__('First name label'),
                          validators=[Required()],
                          widget=WidgetPrebind(TextInput(),
                                               class_='form-control uk-width-1-3'))
    lastname = TextField(__('Last name label'),
                         widget=WidgetPrebind(TextInput(),
                                              class_='form-control uk-width-1-3'))
    roles = QuerySelectMultipleField(__('Roles label'),
                                     query_factory=lambda: roles,
                                     get_label='title',
                                     get_pk=lambda x: x.id,
                                     validators=[Optional()],
                                     option_widget=CheckboxInput(),
                                     widget=WidgetPrebind(ListWidget(prefix_label=False), class_='list-unstyled'))

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

        if kwargs.get('is_current_user', False):
            if 'activity' in self:
                self.__delitem__('activity')

        if kwargs.get('update', False):
            self['password'].validators.append(Optional())
            self['password'].flags.required = False
            self['confirm'].validators.append(Optional())
            self['confirm'].flags.required = False
        else:
            self['password'].validators.append(Required())
            self['password'].flags.required = True
            self['confirm'].validators.append(Required())
            self['confirm'].flags.required = True

        self._uid = kwargs.get('user_id', 0)

    def validate(self):
        rv = Form.validate(self)

        if not rv:
            return False

        user = User.query.filter(sa.func.lower(User.login) == self.login.data.lower()).first()

        if user and self._uid != user.id:
            self.login.errors.append(_('User already exists message'))
            return False

        return True


class UserFiltersForm(wForm):
    fullname = TextField(widget=WidgetPrebind(TextInput(),
                                              class_='form-control input-sm'))
    login = TextField(widget=WidgetPrebind(TextInput(),
                                           class_='form-control input-sm'))
    activity = SelectField(choices=all_list + yesno_list,
                           widget=WidgetPrebind(Select(),
                                                class_='form-control input-sm'))
    role = QuerySelectField(query_factory=lambda: roles,
                            allow_blank=True,
                            blank_text=__('All option'),
                            get_label='title',
                            widget=WidgetPrebind(Select(),
                                                 class_='form-control input-sm uk-width-1-1'))
