jQuery.fn.reverse = [].reverse;

$(function(){
	var isScrolling = false;
	var delta = 0;
	var timer;
	var oldTop = $(document).scrollTop();
	var height = $(window).height();
	var sensitivity = 0.15; // minimum fraction of screen height to scroll to trigger block switching
	
	$(document).on('scroll', onScroll);
	$(window).on('resize', onResize);
	
	function onScroll(){
		if(timer) clearTimeout(timer); // prevent premature autoscroll
		if(isScrolling) return; // don't count automatic scrolling events
		
		var self = $(this);
		var top = self.scrollTop();
		delta = top - oldTop;
		oldTop = top;
		
		function scrollDown(){
			if(timer) clearTimeout(timer); // prevent premature autoscroll
			$('.block').each(function(){
				var t = $(this).offset().top;
				if((t > top) && (t < top + height * (1 - sensitivity))) {
					isScrolling = true;
					$('html').animate({scrollTop: t}, {complete: function(){ isScrolling = false; }}, 250);
					return;
				}
			});
			timer = setTimeout(scrollUp, 500);
		}
		
		function scrollUp(){
			if(timer) clearTimeout(timer); // prevent premature autoscroll
			$('.block').reverse().each(function(){
				var h = $(this).height();
				var b = $(this).offset().top + h;
				if((b > top + height * sensitivity) && (b < top + height)) {
					isScrolling = true;
					$('html').animate({scrollTop: b - height}, {complete: function(){ isScrolling = false; }}, 250);
					return;
				}
			});
			timer = setTimeout(scrollDown, 500);
		}
		
		if(delta > 0){
			scrollDown();
		} else {
			scrollUp();
		}
	}
	
	function onResize(){
		height = $(window).height();
	}
});