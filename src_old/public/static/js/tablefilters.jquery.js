$.fn.tableFilters = function (data) {
	var $elem = this;
	var thead = $("thead", $elem);
	function wFilter(){
		var $params = $("input, select", thead).serialize();
		document.location = "?" + $params;
	}
	$("thead select", $elem).on("change", wFilter);
	$("thead input").keyup(function (event) {
		if (event.keyCode == '13') {
			wFilter()
		}
		return false;
	});
	return $elem;
};