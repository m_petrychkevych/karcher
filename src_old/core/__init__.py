import sqlalchemy as sa

from flask.ext.babel import lazy_gettext as __


all_list = [('', __('All option'))]
yesno_list = [('True', __('Yes option')),
              ('False', __('No option'))]


def create_alembic_target_metadata():
    from brpr.rbac.models import Base as RBACBase
    from core.models import BaseTreeNode
    from flask import current_app
    alembic_metadata = current_app.extensions['migrate'].db.metadata if 'migrate' in current_app.extensions else None

    if not alembic_metadata:
        return None

    target_metadata = sa.MetaData()

    for metadata in [alembic_metadata, BaseTreeNode.metadata, RBACBase.metadata]:
        for tbl in metadata.tables.values():
            tbl.tometadata(target_metadata)

    return target_metadata
