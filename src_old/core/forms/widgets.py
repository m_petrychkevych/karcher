from wtforms.widgets import FileInput, html_params, HTMLString


class UploadInput(FileInput):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        class_ = kwargs.get('class_', '')
        class_ = class_ + ' file-loading' if class_ else 'file-loading'
        kwargs['class_'] = class_
        return HTMLString('<input %s>' % html_params(name=field.name, type='file', **kwargs))
