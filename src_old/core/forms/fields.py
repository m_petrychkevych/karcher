from wtforms import FileField
from wtforms.widgets import Select
from . import widgets


__all__ = (
    'UploadField',
)


class UploadField(FileField):
    """
    Can render a file-upload field. Will take any passed filename value, if
    any is sent by the browser in the post params.
    """
    widget = widgets.UploadInput()


class ColorSelect(Select):
    """
    Renders a color select field.
    """
    @classmethod
    def render_option(cls, value, label, selected, **kwargs):
        data = {'data-color': label}
        options = dict(kwargs.items() + data.items())
        return super(ColorSelect, cls).render_option(value, label, selected, **options)
