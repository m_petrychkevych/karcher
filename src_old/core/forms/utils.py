
def check_file_extension(filename, allowed_extensions=[]):
    return '.' in filename and filename.rsplit('.', 1)[1] in allowed_extensions


def is_empty(filters_form):
    result = True

    for d in filters_form.data.values():
        if d and d != 'None':
            result = False
            break

    return result
