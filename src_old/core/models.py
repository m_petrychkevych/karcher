import sqlalchemy as sa
import sqlamp

from flask import current_app
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import declarative_base


db = SQLAlchemy(current_app) if 'sqlalchemy' not in current_app.extensions.keys() else current_app.extensions.get('sqlalchemy').db
BaseTreeNode = declarative_base(metadata=sa.MetaData(db.engine), metaclass=sqlamp.DeclarativeMeta)
