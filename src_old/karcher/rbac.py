# coding: utf-8
from flask.ext.babel import gettext as _

from .init import rbac


@rbac.fixtures
def rbac_fixtures():
    return {
        'objects': [],
        'operations': [],
        'roles': [
            dict(title=u'Клиент',
                 name='customer',
                 permissions=[])
        ]
    }


def rbac_translations():
    # Objects
    _('RBAC User object')
    _('RBAC Permission object')
    _('RBAC Role object')

    # Operations
    _('RBAC Create operation')
    _('RBAC Edit operation')
    _('RBAC Delete operation')
    _('RBAC View operation')
    _('RBAC Assign operation')
    _('RBAC Deassign operation')
    _('RBAC Grant operation')
    _('RBAC Revoke operation')
