# coding: utf-8
from __future__ import print_function
import os

from brpr.blueprints.language.manage import MakeMessage, CompileMessage
from brpr.flask.blueprint import DatabaseBlueprint
from flask import current_app
from flask.ext.assets import ManageAssets
from flask.ext.migrate import MigrateCommand
from flask.ext.script import Server, Manager
from flask.ext.script.commands import Clean, ShowUrls

from .init import app, collect, db


manager = Manager(app)

manager.add_command('clean', Clean())
manager.add_command('routes', ShowUrls())
manager.add_command('makemessages', MakeMessage('karcher'))
manager.add_command('compilemessages', CompileMessage('karcher'))
manager.add_command('runserver', Server())
manager.add_command('db', MigrateCommand)
manager.add_command('migrate', MigrateCommand)
manager.add_command('assets', ManageAssets())

collect.init_script(manager)


@manager.command
def fastcgi():
    """ Run application as fastcgi. """
    from flup.server.fcgi import WSGIServer

    class ScriptNameStripper(object):
        def __init__(self, app):
            self.app = app

        def __call__(self, environ, start_response):
            environ['SCRIPT_NAME'] = ''
            return self.app(environ, start_response)

    wapp = ScriptNameStripper(app)
    WSGIServer(wapp).run()


@manager.command
def folders():
    """ Create all public and private media folders. """
    print('Creating public and private folders ...')

    tmp_path = os.path.join(current_app.config.get('PROJECT_ROOT', ''), 'tmp')
    base_path = os.path.join(current_app.config.get('PROJECT_ROOT', ''), 'public')
    images_path = os.path.join(base_path, 'images')

    dir_list = [tmp_path,
                base_path,
                images_path,
                os.path.join(images_path, 'blocks_bg'),
                os.path.join(images_path, 'blocks_ct')]

    for dir in dir_list:
        if not os.path.exists(dir):
            os.makedirs(dir)
            print('Creating path {0} ... '.format(dir), end='')
            print('ok')


def drop_all():
    from sqlalchemy.engine import reflection
    from sqlalchemy.schema import (DropConstraint,
                                   DropTable,
                                   ForeignKeyConstraint,
                                   MetaData,
                                   Table)
    inspector = reflection.Inspector.from_engine(db.engine)
    metadata = MetaData()
    tbs = []
    all_fks = []

    for table_name in inspector.get_table_names():
        fks = []

        for fk in inspector.get_foreign_keys(table_name):
            if not fk['name']:
                continue

            fks.append(ForeignKeyConstraint((), (), name=fk['name']))

        t = Table(table_name, metadata, *fks)
        tbs.append(t)
        all_fks.extend(fks)

    for fkc in all_fks:
        db.engine.execute(DropConstraint(fkc))

    for table in tbs:
        db.engine.execute(DropTable(table))


def _fixtures(db, filename):
    import os

    fname = os.path.join(os.path.dirname(__file__), filename)

    if os.path.exists(fname):
        f = open(fname)
        content = None
        content = f.read()

        if content:
            db.session.execute(content.decode('utf-8'))
            db.session.flush()
            print('Initialised with fixtures file {0} ...'.format(filename))
        else:
            print('No fixtures was used. Fixtures file {0} is empty'.format(filename))
    else:
        print('No fixtures was used. Fixtures file {0} not found'.format(filename))


@manager.command
def initdb(console=True):
    """ Drop all tables and creates new. """
    import rbac
    from users.models import User, UserProfile

    user_defaults = {'login': 'admin@brpr.ru',
                     'password': 'admin'}
    input_msg = 'Enter {0} [{1}]: '

    if console:
        for key, value in user_defaults.iteritems():
            user_defaults[key] = raw_input(input_msg.format(key, value)) or value

    user = User(**user_defaults)
    user.activity = True
    user.system = True
    profile = UserProfile(lastname=user_defaults['login'],
                          firstname=user_defaults['login'],
                          user=user)

    drop_all()

    db.create_all()
    from core.models import BaseTreeNode
    BaseTreeNode.metadata.create_all()
    rbac.rbac._init_backend()
    db.session.add(user)
    db.session.add(profile)
    rbac.rbac._fixtures(db.session, user.login)
    _fixtures(db, 'initial_data.sql')
    db.session.flush()
    DatabaseBlueprint.sync(db)
    db.session.commit()


def main():
    manager.run()
