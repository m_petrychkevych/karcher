-- Initialize pages
INSERT INTO pages (id, title, published) VALUES (1, 'Главная страница', true);
SELECT setval('pages_id_seq', (SELECT MAX(id) FROM pages));


-- Initiailize site
INSERT INTO site (id, parent_id, page_id, slug, mp_path, mp_depth, mp_tree_id) VALUES (
    1, null, 1, '', '', 0, 1
);
SELECT setval('site_id_seq', (SELECT MAX(id) FROM site));

--
-- Color dictionary
--

-- Background colors
INSERT INTO color_dictionary (id, color, type_, position) VALUES (1, '#ffee3b', 'Background', 1);
INSERT INTO color_dictionary (id, color, type_, position) VALUES (2, '#ffffff', 'Background', 2);
INSERT INTO color_dictionary (id, color, type_, position) VALUES (3, '#eeeeee', 'Background', 3);

-- Font colors
INSERT INTO color_dictionary (id, color, type_, position) VALUES (4, '#000000', 'Font', 1);
INSERT INTO color_dictionary (id, color, type_, position) VALUES (5, '#5e5e5e', 'Font', 2);


SELECT setval('color_dictionary_id_seq', (SELECT MAX(id) FROM color_dictionary));


--
-- Test blocks
--
INSERT INTO page_blocks (id, page_id, position, comment, published, status, type_, font_color_id, background_color_id, background_size, background_position, background_image) VALUES (
    1, 1, 1, 'Первый блок', true, 'Active', 'Hd1Tx2Ft1', null, null, 'cover', 'center bottom', '1.jpg'
);
INSERT INTO page_blocks (id, page_id, position, comment, published, status, type_, font_color_id, background_color_id, background_size, background_position, background_image) VALUES (
    2, 1, 2, 'Второй блок', true, 'Active', 'Hd1Tx2Hd1Im1', null, null, 'auto', 'center center', ''
);
INSERT INTO page_blocks (id, page_id, position, comment, published, status, type_, font_color_id, background_color_id, background_size, background_position, background_image) VALUES (
    3, 1, 3, 'Третий блок', true, 'Active', 'Hd1Tx2Calc', null, null, 'auto', 'center center', ''
);

SELECT setval('page_blocks_id_seq', (SELECT MAX(id) FROM page_blocks));

INSERT INTO hd1tx2ft1 (id, header, left_text, right_text, footer, pageblock_id) VALUES (
    1,
    'Мойки самообслуживания&nbsp;&mdash; высокорентабельный бизнес с&nbsp;минимальными рисками !!!',
    '<h3 class="yellow-bg">Высокая популярность у клиентов</h3>
                                                        <ul>
                                                                <li>На мойках самообслуживания не бывает очередей</li>
                                                                <li>Стоимость мойки всего 100–150 рублей</li>
                                                                <li>Высокое качество мойки благодаря технологиям Karcher</li>
                                                                <li>Каждый второй клиент возвращается</li>
                                                        </ul>',
    '<h3 class="yellow-bg">Минимальные расходы и высокий доход</h3>
                                                        <ul>
                                                                <li>Низкие операционные расходы<br><small>(чистящие средства и коммунальные платежи)</small></li>
                                                                <li>Отсутствие персонала на самой мойке</li>
                                                                <li>Стабильный поток клиентов — 250 машин в сутки</li>
                                                                <li>Быстро растущая популярность<br><small>70% клиентов рассказывают о мойках<br>самообслуживания знакомым)</small></li>
                                                        </ul>',
    '<big><b>Высокая рентабельность бизнеса. Полная окупаемость за 2 года!</b></big>', 1
);
SELECT setval('hd1tx2ft1_id_seq', (SELECT MAX(id) FROM hd1tx2ft1));

INSERT INTO hd1tx2hd1im1 (id, header_1, left_text, right_text, header_2, pageblock_id) VALUES (
    1,
    'Потенциал рынка моек самообслуживания',
    '<ul>
								<li>Более 40&nbsp;млн. легковых автомобилей в&nbsp;РФ<br /><small>Тенденция дальнейшего роста</small></li>
								<li>Средняя периодичность мойки&nbsp;— 3&nbsp;раза в&nbsp;месяц<br /><small>Постоянство востребованности услуги</small></li>
							</ul>',
    '<ul>
								<li>Дефицит предложения на рынке моечных услуг<br /><small>Постоянные очереди на автомойку</small></li>
								<li>Рынок МСО только формируется<br /><small>Востребованность необычайно высока</small></li>
							</ul>',
    'Количество моек в России', 2
);
SELECT setval('hd1tx2hd1im1_id_seq', (SELECT MAX(id) FROM hd1tx2hd1im1));


INSERT INTO hd1tx2calc (id, header, left_text, right_text, pageblock_id) VALUES (
    1,
    'Гарантированный поток клиентов<br />и быстрый возврат инвестиций',
    '<h4>Для автовладельцев стоимость мойки составит всего 100 рублей.</h3>
							<p>Это в несколько раз меньше, чем на обычных мойках, а значит, вы всегда будете обеспечены клиентами.<br />Ваши клиенты могут выбрать из множества программ мойки, включая покрытие воском и пылесос.</p>
							<p>Эффективность оборудования Karcher сводит к минимуму затраты расходных материалов. В совокупности с минимальными начальными затратами и большим потоком клиентов это позволяет с уверенностью прогнозировать срок полного возврата инвестиций.</p>
							<p>В среднем мойка из 6 постов <b>полностью окупается за 2 года.</b></p>',
    '<h3><small>Давайте посчитаем!</small></h3>
							<p><em>Насколько выгодным окажется ваш бизнес? Ключевые цифры вы можете узнать уже сейчас с помощью нашего <a href="#">калькулятора</a>.</em></p>',
    3
);
SELECT setval('hd1tx2calc_id_seq', (SELECT MAX(id) FROM hd1tx2calc));
