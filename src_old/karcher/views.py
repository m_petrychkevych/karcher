# coding: utf-8
import os

from flask import current_app, render_template, send_from_directory
from flask.ext.assets import Bundle
from users.models import User

from .init import app, assets, lm
from .models import Page, PageBlock


@app.route('/')
def index():
    page = Page.query.get(1)

    return page.render()
    # return render_template('index.html')


@app.route('/public/<path:filename>')
def public_dir(filename):
    path = os.path.join(current_app.config.get('PROJECT_ROOT', ''), 'public')
    return send_from_directory(path, filename)


@app.errorhandler(401)
def not_authorized(error):
    return render_template('401.html', error=error), 401


@app.errorhandler(404)
def error_404(error):
    return render_template('404.html', error=error), 404


@app.errorhandler(500)
def error_500(error):
    return render_template('500.html', error=error), 500


@app.errorhandler(501)
def not_implemented(error):
    return render_template('501.html', error=error), 501


@lm.user_loader
def load_user(user_id):
    return User.load_user(user_id)


assets.register('css_admin_common',
                Bundle('admin/css/bootstrap.css',
                       'admin/css/bootstrap-theme.css',
                       'admin/css/dataTables.bootstrap.css',
                       'admin/css/rowReorder.bootstrap.css',
                       'admin/css/bootstrap-colorselector.css',
                       'admin/css/common.css',
                       filters='jinja2',
                       output='css/final/admin.css'))

assets.register('js_admin_common',
                Bundle('js/jquery.min.js',
                       'admin/js/bootstrap.js',
                       'admin/js/jquery.dataTables.js',
                       'admin/js/dataTables.bootstrap.js',
                       'admin/js/dataTables.rowReorder.js',
                       'admin/js/bootstrap-colorselector.js',
                       'admin/js/buttonsbar.jquery.js',
                       'admin/js/tablefilters.jquery.js',
                       'admin/js/common.js',
                       filters='jinja2',
                       output='js/final/admin.js'))

assets.register('css_front_common',
                Bundle('css/uikit.css',
                       'admin/css/uikit.theme.css',
                       'css/components/notify.css',
                       'css/common.css',
                       filters='jinja2',
                       output='css/final/karcher.css'))

assets.register('js_front_common',
                Bundle('js/jquery.min.js',
                       'js/uikit.min.js',
                       'js/components/notify.min.js',
                       filters='jinja2',
                       output='js/final/karcher.js'))

assets.register('css_front_with_admin_common',
                Bundle('admin/css/bootstrap.css',
                       'admin/css/bootstrap-theme.css',
                       'admin/css/common.css',
                       'css/uikit.css',
                       'admin/css/uikit.theme.css',
                       'css/components/notify.css',
                       'css/common.css',
                       filters='jinja2',
                       output='css/final/karcher.css'))

assets.register('js_front_with_admin_common',
                Bundle('js/jquery.min.js',
                       'admin/js/bootstrap.js',
                       'js/uikit.min.js',
                       'js/components/notify.min.js',
                       filters='jinja2',
                       output='js/final/akarcher.js'))
