# coding: utf-8
import os
import sqlalchemy as sa
import sqlamp
import uuid

from core.models import BaseTreeNode
from flask import abort, current_app, render_template, url_for
from flask.ext.babel import gettext as _, lazy_gettext as __
from jinja2.exceptions import TemplateNotFound
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property

from .init import db


class BlockCollection(object):
    class __metaclass__(type):
        def __init__(cls, name, bases, attrs):
            super(type, cls).__init__(name, bases, attrs)

            for c in bases:
                if isinstance(c, cls):
                    raise TypeError(str(c.__name__) + ' is final')

    collection = dict()

    @classmethod
    def register_block_class(cls, reg_cls):
        cls.collection.setdefault(reg_cls.__name__,
                                  dict(class_=reg_cls,
                                       type_=reg_cls.__name__,
                                       description=getattr(reg_cls, '__description__', __('Block default description'))))
        return reg_cls

    @classmethod
    def listing(cls):
        return [(c['type_'], c['description']) for c in cls.collection.values()]

    @classmethod
    def check_block(cls, clsname):
        """ TODO: Maybe useless??? """
        return clsname in cls.collection.keys()


def register_as_block(cls):
    """ Decorator for register block classes """
    return BlockCollection.register_block_class(cls)


class ColorDictionary(db.Model):
    typeFont = 'Font'
    typeBackground = 'Background'

    __tablename__ = 'color_dictionary'

    id = db.Column(db.Integer, primary_key=True)
    color = db.Column(db.String(7))
    type_ = db.Column(db.Enum(typeFont, typeBackground, name='color_types'), db.DefaultClause(typeFont))
    position = db.Column(db.Integer, default=0, server_default='0')

    __table_args__ = (db.UniqueConstraint('color', 'type_'), )

    def __unicode__(self):
        return u'{0}'.format(self.color)

    @classmethod
    def font_colors(cls):
        query = cls.query
        query = query.filter(cls.type_.__eq__(cls.typeFont))
        query = query.order_by(cls.position.asc())
        return query

    @classmethod
    def background_colors(cls):
        query = cls.query
        query = query.filter(cls.type_.__eq__(cls.typeBackground))
        query = query.order_by(cls.position.asc())
        return query


class AbstractBlock(object):
    """ Base class for all other types of block. """
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = db.Column(db.Integer, primary_key=True)

    @declared_attr
    def pageblock_id(cls):
        return db.Column('pageblock_id', db.ForeignKey('page_blocks.id'))

    @declared_attr
    def block(cls):
        return db.relationship('PageBlock', backref=db.backref('%s_attributes' % cls.__name__.lower(), uselist=False, lazy='joined'))

    # Usefull methods (BlockAPI)
    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready:
            return

        # Collect data
        kwargs.setdefault('block_data', self.__get_columns_data())

        try:
            return render_template(self.__default_template, editable=editable, **kwargs)
        except TemplateNotFound as e:
            current_app.logger.exception(e)

        return 'TODO: Abstract block Default template or selfname template'
        # raise NotImplementedError()

    def preview_icon(self):
        # TODO: Make default preview image
        fname = url_for('admin.static', filename='images/blocks/{0}.png'.format(cls.__name__.lower()))
        return fname

    @property
    def publish_ready(self):
        # TODO: Check template exists!
        return True

    def __get_columns(self):
        """ Returns table columns for self block class. """
        cls = self.__class__
        ready = {}

        if hasattr(cls, '__table__'):
            for item in cls.__table__.columns.items():
                ready.setdefault(item[0], item[1])

        return ready

    def __get_columns_data(self):
        """ Returns table columns data for self block class. """
        columns = self.__get_columns()
        ready = {}

        for col in columns.items():
            ready.setdefault(col[0], getattr(self, col[0]))

        return ready

    def get_form(self):
        form = None

        try:
            mod = __import__('admin.block_forms')
            form = getattr(mod.block_forms, '{0}Form'.format(self.__class__.__name__), None)
        except ImportError as e:
            current_app.logger.exception(e)

        return form

    @property
    def __default_template(self):
        fname = '{0}.html'.format(self.__class__.__name__.lower())
        return os.path.join('elements', 'blocks', fname)


class Page(db.Model):
    __tablename__ = 'pages'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    published = db.Column(db.Boolean, default=True, server_default='true')

    blocks = db.relationship('PageBlock', uselist=True, backref=db.backref('page', uselist=False))

    __mapper_args__ = {'order_by': id.asc()}

    @classmethod
    def admin_list(cls, with_count=False):
        query = cls.query
        count = query.count() if with_count else 0

        if with_count:
            return query, count

        return query

    def render(self, editable=False, *args, **kwargs):
        r = []

        # Count blocks
        blocks_total = len([b.id for b in self.blocks if b.status == PageBlock.statusActive])

        for b_idx, block in enumerate(self.blocks):
            if block.status == PageBlock.statusActive:
                block_render = block.render(editable=editable, block_index=b_idx, blocks_total=blocks_total, *args, **kwargs)
    
                if block_render:
                    r.append(block.render(editable=editable, *args, **kwargs))

        if not r:
            abort(404)

        block_collection = '\n'.join(r)
        kwargs.setdefault('editable', editable)

        if editable:
            kwargs.setdefault('page_data', {'page': self,
                                            'page_id': self.id})

        return render_template('page.html',
                               block_collection=block_collection,
                               **kwargs)


class PageBlock(db.Model):
    statusActive = 'Active'
    statusTrash = 'Trash'
    sizeBackgrounds = [
        ('auto', __('Background-size auto option')),
        ('contain', __('Background-size contain option')),
        ('cover', __('Background-size cover option'))
    ]
    positionBackgrounds = [
        ('center bottom', __('Background-position center bottom option')),
        ('center center', __('Background-position center center option')),
        ('center top', __('Background-position center top option')),
        ('left bottom', __('Background-position left bottom option')),
        ('left center', __('Background-position left center option')),
        ('left top', __('Background-position left top option')),
        ('right bottom', __('Background-position right bottom option')),
        ('right center', __('Background-position right center option')),
        ('right top', __('Background-position right top option'))
    ]

    __tablename__ = 'page_blocks'

    id = db.Column(db.Integer, primary_key=True)
    page_id = db.Column(db.Integer, db.ForeignKey(Page.id), nullable=False)
    position = db.Column(db.Integer, default=0, server_default='0')
    comment = db.Column(db.String)
    published = db.Column(db.Boolean, default=False, server_default='false')
    status = db.Column(db.Enum(statusActive, statusTrash, name='block_status'), db.DefaultClause(statusActive))
    type_ = db.Column(db.String, nullable=False)

    # Common block design
    font_color_id = db.Column(db.Integer, db.ForeignKey('color_dictionary.id'), nullable=True)
    background_color_id = db.Column(db.Integer, db.ForeignKey('color_dictionary.id'), nullable=True)

    font_color = db.relationship('ColorDictionary', foreign_keys=[font_color_id], lazy='joined')
    background_color = db.relationship('ColorDictionary', foreign_keys=[background_color_id], lazy='joined')

    background_size = db.Column(db.Enum(*[sb[0] for sb in sizeBackgrounds], name='background_sizes'), db.DefaultClause(sizeBackgrounds[0][0]))
    background_position = db.Column(db.Enum(*[pb[0] for pb in positionBackgrounds], name='background_positions'), db.DefaultClause(positionBackgrounds[1][0]))

    # TODO: background-image:
    background_image = db.Column(db.Text())
    # TODO: background height param

    __mapper_args__ = {'order_by': position.asc()}

    def get_data_object(self):
        rel = self._get_attr_relation()

        if rel:
            return rel

        return None

    def _get_relationship_name(self):
        """ Returns current block type attribute relationship name. """
        return '%s_attributes' % (self.type_.lower())

    def create_all_data(self):
        """ Create and return specified type object  """
        return eval(self.type_)(block=self)

    def _get_attr_relation(self):
        """ Returns current block type attribute relationship. """
        attr = self._get_relationship_name()

        if hasattr(self, attr):
            return getattr(self, attr)

        return None

    def _get_block_inline_style(self):
        attrs = [u'background-size: {0}'.format(self.background_size),
                 u'background-position: {0}'.format(self.background_position)]

        if self.font_color:
            attrs.append(u'color: {0}'.format(self.font_color.color))

        if self.background_color:
            attrs.append(u'background-color: {0}'.format(self.background_color.color))

        if self.background_image:
            attrs.append(u'background-image: url({0})'.format(url_for('public_dir', filename=os.path.join('images', 'blocks_bg', self.background_image))))

        if attrs:
            return u' style="{0}"'.format(u';'.join(attrs))

        return u''

    def _generate_block_token(self):
        return uuid.uuid4().hex

    def render(self, editable=False, *args, **kwargs):
        if not self.publish_ready and not editable:
            return

        rel = self._get_attr_relation()

        inner_block_content = ''
        # Global default block attributes
        kwargs.setdefault('global_block_id', self.global_id)

        if rel:
            inner_block_content = rel.render(editable=editable, *args, **kwargs)

            if not inner_block_content:
                return

        return render_template('elements/common_block.html',
                               editable=editable,
                               block_only_id=self.id,
                               block_inline_style=self._get_block_inline_style(),
                               inner_block_content=inner_block_content,
                               block_token=self._generate_block_token(),
                               **kwargs)

    @property
    def publish_ready(self):
        return self.status == self.statusActive and self.published is True

    @property
    def global_id(self):
        return u'global_block_{0}'.format(self.id)

    def get_form(self):
        rel = self._get_attr_relation()
        form = None

        if rel:
            form = rel.get_form()

        return form

    @classmethod
    def before_insert_listener(cls, mapper, connection, target):  # TODO: Make a mixin or another def ?
        count = db.session.query(sa.func.count(cls.id)).first()
        count = count[0] if count else 0
        target.position = count + 1


sa.event.listen(PageBlock, 'before_insert', PageBlock.before_insert_listener)


class Site(BaseTreeNode):
    __tablename__ = 'site'
    __mp_manager__ = 'mp'

    id = db.Column(db.Integer, primary_key=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('site.id'))
    page_id = db.Column(db.Integer, db.ForeignKey(Page.id), nullable=False)
    slug = db.Column(db.String)

    parent = db.relationship('Site', remote_side=[id])
    page = db.relationship(Page)

    # Meta data
    meta_title = db.Column(db.Text)
    meta_keywords = db.Column(db.Text)
    meta_description = db.Column(db.Text)

    def __unicode__(self):
        return '{0}'

    @property
    def tree_title(self):
        depth = u'-' * self.mp_depth
        return u'{0} {1}'.format(depth, self.title)

    @hybrid_property
    def title(self):
        return self.title

    @hybrid_property
    def published(self):
        return self.page.published

    @hybrid_property
    def publish_ready(self):
        return self.page.publish_ready

    @classmethod
    def admin_list(cls):
        query = db.session.query(cls)
        return query


# -- BLOCKS --
@register_as_block
class Hd1Tx2Ft1(db.Model, AbstractBlock):
    __description__ = _('Hd1Tx2Ft block description')

    header = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    left_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    right_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    footer = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)


@register_as_block
class Hd1Tx2Hd1Im1(db.Model, AbstractBlock):
    __description__ = _('Hd1Tx2Hd1Im1 block description')

    header_1 = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    left_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    right_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    header_2 = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)


@register_as_block
class Hd1Tx2Calc(db.Model, AbstractBlock):
    __description__ = _('Hd1Tx2Hd1Im1 block description')

    header = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    left_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
    right_text = db.Column(db.Text, default='&nbsp;', server_default='&nbsp;', nullable=False)
